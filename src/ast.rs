//use super::gql::*;
use super::gql::{self, Number, Token, TokenType};
use core::fmt;
use core::hash::{Hash, Hasher};
use regex::Regex;
use std::mem::replace;
use std::rc::Rc;
use std::sync::Arc;

pub type Result<T> = core::result::Result<T, ASTError>;

#[derive(PartialEq, Debug, Clone)]
pub enum ASTError {
    Empty,

    NotABinaryExpression(Arc<Expression>),
    InvalidBinaryExpression(Arc<Token>, Arc<Expression>, Arc<Expression>),
    NoLeftBinaryExpression,
    MissingPiecesOfBinaryExpression(
        Option<Arc<Token>>,
        Option<Arc<Expression>>,
        Option<Arc<Expression>>,
    ),

    InvalidFunctionCall(Arc<Token>, Arc<Token>),

    NoMoreToken(usize),
    UnknownInitialToken(Arc<Token>),

    UnknownSelectToken(Arc<Token>),
    InvalidSelectionAlias(Arc<Token>),

    UnknownOrderingToken(Arc<Token>),

    UnknownGroupingToken(Arc<Token>),

    InvalidLimitToken(Arc<Token>),
    UnknownLimitToken(Arc<Token>),

    InvalidOffsetToken(Arc<Token>),
    UnknownOffsetToken(Arc<Token>),

    UnknownWhereToken(Arc<Token>),
    UnknownExpressionToken(Arc<Token>),
    UnknownUnaryExpressionToken(Arc<Token>, Arc<Token>),
    UnknownBinaryExpressionToken(Arc<Token>, Arc<Token>, Arc<Token>),
}

#[derive(PartialEq, Default, Debug, Clone)]
pub struct Statement {
    pub select: Option<Select>,
    pub condition: Option<Arc<Expression>>,
    pub order: Option<Vec<Order>>,
    pub group: Option<Vec<Arc<Expression>>>,
    pub having: Option<Arc<Expression>>,
    pub limit: Option<u64>,
    pub offset: Option<u64>,
    pub directives: Option<Vec<Directive>>,
}

impl Statement {
    fn new() -> Self {
        Self {
            select: None,
            condition: None,
            order: None,
            group: None,
            having: None,
            limit: None,
            offset: None,
            directives: None,
        }
    }

    pub fn get_select(&self) -> Vec<Selection> {
        if let Some(select) = self.select.as_ref() {
            if !select.selections.is_empty() {
                let mut selections = Vec::new();

                for selection in &select.selections {
                    match selection.expression.as_ref() {
                        Expression::AllColumns => selections.append(&mut Self::select_all()),
                        _ => selections.push(selection.clone()),
                    }
                }

                return selections;
            }
        }

        Self::select_default()
    }

    fn names_as_selections(columns: Vec<String>) -> Vec<Selection> {
        columns
            .into_iter()
            .map(|column| Selection {
                expression: Arc::new(Expression::Identifier(column)),
                alias: None,
            })
            .collect()
    }

    pub fn select_all() -> Vec<Selection> {
        Self::names_as_selections(vec![
            "old_file_path".to_owned(),
            "new_file_path".to_owned(),
            "old_file_name".to_owned(),
            "new_file_name".to_owned(),
            "old_file_extension".to_owned(),
            "new_file_extension".to_owned(),
            "added_files".to_owned(),
            "deleted_files".to_owned(),
            "renamed_files".to_owned(),
            "modified_files".to_owned(),
            "author".to_owned(),
            "author_name".to_owned(),
            "author_email".to_owned(),
            "commit_hash".to_owned(),
            "commit_message".to_owned(),
            "commit_date".to_owned(),
            "commit_insertions".to_owned(),
            "files_changed".to_owned(),
            "commit_deletions".to_owned(),
            "is_merge_commit".to_owned(),
            "is_root_commit".to_owned(),
        ])
    }

    pub fn select_default() -> Vec<Selection> {
        Self::names_as_selections(vec![
            "added_files".to_owned(),
            "deleted_files".to_owned(),
            "renamed_files".to_owned(),
            "modified_files".to_owned(),
            "author".to_owned(),
            "author_name".to_owned(),
            "author_email".to_owned(),
            "commit_hash".to_owned(),
            "commit_message".to_owned(),
            "commit_date".to_owned(),
            "commit_insertions".to_owned(),
            "files_changed".to_owned(),
            "commit_deletions".to_owned(),
            "is_merge_commit".to_owned(),
            "is_root_commit".to_owned(),
        ])
    }

    pub fn is_distinct(&self) -> bool {
        self.select.as_ref().map(|s| s.distinct).unwrap_or(false)
    }

    pub fn contains_directive(&self, directive: Directive) -> bool {
        if let Some(directives) = &self.directives {
            directives.contains(&directive)
        } else {
            false
        }
    }

    pub fn number_of_threads(&self) -> u8 {
        if let Some(directives) = &self.directives {
            for directive in directives {
                match directive {
                    Directive::Thread(t) => return *t,
                    _ => {}
                }
            }
        }

        4
    }
}

#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy)]
pub enum Directive {
    Thread(u8),
    NoFetch,
    NoPrune,
}

impl Directive {
    fn from(keyword: &str) -> Option<Self> {
        let keyword = keyword.to_lowercase();
        let keyword = keyword.as_str();

        let thread_regex = Regex::new("^thread(\\d+)$");

        if let Ok(regex) = thread_regex {
            let mut capture = regex.captures_iter(keyword);
            if let Some(capture) = capture.next() {
                let number = &capture[1];
                let number: u8 = number.parse().ok()?;
                return Some(Directive::Thread(number));
            }
        }

        match keyword {
            "no_fetch" => Some(Directive::NoFetch),
            "no_prune" => Some(Directive::NoPrune),
            _ => None,
        }
    }
}

#[derive(Eq, PartialEq, Debug, Clone, Hash)]
pub struct Argument {
    pub name: Option<String>,
    pub expression: Arc<Expression>,
}

impl Argument {
    pub fn new(name: Option<String>, expression: Arc<Expression>) -> Self {
        Self { name, expression }
    }

    pub fn named(name: &str, expression: Arc<Expression>) -> Self {
        Self {
            name: Some(name.to_owned()),
            expression,
        }
    }

    pub fn unnamed(expression: Arc<Expression>) -> Self {
        Self {
            name: None,
            expression,
        }
    }

    pub fn is_named(&self) -> bool {
        self.name.is_some() && !self.name.as_ref().unwrap().is_empty()
    }
}

impl fmt::Display for Argument {
    fn fmt(&self, f: &mut fmt::Formatter) -> core::result::Result<(), fmt::Error> {
        if let Some(ref name) = self.name {
            write!(f, "{}: {}", name, self.expression)
        } else {
            write!(f, "{}", self.expression)
        }
    }
}

#[derive(Debug, Clone)]
pub enum Expression {
    Null,
    String(String),
    InterpolatedString(Vec<Arc<Expression>>),
    Integer(u64),
    Float(f64),
    Boolean(bool),
    RegularExpression(String),
    Set(Vec<Arc<Expression>>),
    Function(String, Vec<Arc<Argument>>),
    Identifier(String),
    AllColumns,

    // Prefix operations
    Not(Arc<Expression>),
    Plus(Arc<Expression>),
    Minus(Arc<Expression>),
    BitInverse(Arc<Expression>),

    // Infix operations
    Range(Arc<Expression>, Arc<Expression>),

    Add(Arc<Expression>, Arc<Expression>),
    Subtract(Arc<Expression>, Arc<Expression>),
    Multiply(Arc<Expression>, Arc<Expression>),
    Divide(Arc<Expression>, Arc<Expression>),
    Modulo(Arc<Expression>, Arc<Expression>),
    BitAnd(Arc<Expression>, Arc<Expression>),
    BitOr(Arc<Expression>, Arc<Expression>),
    BitXor(Arc<Expression>, Arc<Expression>),
    BitShiftRight(Arc<Expression>, Arc<Expression>),
    BitShiftLeft(Arc<Expression>, Arc<Expression>),

    Equal(Arc<Expression>, Arc<Expression>),
    NotEqual(Arc<Expression>, Arc<Expression>),
    Contains(Arc<Expression>, Arc<Expression>),
    NotContains(Arc<Expression>, Arc<Expression>),
    Lower(Arc<Expression>, Arc<Expression>),
    LowerOrEqual(Arc<Expression>, Arc<Expression>),
    Greater(Arc<Expression>, Arc<Expression>),
    GreaterOrEqual(Arc<Expression>, Arc<Expression>),

    And(Arc<Expression>, Arc<Expression>),
    Or(Arc<Expression>, Arc<Expression>),
}

impl Hash for Expression {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match self {
            Expression::Null => state.write_u8(0),
            Expression::AllColumns => state.write_u8(1),
            Expression::String(value)
            | Expression::Identifier(value)
            | Expression::RegularExpression(value) => value.hash(state),

            Expression::Integer(value) => value.hash(state),
            Expression::Float(value) => (value.ceil() as i64).hash(state),
            Expression::Boolean(value) => value.hash(state),

            Expression::InterpolatedString(expressions) | Expression::Set(expressions) => {
                for expression in expressions {
                    expression.hash(state);
                }
            }
            Expression::Function(name, arguments) => {
                name.hash(state);
                for arg in arguments {
                    arg.hash(state);
                }
            }

            Expression::Not(rhs)
            | Expression::Plus(rhs)
            | Expression::Minus(rhs)
            | Expression::BitInverse(rhs) => rhs.hash(state),

            Expression::Range(lhs, rhs)
            | Expression::Add(lhs, rhs)
            | Expression::Subtract(lhs, rhs)
            | Expression::Multiply(lhs, rhs)
            | Expression::Divide(lhs, rhs)
            | Expression::Modulo(lhs, rhs)
            | Expression::BitAnd(lhs, rhs)
            | Expression::BitOr(lhs, rhs)
            | Expression::BitXor(lhs, rhs)
            | Expression::BitShiftRight(lhs, rhs)
            | Expression::BitShiftLeft(lhs, rhs)
            | Expression::Equal(lhs, rhs)
            | Expression::NotEqual(lhs, rhs)
            | Expression::Contains(lhs, rhs)
            | Expression::NotContains(lhs, rhs)
            | Expression::Lower(lhs, rhs)
            | Expression::LowerOrEqual(lhs, rhs)
            | Expression::Greater(lhs, rhs)
            | Expression::GreaterOrEqual(lhs, rhs)
            | Expression::And(lhs, rhs)
            | Expression::Or(lhs, rhs) => {
                lhs.hash(state);
                rhs.hash(state);
            }
        }
    }
}

impl Eq for Expression {}

impl PartialEq for Expression {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Expression::Null, Expression::Null) => true,
            (Expression::AllColumns, Expression::AllColumns) => true,
            (Expression::String(lhs), Expression::String(rhs)) => lhs == rhs,
            (Expression::Identifier(lhs), Expression::Identifier(rhs)) => lhs == rhs,
            (Expression::RegularExpression(lhs), Expression::RegularExpression(rhs)) => lhs == rhs,
            (Expression::Integer(lhs), Expression::Integer(rhs)) => lhs == rhs,
            (Expression::Float(lhs), Expression::Float(rhs)) => {
                (lhs - rhs).abs() < std::f64::EPSILON
            }
            (Expression::Boolean(lhs), Expression::Boolean(rhs)) => lhs == rhs,
            (Expression::InterpolatedString(lhs), Expression::InterpolatedString(rhs)) => {
                lhs == rhs
            }
            (Expression::Set(lhs), Expression::Set(rhs)) => lhs == rhs,
            (
                Expression::Function(lhs_name, lhs_arguments),
                Expression::Function(rhs_name, rhs_arguments),
            ) => lhs_name == rhs_name && lhs_arguments == rhs_arguments,

            (Expression::Not(lhs), Expression::Not(rhs)) => lhs == rhs,
            (Expression::Plus(lhs), Expression::Plus(rhs)) => lhs == rhs,
            (Expression::Minus(lhs), Expression::Minus(rhs)) => lhs == rhs,
            (Expression::BitInverse(lhs), Expression::BitInverse(rhs)) => lhs == rhs,

            (
                Expression::Range(lhs_first, lhs_second),
                Expression::Range(rhs_first, rhs_second),
            ) => lhs_first == rhs_first && lhs_second == rhs_second,
            (Expression::Add(lhs_first, lhs_second), Expression::Add(rhs_first, rhs_second)) => {
                lhs_first == rhs_first && lhs_second == rhs_second
            }
            (
                Expression::Subtract(lhs_first, lhs_second),
                Expression::Subtract(rhs_first, rhs_second),
            ) => lhs_first == rhs_first && lhs_second == rhs_second,
            (
                Expression::Multiply(lhs_first, lhs_second),
                Expression::Multiply(rhs_first, rhs_second),
            ) => lhs_first == rhs_first && lhs_second == rhs_second,
            (
                Expression::Divide(lhs_first, lhs_second),
                Expression::Divide(rhs_first, rhs_second),
            ) => lhs_first == rhs_first && lhs_second == rhs_second,
            (
                Expression::Modulo(lhs_first, lhs_second),
                Expression::Modulo(rhs_first, rhs_second),
            ) => lhs_first == rhs_first && lhs_second == rhs_second,
            (
                Expression::BitAnd(lhs_first, lhs_second),
                Expression::BitAnd(rhs_first, rhs_second),
            ) => lhs_first == rhs_first && lhs_second == rhs_second,
            (
                Expression::BitOr(lhs_first, lhs_second),
                Expression::BitOr(rhs_first, rhs_second),
            ) => lhs_first == rhs_first && lhs_second == rhs_second,
            (
                Expression::BitXor(lhs_first, lhs_second),
                Expression::BitXor(rhs_first, rhs_second),
            ) => lhs_first == rhs_first && lhs_second == rhs_second,
            (
                Expression::BitShiftRight(lhs_first, lhs_second),
                Expression::BitShiftRight(rhs_first, rhs_second),
            ) => lhs_first == rhs_first && lhs_second == rhs_second,
            (
                Expression::BitShiftLeft(lhs_first, lhs_second),
                Expression::BitShiftLeft(rhs_first, rhs_second),
            ) => lhs_first == rhs_first && lhs_second == rhs_second,
            (
                Expression::Equal(lhs_first, lhs_second),
                Expression::Equal(rhs_first, rhs_second),
            ) => lhs_first == rhs_first && lhs_second == rhs_second,
            (
                Expression::NotEqual(lhs_first, lhs_second),
                Expression::NotEqual(rhs_first, rhs_second),
            ) => lhs_first == rhs_first && lhs_second == rhs_second,
            (
                Expression::Contains(lhs_first, lhs_second),
                Expression::Contains(rhs_first, rhs_second),
            ) => lhs_first == rhs_first && lhs_second == rhs_second,
            (
                Expression::NotContains(lhs_first, lhs_second),
                Expression::NotContains(rhs_first, rhs_second),
            ) => lhs_first == rhs_first && lhs_second == rhs_second,
            (
                Expression::Lower(lhs_first, lhs_second),
                Expression::Lower(rhs_first, rhs_second),
            ) => lhs_first == rhs_first && lhs_second == rhs_second,
            (
                Expression::LowerOrEqual(lhs_first, lhs_second),
                Expression::LowerOrEqual(rhs_first, rhs_second),
            ) => lhs_first == rhs_first && lhs_second == rhs_second,
            (
                Expression::Greater(lhs_first, lhs_second),
                Expression::Greater(rhs_first, rhs_second),
            ) => lhs_first == rhs_first && lhs_second == rhs_second,
            (
                Expression::GreaterOrEqual(lhs_first, lhs_second),
                Expression::GreaterOrEqual(rhs_first, rhs_second),
            ) => lhs_first == rhs_first && lhs_second == rhs_second,
            (Expression::And(lhs_first, lhs_second), Expression::And(rhs_first, rhs_second)) => {
                lhs_first == rhs_first && lhs_second == rhs_second
            }
            (Expression::Or(lhs_first, lhs_second), Expression::Or(rhs_first, rhs_second)) => {
                lhs_first == rhs_first && lhs_second == rhs_second
            }
            _ => false,
        }
    }
}

impl fmt::Display for Expression {
    fn fmt(&self, f: &mut fmt::Formatter) -> core::result::Result<(), fmt::Error> {
        match self {
            Expression::Null => write!(f, "null")?,
            Expression::String(value) => write!(f, "\"{}\"", value)?,
            Expression::InterpolatedString(expressions) => {
                write!(f, "\"")?;
                for expression in expressions {
                    match expression.as_ref() {
                        Expression::String(value) => write!(f, "{}", value)?,
                        other => write!(f, "\\({})", other)?,
                    }
                }
                write!(f, "\"")?;
            }
            Expression::Integer(value) => write!(f, "{}", value)?,
            Expression::Float(value) => write!(f, "{}", value)?,
            Expression::Boolean(value) => write!(f, "{}", value)?,
            Expression::RegularExpression(value) => write!(f, "`{}`", value)?,
            Expression::Set(expressions) => {
                write!(f, "[")?;
                let mut add_comma = false;
                for expression in expressions {
                    if add_comma {
                        write!(f, ", ")?;
                    }
                    write!(f, "{}", expression)?;
                    add_comma = true;
                }
                write!(f, "]")?;
            }
            Expression::Function(name, arguments) => {
                write!(f, "{}(", name)?;
                let mut add_comma = false;
                for argument in arguments {
                    if add_comma {
                        write!(f, ", ")?;
                    }
                    write!(f, "{}", argument)?;
                    add_comma = true;
                }
                write!(f, ")")?;
            }
            Expression::Identifier(value) => write!(f, "{}", value)?,
            Expression::AllColumns => write!(f, "*")?,

            Expression::Not(expression) => write!(f, "!{}", expression)?,
            Expression::Plus(expression) => write!(f, "+{}", expression)?,
            Expression::Minus(expression) => write!(f, "-{}", expression)?,
            Expression::BitInverse(expression) => write!(f, "~{}", expression)?,

            Expression::Range(lhs, rhs) => write!(f, "{}..{}", lhs, rhs)?,
            Expression::Add(lhs, rhs) => write!(f, "{} + {}", lhs, rhs)?,
            Expression::Subtract(lhs, rhs) => write!(f, "{} - {}", lhs, rhs)?,
            Expression::Multiply(lhs, rhs) => write!(f, "{} * {}", lhs, rhs)?,
            Expression::Divide(lhs, rhs) => write!(f, "{} / {}", lhs, rhs)?,
            Expression::Modulo(lhs, rhs) => write!(f, "{} % {}", lhs, rhs)?,
            Expression::BitAnd(lhs, rhs) => write!(f, "{} & {}", lhs, rhs)?,
            Expression::BitOr(lhs, rhs) => write!(f, "{} | {}", lhs, rhs)?,
            Expression::BitXor(lhs, rhs) => write!(f, "{} ^ {}", lhs, rhs)?,
            Expression::BitShiftRight(lhs, rhs) => write!(f, "{} >> {}", lhs, rhs)?,
            Expression::BitShiftLeft(lhs, rhs) => write!(f, "{} << {}", lhs, rhs)?,

            Expression::Equal(lhs, rhs) => write!(f, "{} = {}", lhs, rhs)?,
            Expression::NotEqual(lhs, rhs) => write!(f, "{} != {}", lhs, rhs)?,
            Expression::Contains(lhs, rhs) => write!(f, "{} ~ {}", lhs, rhs)?,
            Expression::NotContains(lhs, rhs) => write!(f, "{} !~ {}", lhs, rhs)?,
            Expression::Lower(lhs, rhs) => write!(f, "{} < {}", lhs, rhs)?,
            Expression::LowerOrEqual(lhs, rhs) => write!(f, "{} <= {}", lhs, rhs)?,
            Expression::Greater(lhs, rhs) => write!(f, "{} > {}", lhs, rhs)?,
            Expression::GreaterOrEqual(lhs, rhs) => write!(f, "{} >= {}", lhs, rhs)?,

            Expression::And(lhs, rhs) => write!(f, "{} and {}", lhs, rhs)?,
            Expression::Or(lhs, rhs) => write!(f, "{} or {}", lhs, rhs)?,
        }

        Ok(())
    }
}

#[derive(PartialEq, Debug, Clone)]
pub enum Order {
    Asc(Arc<Expression>),
    Desc(Arc<Expression>),
}

#[derive(PartialEq, Debug, Clone)]
pub struct Select {
    pub selections: Vec<Selection>,
    pub distinct: bool,
}

#[derive(PartialEq, Debug, Clone)]
pub struct Selection {
    pub expression: Arc<Expression>,
    pub alias: Option<String>,
}

impl Selection {
    pub fn as_header(&self) -> String {
        match &self.alias {
            Some(alias) => alias.clone(),
            None => self.expression.to_string(),
        }
    }
}

#[derive(PartialEq, Debug, Copy, Clone)]
enum ParserState {
    Start,
    Select,
    Where,
    OrderBy,
    GroupBy,
    Having,
    Limit,
    Offset,
}

#[derive(PartialEq, Debug, Clone)]
struct Parser {
    tokens: Vec<Arc<Token>>,
    index: usize,
    states: Vec<ParserState>,
    statement: Statement,
}

impl Parser {
    fn new(tokens: Vec<Arc<Token>>) -> Self {
        Self {
            tokens,
            index: 0,
            states: vec![ParserState::Start],
            statement: Statement::new(),
        }
    }

    fn len(&self) -> usize {
        self.tokens.len()
    }

    fn get_token(&self) -> Arc<Token> {
        self.unsafe_token_at(self.index)
    }

    fn move_to_next_token(&mut self) -> Result<Arc<Token>> {
        if (self.index + 1) >= self.len() {
            Err(ASTError::NoMoreToken(self.index))
        } else {
            self.index += 1;
            Ok(self.get_token())
        }
    }

    fn next_token(&self) -> Option<Arc<Token>> {
        self.token_at(self.index + 1)
    }

    fn previous_token(&self) -> Option<Arc<Token>> {
        if self.index == 0 {
            None
        } else {
            self.token_at(self.index - 1)
        }
    }

    fn unsafe_token_at(&self, index: usize) -> Arc<Token> {
        self.tokens[index].clone()
    }

    fn token_at(&self, index: usize) -> Option<Arc<Token>> {
        if index >= self.len() {
            None
        } else {
            Some(self.unsafe_token_at(index))
        }
    }

    fn enter_state(&mut self, state: ParserState) {
        self.states.push(state);
    }

    fn leave_state(&mut self) -> Option<ParserState> {
        self.states.pop()
    }

    fn re_read_current_token(&mut self) {
        self.index -= 1;
    }

    fn is_initial_token(category: &TokenType) -> bool {
        match category {
            TokenType::Select
            | TokenType::Where
            | TokenType::OrderBy
            | TokenType::GroupBy
            | TokenType::Having
            | TokenType::Limit
            | TokenType::Offset
            | TokenType::Semicolon => true,
            _ => false,
        }
    }

    fn parse(&mut self) -> Result<()> {
        loop {
            let state = self.states.last();

            if let Some(state) = state {
                match state {
                    ParserState::Start => {
                        let continue_loop = self.parser_state_start()?;
                        if !continue_loop {
                            break;
                        }
                        Ok(())
                    }
                    ParserState::Select => self.parser_state_select(),
                    ParserState::Where => {
                        let expression = self.parse_expression()?;
                        self.statement.condition = Some(expression);
                        self.re_read_current_token();
                        self.leave_state();
                        Ok(())
                    }
                    ParserState::OrderBy => self.parser_state_order_by(),
                    ParserState::GroupBy => self.parser_state_group_by(),
                    ParserState::Having => {
                        let expression = self.parse_expression()?;
                        self.statement.having = Some(expression);
                        self.re_read_current_token();
                        self.leave_state();
                        Ok(())
                    }
                    ParserState::Limit => self.parser_state_limit(),
                    ParserState::Offset => self.parser_state_offset(),
                }?;
            }

            if let Err(ASTError::NoMoreToken(_)) = self.move_to_next_token() {
                break;
            }
        }

        if self.statement.select.is_none() {
            self.statement.select = Some(Select {
                selections: Vec::new(),
                distinct: false,
            })
        }

        Ok(())
    }

    fn parser_state_start(&mut self) -> Result<bool> {
        let token = self.get_token();

        match token.get_category() {
            TokenType::Directive(directive) => self.push_directive(directive.as_str()),
            TokenType::Select => self.enter_state(ParserState::Select),
            TokenType::Where => self.enter_state(ParserState::Where),
            TokenType::OrderBy => self.enter_state(ParserState::OrderBy),
            TokenType::GroupBy => self.enter_state(ParserState::GroupBy),
            TokenType::Having => self.enter_state(ParserState::Having),
            TokenType::Limit => self.enter_state(ParserState::Limit),
            TokenType::Offset => self.enter_state(ParserState::Offset),
            TokenType::Semicolon => return Ok(false),
            _ => {
                return Err(ASTError::UnknownInitialToken(token.clone()));
            }
        }

        Ok(true)
    }

    fn push_directive(&mut self, keyword: &str) {
        if let Some(directive) = Directive::from(keyword) {
            let mut directives = self.statement.directives.take().unwrap_or_else(|| vec![]);
            directives.push(directive);

            self.statement.directives = Some(directives);
        }
    }

    fn parser_state_select(&mut self) -> Result<()> {
        let token = self.get_token();

        let mut select = self.statement.select.take().unwrap_or_else(|| Select {
            selections: Vec::new(),
            distinct: false,
        });

        match token.get_category() {
            TokenType::Comma => {}
            TokenType::Distinct => {
                select.distinct = true;
            }
            TokenType::Asterisk => select.selections.push(Selection {
                expression: Expression::AllColumns.into(),
                alias: None,
            }),
            category => {
                if Self::is_initial_token(category) {
                    self.leave_state();
                    self.re_read_current_token();
                } else {
                    let expression = self.parse_expression()?;
                    let mut selection = Selection {
                        expression,
                        alias: None,
                    };

                    let as_alias = self.get_token();
                    match as_alias.get_category() {
                        TokenType::As => {
                            let alias = self.move_to_next_token()?;
                            match alias.get_category() {
                                TokenType::Identifier(identifier)
                                | TokenType::String(identifier) => {
                                    selection.alias = Some(identifier.clone())
                                }
                                _ => {
                                    return Err(ASTError::InvalidSelectionAlias(alias.clone()));
                                }
                            }
                        }
                        _ => self.re_read_current_token(),
                    }

                    select.selections.push(selection);
                }
            }
        }

        self.statement.select = Some(select);
        Ok(())
    }

    fn parse_expression(&mut self) -> Result<Arc<Expression>> {
        let mut values: Vec<Arc<Expression>> = Vec::new();
        let mut operators: Vec<Arc<Token>> = Vec::new();

        let mut parenthesis_count = 0;

        loop {
            let value_expression = self.parse_value_expression();
            let token = self.get_token();

            if let Ok(value_expression) = value_expression {
                values.push(Arc::new(value_expression));
            } else if token.get_category() == &TokenType::ParenthesisLeft {
                parenthesis_count += 1;
                operators.push(token.clone());
            } else if parenthesis_count > 0 && token.get_category() == &TokenType::ParenthesisRight
            {
                parenthesis_count -= 1;
                while operators.last().is_some()
                    && operators.last().unwrap().get_category() != &TokenType::ParenthesisLeft
                {
                    let expression =
                        Self::parse_binary_expression(operators.pop(), values.pop(), values.pop())?;
                    values.push(Arc::new(expression));
                }
                let _ = operators.pop();
            } else if Self::is_binary_operator(token.clone()) {
                while Self::has_precedence(token.clone(), operators.last()) {
                    let expression =
                        Self::parse_binary_expression(operators.pop(), values.pop(), values.pop())?;
                    values.push(Arc::new(expression));
                }

                operators.push(token.clone());
            } else {
                break;
            }

            if let Err(ASTError::NoMoreToken(_)) = self.move_to_next_token() {
                break;
            }
        }

        while let Some(operator) = operators.pop() {
            let expression =
                Self::parse_binary_expression(Some(operator), values.pop(), values.pop())?;
            values.push(Arc::new(expression));
        }

        values.pop().ok_or(ASTError::NoLeftBinaryExpression)
    }

    fn has_precedence(operator1: Arc<Token>, operator2: Option<&Arc<Token>>) -> bool {
        if let Some(operator2) = operator2 {
            let operator1_precedence =
                Self::binary_operator_precedence(operator1.get_category()).unwrap_or(0);
            let operator2_precedence =
                Self::binary_operator_precedence(operator2.get_category()).unwrap_or(0);

            return operator2_precedence >= operator1_precedence;
        }

        false
    }

    fn is_binary_operator(token: Arc<Token>) -> bool {
        match token.get_category() {
            TokenType::Range
            | TokenType::Plus
            | TokenType::Minus
            | TokenType::Asterisk
            | TokenType::Slash
            | TokenType::Percent
            | TokenType::Ampersand
            | TokenType::Pipe
            | TokenType::Caret
            | TokenType::BitShiftRight
            | TokenType::BitShiftLeft
            | TokenType::Equal
            | TokenType::NotEqual
            | TokenType::Tilde
            | TokenType::NotContains
            | TokenType::Greater
            | TokenType::GreaterOrEqual
            | TokenType::Lower
            | TokenType::LowerOrEqual
            | TokenType::And
            | TokenType::Or => true,
            _ => false,
        }
    }

    fn parse_binary_expression(
        token: Option<Arc<Token>>,
        right: Option<Arc<Expression>>,
        left: Option<Arc<Expression>>,
    ) -> Result<Expression> {
        if token.is_none() || left.is_none() || right.is_none() {
            return Err(ASTError::MissingPiecesOfBinaryExpression(
                token, left, right,
            ));
        }

        let token = token.unwrap();
        let left = left.unwrap();
        let right = right.unwrap();

        match token.get_category() {
            TokenType::Range => Ok(Expression::Range(left, right)),
            TokenType::Plus => Ok(Expression::Add(left, right)),
            TokenType::Minus => Ok(Expression::Subtract(left, right)),
            TokenType::Asterisk => Ok(Expression::Multiply(left, right)),
            TokenType::Slash => Ok(Expression::Divide(left, right)),
            TokenType::Percent => Ok(Expression::Modulo(left, right)),
            TokenType::Ampersand => Ok(Expression::BitAnd(left, right)),
            TokenType::Pipe => Ok(Expression::BitOr(left, right)),
            TokenType::Caret => Ok(Expression::BitXor(left, right)),
            TokenType::BitShiftRight => Ok(Expression::BitShiftRight(left, right)),
            TokenType::BitShiftLeft => Ok(Expression::BitShiftLeft(left, right)),

            TokenType::Equal => Ok(Expression::Equal(left, right)),
            TokenType::NotEqual => Ok(Expression::NotEqual(left, right)),
            TokenType::Tilde => Ok(Expression::Contains(left, right)),
            TokenType::NotContains => Ok(Expression::NotContains(left, right)),
            TokenType::Greater => Ok(Expression::Greater(left, right)),
            TokenType::GreaterOrEqual => Ok(Expression::GreaterOrEqual(left, right)),
            TokenType::Lower => Ok(Expression::Lower(left, right)),
            TokenType::LowerOrEqual => Ok(Expression::LowerOrEqual(left, right)),
            TokenType::And => Ok(Expression::And(left, right)),
            TokenType::Or => Ok(Expression::Or(left, right)),
            _ => Err(ASTError::InvalidBinaryExpression(
                token.clone(),
                left,
                right,
            )),
        }
    }

    /// The operator - is an unary minus if it is
    /// -> preceded by a left parenthesis
    /// -> preceded by another operator
    /// -> the first character of the input
    /// -> it is followed by either a left parenthesis, a value literal, or a column
    fn is_current_token_unary_operator(&self) -> bool {
        let previous = self.previous_token();
        let unary = self.get_token();
        let next = self.next_token();
        if next.is_none() {
            return false;
        }

        let next = next.unwrap();
        let next_category = next.get_category();

        let is_next_valid = match unary.get_category() {
            TokenType::Plus | TokenType::Minus => match next_category {
                TokenType::ParenthesisLeft | TokenType::Identifier(_) | TokenType::Number(_) => {
                    true
                }
                _ => false,
            },
            TokenType::Exclamation => match next_category {
                TokenType::ParenthesisLeft
                | TokenType::Identifier(_)
                | TokenType::True
                | TokenType::False => true,
                _ => false,
            },
            TokenType::Tilde => match next_category {
                TokenType::ParenthesisLeft | TokenType::Identifier(_) | TokenType::Number(_) => {
                    true
                }
                _ => false,
            },
            _ => false,
        };

        if !is_next_valid {
            return false;
        }

        if previous.is_none() {
            return true;
        }

        let previous = previous.unwrap();

        match previous.get_category() {
            TokenType::Identifier(_)
            | TokenType::Null
            | TokenType::Number(_)
            | TokenType::ParenthesisRight
            | TokenType::SquareBraceRight
            | TokenType::CurlyBraceRight
            | TokenType::RegularExpression(_)
            | TokenType::String(_)
            | TokenType::Limit
            | TokenType::Offset
            | TokenType::True
            | TokenType::False => false,
            _ => true,
        }
    }

    /// The larger the returned value, it can move left of last expression to be its own left, and place itself as left of the previous expression
    fn binary_operator_precedence(category: &TokenType) -> Option<u8> {
        match category {
            TokenType::Asterisk | TokenType::Slash | TokenType::Percent => Some(64),

            TokenType::Plus | TokenType::Minus => Some(32),

            TokenType::BitShiftRight | TokenType::BitShiftLeft => Some(16),

            TokenType::Ampersand | TokenType::Pipe | TokenType::Caret => Some(8),

            TokenType::Range => Some(6),

            TokenType::Equal
            | TokenType::NotEqual
            | TokenType::Tilde
            | TokenType::NotContains
            | TokenType::Greater
            | TokenType::GreaterOrEqual
            | TokenType::Lower
            | TokenType::LowerOrEqual => Some(4),
            TokenType::And => Some(2),
            TokenType::Or => Some(1),
            _ => None,
        }
    }

    fn parse_value_expression(&mut self) -> Result<Expression> {
        let token = self.get_token();

        let is_unary = self.is_current_token_unary_operator();
        let left = if is_unary {
            match token.get_category() {
                TokenType::Plus => self.maybe_parse_next_unary_expression(Expression::Plus),
                TokenType::Minus => self.maybe_parse_next_unary_expression(Expression::Minus),
                TokenType::Exclamation => self.maybe_parse_next_unary_expression(Expression::Not),
                TokenType::Tilde => self.maybe_parse_next_unary_expression(Expression::BitInverse),
                _ => None,
            }
        } else {
            None
        };

        if let Some(unary_expression) = left {
            return Ok(unary_expression);
        }

        match token.get_category() {
            TokenType::Null => Ok(Expression::Null),
            TokenType::True => Ok(Expression::Boolean(true)),
            TokenType::False => Ok(Expression::Boolean(false)),
            TokenType::RegularExpression(regex) => Ok(Expression::RegularExpression(regex.clone())),
            TokenType::InterpolatedStringOpen => Ok(Expression::InterpolatedString(
                self.parse_interpolated_string()?,
            )),
            TokenType::String(text) => Ok(Expression::String(text.clone())),
            TokenType::Number(Number::Integer(integer)) => Ok(Expression::Integer(*integer)),
            TokenType::Number(Number::Float(float)) => Ok(Expression::Float(*float)),
            TokenType::SquareBraceLeft => Ok(Expression::Set(self.parse_set_literal()?)),
            TokenType::Identifier(name) => {
                let next_token = self.next_token();
                if next_token.is_some()
                    && next_token.unwrap().get_category() == &TokenType::ParenthesisLeft
                {
                    Ok(Expression::Function(
                        name.clone(),
                        self.parse_function_call(token.clone())?,
                    ))
                } else {
                    Ok(Expression::Identifier(name.clone()))
                }
            }
            _ => Err(ASTError::UnknownExpressionToken(token.clone())),
        }
    }

    fn parse_next_value_expression(&mut self) -> Result<Expression> {
        let _next_token = self.move_to_next_token()?;
        self.parse_value_expression()
    }

    fn maybe_parse_next_unary_expression<F>(&mut self, unary: F) -> Option<Expression>
    where
        F: Fn(Arc<Expression>) -> Expression,
    {
        if let Ok(expression) = self.parse_next_value_expression() {
            return Some(unary(Arc::new(expression)));
        }

        self.index -= 1;
        None
    }

    fn parse_set_literal(&mut self) -> Result<Vec<Arc<Expression>>> {
        let mut expressions = Vec::new();

        loop {
            let token = self.move_to_next_token()?;

            match token.get_category() {
                TokenType::Comma => {}
                TokenType::SquareBraceRight => break,
                _ => {
                    let expression = self.parse_expression();

                    if let Ok(expression) = expression {
                        self.index -= 1;
                        expressions.push(expression);
                    } else {
                        break;
                    }
                }
            };
        }

        Ok(expressions)
    }

    fn parse_interpolated_string(&mut self) -> Result<Vec<Arc<Expression>>> {
        let mut expressions = Vec::new();

        loop {
            let token = self.move_to_next_token()?;

            match token.get_category() {
                TokenType::StringExpressionOpen | TokenType::StringExpressionClose => {}
                TokenType::InterpolatedStringClose => break,
                _ => {
                    let expression = self.parse_expression();

                    if let Ok(expression) = expression {
                        self.index -= 1;
                        expressions.push(expression);
                    } else {
                        break;
                    }
                }
            };
        }

        Ok(expressions)
    }

    fn parse_function_call(&mut self, identifier: Arc<Token>) -> Result<Vec<Arc<Argument>>> {
        let mut expressions = Vec::new();

        let parenthesis = self.move_to_next_token()?;
        match parenthesis.get_category() {
            TokenType::ParenthesisLeft => {}
            _ => {
                return Err(ASTError::InvalidFunctionCall(
                    identifier.clone(),
                    parenthesis.clone(),
                ));
            }
        }

        while let Some(argument) = self.parse_function_argument()? {
            expressions.push(Arc::new(argument));
        }

        Ok(expressions)
    }

    fn parse_function_argument(&mut self) -> Result<Option<Argument>> {
        let token = self.move_to_next_token()?;
        let name = match token.get_category() {
            TokenType::ParenthesisRight => return Ok(None),
            TokenType::Identifier(name) => {
                let token = self.move_to_next_token()?;
                if token.get_category() != &TokenType::Colon {
                    self.re_read_current_token();
                    self.re_read_current_token();
                    None
                } else {
                    Some(name.clone())
                }
            }
            _ => {
                self.re_read_current_token();
                None
            }
        };

        let _token = self.move_to_next_token()?;
        let expression = self.parse_expression();

        self.re_read_current_token();
        let argument = if let Ok(expression) = expression {
            Some(Argument::new(name, expression))
        } else if let Some(identifier) = name {
            Some(Argument::unnamed(Arc::new(Expression::Identifier(
                identifier,
            ))))
        } else {
            self.index += 1;
            None
        };

        let token = self.move_to_next_token()?;
        if token.get_category() != &TokenType::Comma {
            self.re_read_current_token();
        }

        Ok(argument)
    }

    fn parser_state_order_by(&mut self) -> Result<()> {
        let mut orders = Vec::new();
        let mut expressions = Vec::new();

        loop {
            let token = self.get_token();
            match token.get_category() {
                TokenType::Comma | TokenType::Asc => {
                    if let Some(expression) = expressions.pop() {
                        orders.push(Order::Asc(expression));
                    }
                }
                TokenType::Desc => {
                    if let Some(expression) = expressions.pop() {
                        orders.push(Order::Desc(expression));
                    }
                }
                category => {
                    if Self::is_initial_token(category) {
                        self.re_read_current_token();
                        break;
                    }

                    let expression = self.parse_expression();

                    if let Ok(expression) = expression {
                        self.index -= 1;
                        expressions.push(expression);
                    } else {
                        break;
                    }
                }
            };

            if let Err(ASTError::NoMoreToken(_)) = self.move_to_next_token() {
                break;
            }
        }

        while let Some(expression) = expressions.pop() {
            orders.push(Order::Asc(expression));
        }

        self.statement.order = Some(orders);
        self.leave_state();

        Ok(())
    }

    fn parser_state_group_by(&mut self) -> Result<()> {
        let mut expressions = Vec::new();

        loop {
            let token = self.get_token();
            match token.get_category() {
                TokenType::Comma => {}
                category => {
                    if Self::is_initial_token(category) {
                        self.re_read_current_token();
                        break;
                    }

                    let expression = self.parse_expression();

                    if let Ok(expression) = expression {
                        expressions.push(expression);
                        self.re_read_current_token();
                    } else {
                        break;
                    }
                }
            };

            if let Err(ASTError::NoMoreToken(_)) = self.move_to_next_token() {
                break;
            }
        }

        self.statement.group = Some(expressions);
        self.leave_state();
        Ok(())
    }

    fn parser_state_limit(&mut self) -> Result<()> {
        let token = self.get_token();

        match token.get_category() {
            TokenType::Number(number) => {
                let limit = Self::extract_integer(number);

                if limit.is_none() || limit.unwrap() == 0 {
                    return Err(ASTError::InvalidLimitToken(token.clone()));
                }

                self.statement.limit = Some(limit.unwrap());
            }
            category => {
                if Self::is_initial_token(category) && self.statement.limit.is_none() {
                    self.re_read_current_token();
                } else {
                    return Err(ASTError::UnknownLimitToken(token.clone()));
                }
            }
        }

        self.leave_state();
        Ok(())
    }

    fn parser_state_offset(&mut self) -> Result<()> {
        let token = self.get_token();

        match token.get_category() {
            TokenType::Number(number) => {
                let limit = Self::extract_integer(number);

                if limit.is_none() {
                    return Err(ASTError::InvalidOffsetToken(token.clone()));
                }

                self.statement.offset = Some(limit.unwrap());
            }
            category => {
                if Self::is_initial_token(category) && self.statement.offset.is_none() {
                    self.re_read_current_token();
                } else {
                    return Err(ASTError::UnknownOffsetToken(token.clone()));
                }
            }
        }

        self.leave_state();
        Ok(())
    }

    fn extract_integer(number: &Number) -> Option<u64> {
        match number {
            Number::Integer(integer) => Some(*integer),
            _ => None,
        }
    }
}

pub fn generate_ast(gql: &str) -> Result<Statement> {
    let tokens = gql::parse((gql.to_owned() + ";").as_str());
    let tokens: Vec<Arc<Token>> = tokens.into_iter().map(Arc::new).collect();

    let mut parser = Parser::new(tokens);
    parser.parse()?;

    Ok(replace(&mut parser.statement, Statement::new()))
}

#[cfg(test)]
mod tests {
    use super::*;

    const SELECT: u32 = 0x001;
    const CONDITION: u32 = 0x002;
    const ORDER: u32 = 0x004;
    const GROUP: u32 = 0x008;
    const LIMIT: u32 = 0x010;
    const OFFSET: u32 = 0x020;
    const HAVING: u32 = 0x040;
    const DIRECTIVES: u32 = 0x080;

    enum StatementProperty {
        Directives(Option<Vec<Directive>>),
        Select(Option<Select>),
        Where(Option<Arc<Expression>>),
        Order(Option<Vec<Order>>),
        Group(Option<Vec<Arc<Expression>>>),
        Having(Option<Arc<Expression>>),
        Limit(Option<u64>),
        Offset(Option<u64>),
    }

    fn assert_single_statement_option(statement: Statement, option: StatementProperty) {
        assert_statement_option(statement, vec![option]);
    }

    fn assert_statement_option(statement: Statement, options: Vec<StatementProperty>) {
        let mut option_index: u32 = 0;

        for option in options.iter() {
            match option {
                StatementProperty::Select(select) => {
                    assert_eq!(statement.select, *select, "\n\nSelect does not match.\n-----RESULT: \n\n{:?}\n\n-----EXPECTED: \n\n{:?}\n\n", statement.select, select);
                    option_index |= SELECT;
                }
                StatementProperty::Where(condition) => {
                    assert_eq!(statement.condition, *condition, "\n\nWhere does not match.\n-----RESULT: \n\n{:?}\n\n-----EXPECTED: \n\n{:?}\n\n", statement.condition, condition);
                    option_index |= CONDITION;
                }
                StatementProperty::Order(order) => {
                    assert_eq!(statement.order, *order, "\n\nOrder by does not match.\n-----RESULT: \n\n{:?}\n\n-----EXPECTED: \n\n{:?}\n\n", statement.order, order);
                    option_index |= ORDER;
                }
                StatementProperty::Group(group) => {
                    assert_eq!(statement.group, *group, "\n\nGroup by does not match.\n-----RESULT: \n\n{:?}\n\n-----EXPECTED: \n\n{:?}\n\n", statement.group, group);
                    option_index |= GROUP;
                }
                StatementProperty::Limit(limit) => {
                    assert_eq!(statement.limit, *limit, "\n\nLimit does not match.\n-----RESULT: \n\n{:?}\n\n-----EXPECTED: \n\n{:?}\n\n", statement.limit, limit);
                    option_index |= LIMIT;
                }
                StatementProperty::Offset(offset) => {
                    assert_eq!(statement.offset, *offset, "\n\nOffset does not match.\n-----RESULT: \n\n{:?}\n\n-----EXPECTED: \n\n{:?}\n\n", statement.offset, offset);
                    option_index |= OFFSET;
                }
                StatementProperty::Having(having) => {
                    assert_eq!(statement.having, *having, "\n\nHaving does not match.\n-----RESULT: \n\n{:?}\n\n-----EXPECTED: \n\n{:?}\n\n", statement.having, having);
                    option_index |= HAVING;
                }
                StatementProperty::Directives(directives) => {
                    assert_eq!(statement.directives, *directives, "\n\nDirectives does not match.\n-----RESULT: \n\n{:?}\n\n-----EXPECTED: \n\n{:?}\n\n", statement.directives, directives);
                    option_index |= DIRECTIVES;
                }
            }
        }

        if (option_index & SELECT) == 0 {
            assert_eq!(
                statement.select,
                Some(Select {
                    selections: Vec::new(),
                    distinct: false,
                }),
                "\n\nSelect must be ALL.\n-----RESULT: \n\n{:?}\n\n",
                statement.select
            );
        }
        if (option_index & CONDITION) == 0 {
            assert_eq!(
                statement.condition, None,
                "\n\nWhere must be NONE.\n-----RESULT: \n\n{:?}\n\n",
                statement.condition
            );
        }
        if (option_index & ORDER) == 0 {
            assert_eq!(
                statement.order, None,
                "\n\nOrder by must be NONE.\n-----RESULT: \n\n{:?}\n\n",
                statement.order
            );
        }
        if (option_index & GROUP) == 0 {
            assert_eq!(
                statement.group, None,
                "\n\nGroup by must be NONE.\n-----RESULT: \n\n{:?}\n\n",
                statement.group
            );
        }
        if (option_index & LIMIT) == 0 {
            assert_eq!(
                statement.limit, None,
                "\n\nLimit must be NONE.\n-----RESULT: \n\n{:?}\n\n",
                statement.limit
            );
        }
        if (option_index & OFFSET) == 0 {
            assert_eq!(
                statement.offset, None,
                "\n\nOffset must be NONE.\n-----RESULT: \n\n{:?}\n\n",
                statement.offset
            );
        }
        if (option_index & HAVING) == 0 {
            assert_eq!(
                statement.having, None,
                "\n\nHaving must be NONE.\n-----RESULT: \n\n{:?}\n\n",
                statement.having
            );
        }
        if (option_index & DIRECTIVES) == 0 {
            assert_eq!(
                statement.directives, None,
                "\n\nDirectives must be NONE.\n-----RESULT: \n\n{:?}\n\n",
                statement.directives
            );
        }
    }

    #[test]
    fn test_string_interpolation() -> Result<()> {
        let statement = generate_ast(r#"where branch ~ "by \(author_name)!";"#)?;

        let branch = Arc::new(Expression::Identifier("branch".to_owned()));
        let string = Arc::new(Expression::InterpolatedString(vec![
            Arc::new(Expression::String("by ".to_owned())),
            Arc::new(Expression::Identifier("author_name".to_owned())),
            Arc::new(Expression::String("!".to_owned())),
        ]));

        let contains = Arc::new(Expression::Contains(branch, string));

        assert_single_statement_option(statement, StatementProperty::Where(Some(contains)));
        Ok(())
    }

    #[test]
    fn test_double_string_interpolation() -> Result<()> {
        let statement = generate_ast(
            r#"
            where
                branch ~
                    "by \(author_name), \(author_email + date)"
                and date < days(10);
            "#,
        )?;

        let branch = Arc::new(Expression::Identifier("branch".to_owned()));
        let string = Arc::new(Expression::InterpolatedString(vec![
            Arc::new(Expression::String("by ".to_owned())),
            Arc::new(Expression::Identifier("author_name".to_owned())),
            Arc::new(Expression::String(", ".to_owned())),
            Arc::new(Expression::Add(
                Arc::new(Expression::Identifier("author_email".to_owned())),
                Arc::new(Expression::Identifier("date".to_owned())),
            )),
            Arc::new(Expression::String("".to_owned())),
        ]));
        let contains = Arc::new(Expression::Contains(branch, string));

        let date = Arc::new(Expression::Identifier("date".to_owned()));
        let days = Arc::new(Expression::Function(
            "days".to_owned(),
            vec![Arc::new(Argument::unnamed(Arc::new(Expression::Integer(
                10,
            ))))],
        ));
        let lower = Arc::new(Expression::Lower(date, days));

        let and = Arc::new(Expression::And(contains, lower));

        assert_single_statement_option(statement, StatementProperty::Where(Some(and)));

        Ok(())
    }

    #[test]
    fn test_limit() -> Result<()> {
        let statement = generate_ast("limit 10;")?;
        assert_single_statement_option(statement, StatementProperty::Limit(Some(10)));

        let statement = generate_ast("select * limit 100;")?;
        assert_statement_option(
            statement,
            vec![
                StatementProperty::Select(Some(Select {
                    selections: vec![Selection {
                        expression: Arc::new(Expression::AllColumns),
                        alias: None,
                    }],
                    distinct: false,
                })),
                StatementProperty::Limit(Some(100)),
            ],
        );

        Ok(())
    }

    #[test]
    fn test_wrong_limit() {
        let statement = generate_ast("limit -10;");

        assert_eq!(statement.is_err(), true);
        let error = statement.unwrap_err();
        match error {
            ASTError::UnknownLimitToken(token) => {
                assert_eq!(token.get_range(), &(6..7));
                assert_eq!(token.get_category(), &TokenType::Minus);
            }
            _ => panic!("Expected ASTError::UnknownLimitToken, but got {:?}", error),
        }

        let statement = generate_ast("limit 10.5;");

        assert_eq!(statement.is_err(), true);
        let error = statement.unwrap_err();
        match error {
            ASTError::InvalidLimitToken(token) => {
                assert_eq!(token.get_range(), &(5..10));
                assert_eq!(
                    token.get_category(),
                    &TokenType::Number(Number::Float(10.5))
                );
            }
            _ => panic!("Expected ASTError::InvalidLimitToken, but got {:?}", error),
        }
    }

    #[test]
    fn test_offset() -> Result<()> {
        let statement = generate_ast("offset 10;")?;
        assert_single_statement_option(statement, StatementProperty::Offset(Some(10)));

        let statement = generate_ast("limit 1_000_000 offset 100;")?;
        assert_statement_option(
            statement,
            vec![
                StatementProperty::Limit(Some(1_000_000)),
                StatementProperty::Offset(Some(100)),
            ],
        );

        let statement = generate_ast("select commit limit 10 offset 100;")?;
        assert_statement_option(
            statement,
            vec![
                StatementProperty::Select(Some(Select {
                    selections: vec![Selection {
                        expression: Arc::new(Expression::Identifier("commit".to_owned())),
                        alias: None,
                    }],
                    distinct: false,
                })),
                StatementProperty::Limit(Some(10)),
                StatementProperty::Offset(Some(100)),
            ],
        );

        Ok(())
    }

    #[test]
    fn test_semicolon() -> Result<()> {
        let statement = generate_ast("limit 10; limit 100 offset 100")?;
        assert_single_statement_option(statement, StatementProperty::Limit(Some(10)));

        Ok(())
    }

    #[test]
    fn test_select() -> Result<()> {
        let statement = generate_ast("select distinct commit as 'commit';")?;
        assert_single_statement_option(
            statement,
            StatementProperty::Select(Some(Select {
                selections: vec![Selection {
                    expression: Arc::new(Expression::Identifier("commit".to_owned())),
                    alias: Some("commit".to_owned()),
                }],
                distinct: true,
            })),
        );

        let statement = generate_ast("select 10 % 3 as modulo_operation;")?;
        assert_single_statement_option(
            statement,
            StatementProperty::Select(Some(Select {
                selections: vec![Selection {
                    expression: Arc::new(Expression::Modulo(
                        Arc::new(Expression::Integer(10)),
                        Arc::new(Expression::Integer(3)),
                    )),
                    alias: Some("modulo_operation".to_owned()),
                }],
                distinct: false,
            })),
        );

        Ok(())
    }

    #[test]
    fn test_comparison_with_set_expressions() -> Result<()> {
        let statement = generate_ast("where is_merge_commit ~ [branch != 'name', false];")?;

        let column = Arc::new(Expression::Identifier("is_merge_commit".to_owned()));
        let first = Arc::new(Expression::NotEqual(
            Arc::new(Expression::Identifier("branch".to_owned())),
            Arc::new(Expression::String("name".to_owned())),
        ));
        let second = Arc::new(Expression::Boolean(false));

        let set = Arc::new(Expression::Set(vec![first, second]));

        let condition = Arc::new(Expression::Contains(column, set));
        assert_single_statement_option(statement, StatementProperty::Where(Some(condition)));

        Ok(())
    }

    #[test]
    fn test_comparison_precedence() -> Result<()> {
        let statement = generate_ast("where is_merge_commit or branch = 'master' and false;")?;

        let is_merge_commit = Arc::new(Expression::Identifier("is_merge_commit".to_owned()));
        let equal = Arc::new(Expression::Equal(
            Arc::new(Expression::Identifier("branch".to_owned())),
            Arc::new(Expression::String("master".to_owned())),
        ));
        let boolean = Arc::new(Expression::Boolean(false));

        let and = Arc::new(Expression::And(equal, boolean));
        let or = Arc::new(Expression::Or(is_merge_commit, and));

        assert_single_statement_option(statement, StatementProperty::Where(Some(or)));

        let statement = generate_ast("where (is_merge_commit or branch = 'master') and false;")?;

        let is_merge_commit = Arc::new(Expression::Identifier("is_merge_commit".to_owned()));
        let equal = Arc::new(Expression::Equal(
            Arc::new(Expression::Identifier("branch".to_owned())),
            Arc::new(Expression::String("master".to_owned())),
        ));
        let boolean = Arc::new(Expression::Boolean(false));

        let or = Arc::new(Expression::Or(is_merge_commit, equal));
        let and = Arc::new(Expression::And(or, boolean));

        assert_single_statement_option(statement, StatementProperty::Where(Some(and)));

        Ok(())
    }

    #[test]
    fn test_mathematical_operations() -> Result<()> {
        let statement = generate_ast("where +4+2*3+4/5 = -10*-1;")?;
        let value1 = Arc::new(Expression::Plus(Arc::new(Expression::Integer(4))));
        let value2 = Arc::new(Expression::Integer(2));
        let value3 = Arc::new(Expression::Integer(3));
        let value4 = Arc::new(Expression::Integer(4));
        let value5 = Arc::new(Expression::Integer(5));
        let value6 = Arc::new(Expression::Integer(10));
        let value7 = Arc::new(Expression::Integer(1));

        let multiply = Arc::new(Expression::Multiply(value2, value3));
        let divide = Arc::new(Expression::Divide(value4, value5));
        let add = Arc::new(Expression::Add(value1, multiply));
        let add = Arc::new(Expression::Add(add, divide));

        let value6 = Arc::new(Expression::Minus(value6));
        let value7 = Arc::new(Expression::Minus(value7));

        let result = Arc::new(Expression::Multiply(value6, value7));

        let condition = Arc::new(Expression::Equal(add, result));
        assert_single_statement_option(statement, StatementProperty::Where(Some(condition)));

        Ok(())
    }

    #[test]
    fn test_mathematical_operations_with_parenthesis() -> Result<()> {
        let statement = generate_ast("where +4+(2*(3+4))/5 != (-10*-1);")?;
        let value1 = Arc::new(Expression::Plus(Arc::new(Expression::Integer(4))));
        let value2 = Arc::new(Expression::Integer(2));
        let value3 = Arc::new(Expression::Integer(3));
        let value4 = Arc::new(Expression::Integer(4));
        let value5 = Arc::new(Expression::Integer(5));
        let value6 = Arc::new(Expression::Integer(10));
        let value7 = Arc::new(Expression::Integer(1));

        let add = Arc::new(Expression::Add(value3, value4));
        let multiply = Arc::new(Expression::Multiply(value2, add));
        let divide = Arc::new(Expression::Divide(multiply, value5));
        let add = Arc::new(Expression::Add(value1, divide));

        let value6 = Arc::new(Expression::Minus(value6));
        let value7 = Arc::new(Expression::Minus(value7));

        let result = Arc::new(Expression::Multiply(value6, value7));

        let condition = Arc::new(Expression::NotEqual(add, result));
        assert_single_statement_option(statement, StatementProperty::Where(Some(condition)));

        Ok(())
    }

    #[test]
    fn test_simple_condition() -> Result<()> {
        let statement = generate_ast("where branch = 'master';")?;

        let column = Arc::new(Expression::Identifier("branch".to_owned()));
        let value = Arc::new(Expression::String("master".to_owned()));

        let condition = Arc::new(Expression::Equal(column, value));

        assert_single_statement_option(statement, StatementProperty::Where(Some(condition)));

        Ok(())
    }

    #[test]
    fn test_simple_and_or_condition_with_parenthesis() -> Result<()> {
        let statement = generate_ast(
            "where (branch = 'master' or branch = 'develop') and author !~ 'Rafael';",
        )?;

        let first_column = Arc::new(Expression::Identifier("branch".to_owned()));
        let first_value = Arc::new(Expression::String("master".to_owned()));

        let second_column = Arc::new(Expression::Identifier("branch".to_owned()));
        let second_value = Arc::new(Expression::String("develop".to_owned()));

        let third_column = Arc::new(Expression::Identifier("author".to_owned()));
        let third_value = Arc::new(Expression::String("Rafael".to_owned()));

        let first = Arc::new(Expression::Equal(first_column, first_value));
        let second = Arc::new(Expression::Equal(second_column, second_value));
        let third = Arc::new(Expression::NotContains(third_column, third_value));

        let condition = Arc::new(Expression::And(
            Arc::new(Expression::Or(first, second)),
            third,
        ));

        assert_single_statement_option(statement, StatementProperty::Where(Some(condition)));

        let statement = generate_ast(
            "where branch = 'master' and (branch = 'develop' or author !~ 'Rafael');",
        )?;

        let first_column = Arc::new(Expression::Identifier("branch".to_owned()));
        let first_value = Arc::new(Expression::String("master".to_owned()));

        let second_column = Arc::new(Expression::Identifier("branch".to_owned()));
        let second_value = Arc::new(Expression::String("develop".to_owned()));

        let third_column = Arc::new(Expression::Identifier("author".to_owned()));
        let third_value = Arc::new(Expression::String("Rafael".to_owned()));

        let first = Arc::new(Expression::Equal(first_column, first_value));
        let second = Arc::new(Expression::Equal(second_column, second_value));
        let third = Arc::new(Expression::NotContains(third_column, third_value));

        let condition = Arc::new(Expression::And(
            first,
            Arc::new(Expression::Or(second, third)),
        ));

        assert_single_statement_option(statement, StatementProperty::Where(Some(condition)));

        Ok(())
    }

    #[test]
    fn test_and_condition_with_set() -> Result<()> {
        let statement = generate_ast(
            "where branch !~ ['test', 'develop', author + '-poc'] and author ~ `[^(@jostle.me)]`;",
        )?;

        let column = Arc::new(Expression::Identifier("branch".to_owned()));
        let first = Arc::new(Expression::String("test".to_owned()));
        let second = Arc::new(Expression::String("develop".to_owned()));
        let third = Arc::new(Expression::Add(
            Arc::new(Expression::Identifier("author".to_owned())),
            Arc::new(Expression::String("-poc".to_owned())),
        ));
        let value = Arc::new(Expression::Set(vec![first, second, third]));
        let first_condition = Arc::new(Expression::NotContains(column, value));

        let column = Arc::new(Expression::Identifier("author".to_owned()));
        let value = Arc::new(Expression::RegularExpression("[^(@jostle.me)]".to_owned()));
        let second_condition = Arc::new(Expression::Contains(column, value));

        let condition = Arc::new(Expression::And(first_condition, second_condition));

        assert_single_statement_option(statement, StatementProperty::Where(Some(condition)));

        let statement = generate_ast(
            "where author ~ `[^(@jostle.me)]` and branch !~ ['test', 'develop', author + '-poc'];",
        )?;

        let column = Arc::new(Expression::Identifier("author".to_owned()));
        let value = Arc::new(Expression::RegularExpression("[^(@jostle.me)]".to_owned()));
        let first_condition = Arc::new(Expression::Contains(column, value));

        let column = Arc::new(Expression::Identifier("branch".to_owned()));
        let first = Arc::new(Expression::String("test".to_owned()));
        let second = Arc::new(Expression::String("develop".to_owned()));
        let third = Arc::new(Expression::Add(
            Arc::new(Expression::Identifier("author".to_owned())),
            Arc::new(Expression::String("-poc".to_owned())),
        ));
        let value = Arc::new(Expression::Set(vec![first, second, third]));
        let second_condition = Arc::new(Expression::NotContains(column, value));

        let condition = Arc::new(Expression::And(first_condition, second_condition));

        assert_single_statement_option(statement, StatementProperty::Where(Some(condition)));

        Ok(())
    }

    #[test]
    fn test_or_condition() -> Result<()> {
        let statement = generate_ast("where branch ~ '-poc' or author ~ `[^(@jostle.me)]`;")?;

        let column = Arc::new(Expression::Identifier("branch".to_owned()));
        let first = Arc::new(Expression::String("-poc".to_owned()));

        let first_condition = Arc::new(Expression::Contains(column, first));

        let column = Arc::new(Expression::Identifier("author".to_owned()));
        let value = Arc::new(Expression::RegularExpression("[^(@jostle.me)]".to_owned()));
        let second_condition = Arc::new(Expression::Contains(column, value));

        let condition = Arc::new(Expression::Or(first_condition, second_condition));

        assert_single_statement_option(statement, StatementProperty::Where(Some(condition)));

        Ok(())
    }

    #[test]
    fn test_simple_function_call() -> Result<()> {
        let statement = generate_ast("where abs(-1) = 1")?;

        let function = Arc::new(Expression::Function(
            "abs".to_owned(),
            vec![Arc::new(Argument::unnamed(Arc::new(Expression::Minus(
                Arc::new(Expression::Integer(1)),
            ))))],
        ));
        let result = Arc::new(Expression::Integer(1));

        let condition = Arc::new(Expression::Equal(function, result));

        assert_single_statement_option(statement, StatementProperty::Where(Some(condition)));

        Ok(())
    }

    #[test]
    fn test_complex_function_call_with_parenthesis() -> Result<()> {
        let statement =
            generate_ast("select count() as commit_count, date where abs(seconds(from: date, to: ('2019-02-08' + days(10)))) > 800_000 order by date desc")?;

        let column = Arc::new(Expression::Identifier("date".to_owned()));
        let date = Arc::new(Expression::String("2019-02-08".to_owned()));
        let ten = Arc::new(Expression::Integer(10));
        let days_function = Arc::new(Expression::Function(
            "days".to_owned(),
            vec![Arc::new(Argument::unnamed(ten))],
        ));

        let add = Arc::new(Expression::Add(date, days_function));

        let between = Arc::new(Expression::Function(
            "seconds".to_owned(),
            vec![
                Arc::new(Argument::named("from", column)),
                Arc::new(Argument::named("to", add)),
            ],
        ));
        let abs = Arc::new(Expression::Function(
            "abs".to_owned(),
            vec![Arc::new(Argument::unnamed(between))],
        ));

        let result = Arc::new(Expression::Integer(800_000));

        let condition = Arc::new(Expression::Greater(abs, result));

        let select = Select {
            selections: vec![
                Selection {
                    expression: Arc::new(Expression::Function("count".to_owned(), vec![])),
                    alias: Some("commit_count".to_owned()),
                },
                Selection {
                    expression: Arc::new(Expression::Identifier("date".to_owned())),
                    alias: None,
                },
            ],
            distinct: false,
        };
        let order = Order::Desc(Arc::new(Expression::Identifier("date".to_owned())));

        assert_statement_option(
            statement,
            vec![
                StatementProperty::Select(Some(select)),
                StatementProperty::Where(Some(condition)),
                StatementProperty::Order(Some(vec![order])),
            ],
        );

        Ok(())
    }

    #[test]
    fn test_nested_function_calls() -> Result<()> {
        let statement = generate_ast("select max(max(added_files), 10)")?;

        let column = Arc::new(Expression::Identifier("added_files".to_owned()));
        let ten = Arc::new(Expression::Integer(10));
        let first = Arc::new(Expression::Function(
            "max".to_owned(),
            vec![Arc::new(Argument::unnamed(column))],
        ));
        let max = Arc::new(Expression::Function(
            "max".to_owned(),
            vec![
                Arc::new(Argument::unnamed(first)),
                Arc::new(Argument::unnamed(ten)),
            ],
        ));

        let select = Select {
            selections: vec![Selection {
                alias: None,
                expression: max,
            }],
            distinct: false,
        };

        assert_single_statement_option(statement, StatementProperty::Select(Some(select)));

        Ok(())
    }

    #[test]
    fn test_order_by() -> Result<()> {
        let statement = generate_ast("order by branch, commit asc, date desc")?;

        let column = Arc::new(Expression::Identifier("branch".to_owned()));
        let first = Order::Asc(column);

        let column = Arc::new(Expression::Identifier("commit".to_owned()));
        let second = Order::Asc(column);

        let column = Arc::new(Expression::Identifier("date".to_owned()));
        let third = Order::Desc(column);

        let orders = vec![first, second, third];

        assert_single_statement_option(statement, StatementProperty::Order(Some(orders)));

        Ok(())
    }

    #[test]
    fn test_simple_group_by() -> Result<()> {
        let statement = generate_ast("group by repository, branch, author")?;

        let repository = Arc::new(Expression::Identifier("repository".to_owned()));
        let branch = Arc::new(Expression::Identifier("branch".to_owned()));
        let author = Arc::new(Expression::Identifier("author".to_owned()));

        let group = vec![repository, branch, author];

        assert_single_statement_option(statement, StatementProperty::Group(Some(group)));

        let statement = generate_ast("group by repository, branch, author order by date desc")?;

        let repository = Arc::new(Expression::Identifier("repository".to_owned()));
        let branch = Arc::new(Expression::Identifier("branch".to_owned()));
        let author = Arc::new(Expression::Identifier("author".to_owned()));

        let group = vec![repository, branch, author];
        let order = Order::Desc(Arc::new(Expression::Identifier("date".to_owned())));

        assert_statement_option(
            statement,
            vec![
                StatementProperty::Group(Some(group)),
                StatementProperty::Order(Some(vec![order])),
            ],
        );

        let statement = generate_ast("order by date desc group by repository, branch, author;")?;

        let repository = Arc::new(Expression::Identifier("repository".to_owned()));
        let branch = Arc::new(Expression::Identifier("branch".to_owned()));
        let author = Arc::new(Expression::Identifier("author".to_owned()));

        let group = vec![repository, branch, author];
        let order = Order::Desc(Arc::new(Expression::Identifier("date".to_owned())));

        assert_statement_option(
            statement,
            vec![
                StatementProperty::Group(Some(group)),
                StatementProperty::Order(Some(vec![order])),
            ],
        );

        Ok(())
    }

    #[test]
    fn test_simple_having() -> Result<()> {
        let statement = generate_ast("group by branch having count() >= 100 order by date;")?;

        let branch = Arc::new(Expression::Identifier("branch".to_owned()));
        let group = vec![branch];

        let count = Arc::new(Expression::Function("count".to_owned(), vec![]));
        let condition = Arc::new(Expression::GreaterOrEqual(
            count,
            Arc::new(Expression::Integer(100)),
        ));

        let order = Order::Asc(Arc::new(Expression::Identifier("date".to_owned())));

        assert_statement_option(
            statement,
            vec![
                StatementProperty::Group(Some(group)),
                StatementProperty::Having(Some(condition)),
                StatementProperty::Order(Some(vec![order])),
            ],
        );

        Ok(())
    }

    #[test]
    fn test_directives() -> Result<()> {
        let statement = generate_ast("#thread2 #no_fetch where branch ~ 'master'")?;

        let branch = Arc::new(Expression::Identifier("branch".to_owned()));
        let value = Arc::new(Expression::String("master".to_owned()));

        let condition = Arc::new(Expression::Contains(branch, value));

        let directives = vec![Directive::Thread(2), Directive::NoFetch];

        assert_statement_option(
            statement,
            vec![
                StatementProperty::Directives(Some(directives)),
                StatementProperty::Where(Some(condition)),
            ],
        );

        Ok(())
    }

    #[test]
    fn test_full_select_statement() -> Result<()> {
        let statement = generate_ast(
            r#"
            select branch, max(date) as latest_date
            where author !~ [
                "Rafael Guerreiro",
                "Jon Snow"
            ]
            order by latest_date
            group by branch
            having max(date) < '2018-01-01T00:00:00Z'
            limit 25
            offset 50;
        "#,
        )?;

        let branch = Arc::new(Expression::Identifier("branch".to_owned()));
        let max_date = Arc::new(Expression::Function(
            "max".to_owned(),
            vec![Arc::new(Argument::unnamed(Arc::new(
                Expression::Identifier("date".to_owned()),
            )))],
        ));

        let select = Select {
            selections: vec![
                Selection {
                    expression: branch.clone(),
                    alias: None,
                },
                Selection {
                    expression: max_date.clone(),
                    alias: Some("latest_date".to_owned()),
                },
            ],
            distinct: false,
        };

        let author = Arc::new(Expression::Identifier("author".to_owned()));
        let set = Arc::new(Expression::Set(vec![
            Arc::new(Expression::String("Rafael Guerreiro".to_owned())),
            Arc::new(Expression::String("Jon Snow".to_owned())),
        ]));

        let condition = Arc::new(Expression::NotContains(author.clone(), set.clone()));

        let order = vec![Order::Asc(Arc::new(Expression::Identifier(
            "latest_date".to_owned(),
        )))];

        let group = vec![branch.clone()];
        let having_left = max_date.clone();
        let having_right = Arc::new(Expression::String("2018-01-01T00:00:00Z".to_owned()));

        let having = Arc::new(Expression::Lower(having_left, having_right));

        let limit = 25;
        let offset = 50;

        assert_statement_option(
            statement,
            vec![
                StatementProperty::Select(Some(select)),
                StatementProperty::Where(Some(condition)),
                StatementProperty::Order(Some(order)),
                StatementProperty::Group(Some(group)),
                StatementProperty::Having(Some(having)),
                StatementProperty::Limit(Some(limit)),
                StatementProperty::Offset(Some(offset)),
            ],
        );

        Ok(())
    }
}
