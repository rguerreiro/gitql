use super::ast::{self, ASTError, Argument, Directive, Expression, Order, Selection, Statement};
use super::execution::{ExecutionPlan, ScanScope};
use super::functions;
use super::merge_sort;
use super::repository::{
    self, Commit, Cred, CredentialType, Delta, Diff, DiffDelta, DiffFile, DiffHunk, DiffLine,
    GitError, GitqlRepository, ObjectType, Odb, Oid, Repository,
};
use crate::Request;
use chrono::prelude::*;
use core::fmt;
use core::hash::{Hash, Hasher};
use core::ops::Range;
use regex::{Error as RegexError, Regex};
use serde::de::Visitor;
use serde::{
    ser::{SerializeSeq, SerializeTuple},
    Deserialize, Deserializer, Serialize, Serializer,
};
use std::cmp::{Ordering, PartialEq, PartialOrd};
use std::collections::HashMap;
use std::collections::HashSet;
use std::mem::replace;
use std::ops::{Add, BitAnd, BitOr, BitXor, Div, Mul, Neg, Not, Rem, Shl, Shr, Sub};
use std::rc::Rc;
use std::string::FromUtf8Error;
use std::sync::{Arc, Mutex, MutexGuard, PoisonError, RwLock};
use std::thread;
use std::time::SystemTime;
use time::Duration;

const DATE_FORMAT: &'static str = "%Y-%m-%d";
const DATE_TIME_FORMAT: &'static str = "%Y-%m-%dT%H:%M:%SZ";

#[derive(Debug)]
pub enum ApplicationError {
    ASTError(Box<ASTError>),
    GitError(GitError),
    FromUtf8Error(FromUtf8Error),
    //    PoisonError(PoisonError<MutexGuard<'_, Repository>>),
    PoisonError(String),

    HavingMustBeWithGroupBy,

    UnreachableCommitLineColumn,
    UnreachableCommitHunkColumn,
    UnreachableCommitDeltaColumn,
    UnreachableCommitDiffColumn,
    UnreachableCommitColumn,
    UnreachableRepositoryColumn,

    InvalidRegularExpression(RegexError),
    InvalidFunctionCall(String, (String, Vec<Arc<Argument>>)),
    InvalidFunctionCallScope(StatementScope, (String, Vec<Arc<Argument>>)),

    InvalidExpression(Arc<Expression>),

    WhereExpressionMustBeBoolean(Arc<Expression>),
    HavingExpressionMustBeBoolean(Arc<Expression>),

    UnknownIdentifier(String),

    ExpressionRequiresRowContextData(Arc<Expression>),
    ExpressionRequiresAggregateContextData(Arc<Expression>),

    InvalidSortBy(Arc<Order>),
}

impl From<ASTError> for ApplicationError {
    fn from(e: ASTError) -> Self {
        ApplicationError::ASTError(Box::new(e))
    }
}

impl From<GitError> for ApplicationError {
    fn from(e: GitError) -> Self {
        ApplicationError::GitError(e)
    }
}

impl From<RegexError> for ApplicationError {
    fn from(e: RegexError) -> Self {
        ApplicationError::InvalidRegularExpression(e)
    }
}

impl From<FromUtf8Error> for ApplicationError {
    fn from(e: FromUtf8Error) -> Self {
        ApplicationError::FromUtf8Error(e)
    }
}

impl<T> From<PoisonError<T>> for ApplicationError {
    fn from(e: PoisonError<T>) -> Self {
        ApplicationError::PoisonError(e.to_string())
    }
}

enum FunctionInvocationError {
    Message(String),
    InvalidScope(StatementScope),
    ApplicationError(ApplicationError),
    ExpressionRequiresAggregateContextData,
}

impl From<ApplicationError> for FunctionInvocationError {
    fn from(e: ApplicationError) -> Self {
        FunctionInvocationError::ApplicationError(e)
    }
}

impl From<String> for FunctionInvocationError {
    fn from(e: String) -> Self {
        FunctionInvocationError::Message(e)
    }
}

impl From<&str> for FunctionInvocationError {
    fn from(e: &str) -> Self {
        e.to_owned().into()
    }
}

#[inline]
fn from_time_to_date_time(value: repository::Time) -> DateTime<Utc> {
    DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(value.seconds(), 0), Utc)
}

#[derive(PartialEq, Debug, Copy, Clone)]
pub enum Column {
    //    LineOldFileContent,
    //    LineNewFileContent,
    //    LineOldFileNumber,
    //    LineNewFileNumber,
    DeltaOldFilePath,
    DeltaNewFilePath,
    DeltaOldFileName,
    DeltaNewFileName,
    DeltaOldFileExtension,
    DeltaNewFileExtension,

    DiffAddedFiles,
    DiffDeletedFiles,
    DiffRenamedFiles,
    DiffModifiedFiles,

    DiffInsertions,
    DiffFilesChanged,
    DiffDeletions,

    CommitAuthor,
    CommitAuthorName,
    CommitAuthorEmail,
    CommitHash,
    CommitMessage,
    CommitDate,
    CommitIsMerge,
    CommitIsRoot,
}

impl Column {
    pub fn from_name(name: &str) -> Option<Self> {
        let name = name.to_lowercase();

        match name.as_str() {
            //            "old_file_line_content" => Some(Column::LineOldFileContent),
            //            "new_file_line_content" => Some(Column::LineNewFileContent),
            //            "old_file_line_number" => Some(Column::LineOldFileNumber),
            //            "new_file_line_number" => Some(Column::LineNewFileNumber),
            "old_file_path" => Some(Column::DeltaOldFilePath),
            "new_file_path" => Some(Column::DeltaNewFilePath),
            "old_file_name" => Some(Column::DeltaOldFileName),
            "new_file_name" => Some(Column::DeltaNewFileName),
            "old_file_extension" => Some(Column::DeltaOldFileExtension),
            "new_file_extension" => Some(Column::DeltaNewFileExtension),

            "added_files" => Some(Column::DiffAddedFiles),
            "deleted_files" => Some(Column::DiffDeletedFiles),
            "renamed_files" => Some(Column::DiffRenamedFiles),
            "modified_files" => Some(Column::DiffModifiedFiles),
            "commit_insertions" => Some(Column::DiffInsertions),
            "files_changed" => Some(Column::DiffFilesChanged),
            "commit_deletions" => Some(Column::DiffDeletions),

            "author" => Some(Column::CommitAuthor),
            "author_name" => Some(Column::CommitAuthorName),
            "author_email" => Some(Column::CommitAuthorEmail),
            "commit_hash" => Some(Column::CommitHash),
            "commit_message" => Some(Column::CommitMessage),
            "commit_date" => Some(Column::CommitDate),
            "is_merge_commit" => Some(Column::CommitIsMerge),
            "is_root_commit" => Some(Column::CommitIsRoot),

            _ => None,
        }
    }

    fn as_value(self, row: Arc<SearchRow>) -> Result<Value, ApplicationError> {
        match self {
            Column::DeltaOldFilePath => Ok(row.get_delta()?.old_file_path.clone().into()),
            Column::DeltaNewFilePath => Ok(row.get_delta()?.new_file_path.clone().into()),
            Column::DeltaOldFileName => Ok(row.get_delta()?.old_file_name.clone().into()),
            Column::DeltaNewFileName => Ok(row.get_delta()?.new_file_name.clone().into()),
            Column::DeltaOldFileExtension => Ok(row.get_delta()?.old_file_extension.clone().into()),
            Column::DeltaNewFileExtension => Ok(row.get_delta()?.old_file_extension.clone().into()),

            Column::DiffAddedFiles => Ok(row.get_diff()?.added_files.into()),
            Column::DiffDeletedFiles => Ok(row.get_diff()?.deleted_files.into()),
            Column::DiffRenamedFiles => Ok(row.get_diff()?.renamed_files.into()),
            Column::DiffModifiedFiles => Ok(row.get_diff()?.modified_files.into()),
            Column::DiffInsertions => Ok(row.get_diff()?.insertions.into()),
            Column::DiffDeletions => Ok(row.get_diff()?.deletions.into()),
            Column::DiffFilesChanged => Ok(row.get_diff()?.files_changed.into()),

            Column::CommitAuthor => Ok(row.get_commit()?.author().into()),
            Column::CommitAuthorName => Ok(row.get_commit()?.author_name.clone().into()),
            Column::CommitAuthorEmail => Ok(row.get_commit()?.author_email.clone().into()),
            Column::CommitHash => Ok(row.get_commit()?.hash.clone().into()),
            Column::CommitMessage => Ok(row.get_commit()?.message.clone().into()),
            Column::CommitDate => Ok(row.get_commit()?.date.into()),
            Column::CommitIsMerge => Ok(row.get_commit()?.is_merge.into()),
            Column::CommitIsRoot => Ok(row.get_commit()?.is_root.into()),
        }
    }
}

#[inline]
fn f64_to_i64_bits(f: f64) -> i64 {
    f.to_bits() as i64
}

#[derive(Debug, Clone)]
pub struct RegularExpression {
    expression: String,
    compiled: Regex,
}

impl RegularExpression {
    pub fn new(expression: &str) -> Result<Self, regex::Error> {
        Ok(Self {
            expression: expression.to_owned(),
            compiled: Regex::new(expression)?,
        })
    }

    pub fn is_match(&self, value: &str) -> bool {
        self.compiled.is_match(value)
    }

    pub fn contains(&self, value: &str) -> bool {
        self.compiled.find(value).is_some()
    }
}

impl Hash for RegularExpression {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.expression.hash(state)
    }
}

impl PartialEq for RegularExpression {
    fn eq(&self, other: &Self) -> bool {
        self.expression == other.expression
    }
}

impl fmt::Display for RegularExpression {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "{}", self.expression)
    }
}

impl PartialOrd for RegularExpression {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.expression.partial_cmp(&other.expression)
    }
}

#[derive(Debug, Clone)]
pub enum Value {
    // Undefined always matches and returns true or itself, no matter what's it's being compared to
    Undefined,
    Null,
    Date(Date<Utc>),
    DateTime(DateTime<Utc>),
    Duration(Duration),
    String(String),
    RegularExpression(RegularExpression),
    Integer(i64),
    Float(f64),
    Boolean(bool),
    Set(Vec<Value>),
    Range(Arc<Value>, Arc<Value>),
}

impl Value {
    fn new_range(lhs: Value, rhs: Value) -> Option<Self> {
        let range = match lhs.partial_cmp(&rhs) {
            Some(Ordering::Less) | Some(Ordering::Equal) => Some((lhs, rhs)),
            Some(Ordering::Greater) => Some((rhs, lhs)),
            _ => None,
        };

        range.map(|r| Value::Range(Arc::new(r.0), Arc::new(r.1)))
    }

    fn try_from_date_string(value: &str) -> Self {
        {
            let date_time = NaiveDateTime::parse_from_str(value, DATE_TIME_FORMAT);
            if date_time.is_ok() {
                return date_time.unwrap().into();
            }
        }

        {
            let date = NaiveDate::parse_from_str(value, DATE_FORMAT);
            if date.is_ok() {
                return date.unwrap().into();
            }
        }

        value.into()
    }

    pub fn is_undefined(&self) -> bool {
        match self {
            Value::Null
            | Value::Date(_)
            | Value::DateTime(_)
            | Value::Duration(_)
            | Value::String(_)
            | Value::RegularExpression(_)
            | Value::Integer(_)
            | Value::Float(_)
            | Value::Boolean(_) => false,
            Value::Set(values) => {
                for value in values {
                    if value.is_undefined() {
                        return true;
                    }
                }

                false
            }
            Value::Range(lhs, rhs) => lhs.is_undefined() || rhs.is_undefined(),
            Value::Undefined => true,
        }
    }

    fn ordinal(&self) -> usize {
        match self {
            Value::Null => 0,
            Value::Date(_) => 1,
            Value::DateTime(_) => 2,
            Value::Duration(_) => 3,
            Value::String(_) => 4,
            Value::RegularExpression(_) => 5,
            Value::Integer(_) => 6,
            Value::Float(_) => 7,
            Value::Boolean(_) => 8,
            Value::Set(_) => 9,
            Value::Range(_, _) => 10,
            Value::Undefined => 11,
        }
    }

    fn equals(&self, other: &Self) -> bool {
        if self.is_undefined() || other.is_undefined() {
            return true;
        }

        match (self, other) {
            (Value::Null, Value::Null) => true,

            (Value::Integer(lhs), Value::Integer(rhs)) => *lhs == *rhs,
            (Value::Float(lhs), Value::Float(rhs)) => (*lhs - *rhs).abs() < std::f64::EPSILON,
            (Value::Integer(integer), Value::Float(float))
            | (Value::Float(float), Value::Integer(integer)) => {
                (*float - (*integer as f64)).abs() < std::f64::EPSILON
            }

            (Value::Date(lhs), Value::Date(rhs)) => *lhs == *rhs,
            (Value::DateTime(lhs), Value::DateTime(rhs)) => *lhs == *rhs,

            (Value::Date(date), Value::DateTime(date_time))
            | (Value::DateTime(date_time), Value::Date(date)) => date_time.date() == *date,

            (Value::Date(date), Value::String(string))
            | (Value::String(string), Value::Date(date)) => {
                match Self::try_from_date_string(string.as_str()) {
                    Value::Date(as_date) => *date == as_date,
                    Value::DateTime(as_date_time) => *date == as_date_time.date(),
                    _ => false,
                }
            }

            (Value::DateTime(date_time), Value::String(string))
            | (Value::String(string), Value::DateTime(date_time)) => {
                match Self::try_from_date_string(string.as_str()) {
                    Value::Date(as_date) => date_time.date() == as_date,
                    Value::DateTime(as_date_time) => *date_time == as_date_time,
                    _ => false,
                }
            }

            (Value::Duration(lhs), Value::Duration(rhs)) => *lhs == *rhs,

            (Value::String(lhs), Value::String(rhs)) => *lhs == *rhs,

            (Value::Boolean(lhs), Value::Boolean(rhs)) => *lhs == *rhs,

            (Value::Set(lhs), Value::Set(rhs)) => {
                // TODO improve set comparison
                *lhs == *rhs
            }

            (Value::Set(set), Value::Integer(_))
            | (Value::Set(set), Value::Float(_))
            | (Value::Set(set), Value::String(_))
            | (Value::Set(set), Value::Date(_))
            | (Value::Set(set), Value::DateTime(_))
            | (Value::Set(set), Value::Duration(_))
            | (Value::Set(set), Value::Boolean(_))
            | (Value::Set(set), Value::Null) => set.contains(&other),

            (Value::Integer(_), Value::Set(set))
            | (Value::Float(_), Value::Set(set))
            | (Value::String(_), Value::Set(set))
            | (Value::Date(_), Value::Set(set))
            | (Value::DateTime(_), Value::Set(set))
            | (Value::Duration(_), Value::Set(set))
            | (Value::Boolean(_), Value::Set(set))
            | (Value::Null, Value::Set(set)) => set.contains(self),

            (Value::Set(set), Value::RegularExpression(_)) => set.iter().any(|v| other == v),
            (Value::RegularExpression(_), Value::Set(set)) => set.iter().any(|v| self == v),

            (Value::RegularExpression(lhs), Value::RegularExpression(rhs)) => *lhs == *rhs,

            (Value::RegularExpression(regular_expression), Value::Integer(_))
            | (Value::RegularExpression(regular_expression), Value::Float(_))
            | (Value::RegularExpression(regular_expression), Value::String(_))
            | (Value::RegularExpression(regular_expression), Value::Date(_))
            | (Value::RegularExpression(regular_expression), Value::DateTime(_))
            | (Value::RegularExpression(regular_expression), Value::Duration(_))
            | (Value::RegularExpression(regular_expression), Value::Boolean(_)) => {
                regular_expression.is_match(other.to_string().as_str())
            }

            (Value::Integer(_), Value::RegularExpression(regular_expression))
            | (Value::Float(_), Value::RegularExpression(regular_expression))
            | (Value::String(_), Value::RegularExpression(regular_expression))
            | (Value::Date(_), Value::RegularExpression(regular_expression))
            | (Value::DateTime(_), Value::RegularExpression(regular_expression))
            | (Value::Duration(_), Value::RegularExpression(regular_expression))
            | (Value::Boolean(_), Value::RegularExpression(regular_expression))
            | (Value::Null, Value::RegularExpression(regular_expression)) => {
                regular_expression.is_match(self.to_string().as_str())
            }

            (Value::String(string), into_string) | (into_string, Value::String(string)) => {
                *string == (*into_string).to_string()
            }

            _ => false,
        }
    }

    fn contains(&self, other: &Self) -> Option<bool> {
        if self.is_undefined() || other.is_undefined() {
            return Some(true);
        }

        match (self, other) {
            (Value::Integer(integer), Value::Float(float))
            | (Value::Float(float), Value::Integer(integer)) => {
                Some((float.floor() as i64) == *integer || (float.ceil() as i64) == *integer)
            }

            (Value::String(lhs), Value::String(rhs)) => Some(lhs.contains(rhs.as_str())),

            (Value::Set(_), Value::Set(set)) => {
                for value in set {
                    if self.contains(value).unwrap_or(false) {
                        return Some(true);
                    }
                }

                Some(false)
            }

            (Value::Set(set), Value::Integer(_))
            | (Value::Set(set), Value::Float(_))
            | (Value::Set(set), Value::String(_))
            | (Value::Set(set), Value::Date(_))
            | (Value::Set(set), Value::DateTime(_))
            | (Value::Set(set), Value::Duration(_))
            | (Value::Set(set), Value::Boolean(_))
            | (Value::Set(set), Value::Null) => {
                for value in set {
                    if other.contains(value).unwrap_or(false) {
                        return Some(true);
                    }
                }

                Some(false)
            }

            (Value::Integer(_), Value::Set(set))
            | (Value::Float(_), Value::Set(set))
            | (Value::String(_), Value::Set(set))
            | (Value::Date(_), Value::Set(set))
            | (Value::DateTime(_), Value::Set(set))
            | (Value::Duration(_), Value::Set(set))
            | (Value::Boolean(_), Value::Set(set))
            | (Value::Null, Value::Set(set)) => {
                for value in set {
                    if self.contains(value).unwrap_or(false) {
                        return Some(true);
                    }
                }

                Some(false)
            }

            (Value::Range(lower, upper), Value::Integer(_))
            | (Value::Range(lower, upper), Value::Float(_))
            | (Value::Range(lower, upper), Value::String(_))
            | (Value::Range(lower, upper), Value::Date(_))
            | (Value::Range(lower, upper), Value::DateTime(_))
            | (Value::Range(lower, upper), Value::Duration(_))
            | (Value::Range(lower, upper), Value::Boolean(_))
            | (Value::Range(lower, upper), Value::Null) => {
                let lhs = lower.as_ref().partial_cmp(&other);
                let rhs = other.partial_cmp(upper.as_ref());

                match (lhs, rhs) {
                    (Some(Ordering::Less), Some(Ordering::Less))
                    | (Some(Ordering::Equal), Some(Ordering::Less)) => Some(true),
                    (Some(_), Some(_)) => Some(false),
                    _ => None,
                }
            }

            (Value::Integer(_), Value::Range(lower, upper))
            | (Value::Float(_), Value::Range(lower, upper))
            | (Value::String(_), Value::Range(lower, upper))
            | (Value::Date(_), Value::Range(lower, upper))
            | (Value::DateTime(_), Value::Range(lower, upper))
            | (Value::Duration(_), Value::Range(lower, upper))
            | (Value::Boolean(_), Value::Range(lower, upper))
            | (Value::Null, Value::Range(lower, upper)) => {
                let lhs = lower.as_ref().partial_cmp(&self);
                let rhs = self.partial_cmp(upper.as_ref());

                match (lhs, rhs) {
                    (Some(Ordering::Less), Some(Ordering::Less))
                    | (Some(Ordering::Equal), Some(Ordering::Less)) => Some(true),
                    (Some(_), Some(_)) => Some(false),
                    _ => None,
                }
            }

            (Value::Set(set), Value::RegularExpression(_)) => {
                Some(set.iter().any(|v| other.contains(v).unwrap_or(false)))
            }
            (Value::RegularExpression(_), Value::Set(set)) => {
                Some(set.iter().any(|v| self.contains(v).unwrap_or(false)))
            }

            (Value::RegularExpression(regular_expression), Value::Integer(_))
            | (Value::RegularExpression(regular_expression), Value::Float(_))
            | (Value::RegularExpression(regular_expression), Value::String(_))
            | (Value::RegularExpression(regular_expression), Value::Date(_))
            | (Value::RegularExpression(regular_expression), Value::DateTime(_))
            | (Value::RegularExpression(regular_expression), Value::Duration(_))
            | (Value::RegularExpression(regular_expression), Value::Boolean(_)) => {
                Some(regular_expression.contains(other.to_string().as_str()))
            }

            (Value::Integer(_), Value::RegularExpression(regular_expression))
            | (Value::Float(_), Value::RegularExpression(regular_expression))
            | (Value::String(_), Value::RegularExpression(regular_expression))
            | (Value::Date(_), Value::RegularExpression(regular_expression))
            | (Value::DateTime(_), Value::RegularExpression(regular_expression))
            | (Value::Duration(_), Value::RegularExpression(regular_expression))
            | (Value::Boolean(_), Value::RegularExpression(regular_expression))
            | (Value::Null, Value::RegularExpression(regular_expression)) => {
                Some(regular_expression.contains(self.to_string().as_str()))
            }

            (Value::String(string), into_string) | (into_string, Value::String(string)) => {
                Some(string.contains((*into_string).to_string().as_str()))
            }

            _ => None,
        }
    }
}

impl Serialize for Value {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        match self {
            Value::Undefined | Value::Null => serializer.serialize_none(),
            Value::Date(value) => {
                serializer.serialize_str(value.format(DATE_FORMAT).to_string().as_str())
            }
            Value::DateTime(value) => {
                serializer.serialize_str(value.format(DATE_TIME_FORMAT).to_string().as_str())
            }
            Value::Duration(value) => {
                let seconds = value.num_seconds();

                if let Some(nanoseconds) = value.num_nanoseconds() {
                    let duration = (seconds as f64) + ((nanoseconds as f64) / 1_000_000_000f64);
                    serializer.serialize_f64(duration)
                } else {
                    serializer.serialize_i64(seconds)
                }
            }
            Value::String(value) => serializer.serialize_str(value),
            Value::RegularExpression(value) => serializer.serialize_str(value.expression.as_str()),
            Value::Integer(value) => serializer.serialize_i64(*value),
            Value::Float(value) => serializer.serialize_f64(*value),
            Value::Boolean(value) => serializer.serialize_bool(*value),
            Value::Set(values) => {
                let mut serializer: S::SerializeSeq =
                    serializer.serialize_seq(Some(values.len()))?;

                for value in values {
                    serializer.serialize_element(value)?;
                }
                serializer.end()
            }
            Value::Range(lhs, rhs) => {
                let mut serializer: S::SerializeTuple = serializer.serialize_tuple(2)?;
                serializer.serialize_element(lhs.as_ref())?;
                serializer.serialize_element(rhs.as_ref())?;

                serializer.end()
            }
        }
    }
}

impl Hash for Value {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match self {
            Value::Undefined | Value::Null => state.write_u8(0),
            Value::Date(value) => value.hash(state),
            Value::DateTime(value) => value.hash(state),
            Value::Duration(value) => value.hash(state),
            Value::String(value) => value.hash(state),
            Value::RegularExpression(value) => value.hash(state),
            Value::Integer(value) => value.hash(state),
            Value::Float(value) => state.write_u64(value.to_bits()),
            Value::Boolean(value) => value.hash(state),
            Value::Set(values) => {
                for value in values.iter() {
                    value.hash(state)
                }
            }
            Value::Range(lhs, rhs) => {
                lhs.hash(state);
                rhs.hash(state);
            }
        }
    }
}

impl Eq for Value {}

impl PartialEq for Value {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Value::Undefined, Value::Undefined) => true,
            (Value::Null, Value::Null) => true,
            (Value::Integer(lhs), Value::Integer(rhs)) => *lhs == *rhs,
            (Value::Float(lhs), Value::Float(rhs)) => *lhs == *rhs,
            (Value::Date(lhs), Value::Date(rhs)) => *lhs == *rhs,
            (Value::DateTime(lhs), Value::DateTime(rhs)) => *lhs == *rhs,
            (Value::Duration(lhs), Value::Duration(rhs)) => *lhs == *rhs,
            (Value::String(lhs), Value::String(rhs)) => *lhs == *rhs,
            (Value::Boolean(lhs), Value::Boolean(rhs)) => *lhs == *rhs,
            (Value::Set(lhs), Value::Set(rhs)) => *lhs == *rhs,
            (Value::RegularExpression(lhs), Value::RegularExpression(rhs)) => *lhs == *rhs,
            (Value::Range(lhs_zero, lhs_one), Value::Range(rhs_zero, rhs_one)) => {
                *lhs_zero == *rhs_zero && *lhs_one == *rhs_one
            }

            _ => false,
        }
    }
}

impl PartialOrd for Value {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        if self == other {
            return Some(Ordering::Equal);
        }

        if self.is_undefined() {
            return Some(Ordering::Less);
        } else if other.is_undefined() {
            return Some(Ordering::Greater);
        }

        match (self, other) {
            (Value::Integer(lhs), Value::Integer(rhs)) => lhs.partial_cmp(rhs),
            (Value::Float(lhs), Value::Float(rhs)) => lhs.partial_cmp(rhs),
            (Value::Integer(lhs), Value::Float(rhs)) => (*lhs as f64).partial_cmp(rhs),
            (Value::Float(lhs), Value::Integer(rhs)) => lhs.partial_cmp(&(*rhs as f64)),

            (Value::Date(lhs), Value::Date(rhs)) => lhs.partial_cmp(rhs),
            (Value::DateTime(lhs), Value::DateTime(rhs)) => lhs.partial_cmp(rhs),

            (Value::Date(date), Value::DateTime(date_time)) => date.partial_cmp(&date_time.date()),
            (Value::DateTime(date_time), Value::Date(date)) => date_time.date().partial_cmp(date),

            (Value::Date(lhs), Value::String(rhs)) => {
                match Self::try_from_date_string(rhs.as_str()) {
                    Value::Date(rhs) => lhs.partial_cmp(&rhs),
                    Value::DateTime(rhs) => lhs.partial_cmp(&rhs.date()),
                    _ => None,
                }
            }
            (Value::String(lhs), Value::Date(rhs)) => {
                match Self::try_from_date_string(lhs.as_str()) {
                    Value::Date(lhs) => lhs.partial_cmp(&rhs),
                    Value::DateTime(lhs) => lhs.date().partial_cmp(&rhs),
                    _ => None,
                }
            }

            (Value::DateTime(lhs), Value::String(rhs)) => {
                match Self::try_from_date_string(rhs.as_str()) {
                    Value::Date(rhs) => lhs.date().partial_cmp(&rhs),
                    Value::DateTime(rhs) => lhs.partial_cmp(&rhs),
                    _ => None,
                }
            }
            (Value::String(lhs), Value::DateTime(rhs)) => {
                match Self::try_from_date_string(lhs.as_str()) {
                    Value::Date(lhs) => lhs.partial_cmp(&rhs.date()),
                    Value::DateTime(lhs) => lhs.partial_cmp(&rhs),
                    _ => None,
                }
            }

            (Value::Duration(lhs), Value::Duration(rhs)) => lhs.partial_cmp(rhs),

            (Value::String(lhs), Value::String(rhs)) => lhs.partial_cmp(rhs),

            (Value::String(string), into_string) => string.partial_cmp(&(*into_string).to_string()),
            (into_string, Value::String(string)) => (*into_string).to_string().partial_cmp(string),

            (Value::RegularExpression(lhs), Value::RegularExpression(rhs)) => lhs.partial_cmp(rhs),

            (Value::Boolean(lhs), Value::Boolean(rhs)) => lhs.partial_cmp(rhs),

            _ => None,
        }
    }
}

impl Ord for Value {
    fn cmp(&self, other: &Self) -> Ordering {
        match self.partial_cmp(other) {
            Some(ordering) => ordering,
            None => self.ordinal().cmp(&other.ordinal()),
        }
    }
}

impl Neg for Value {
    type Output = Option<Self>;

    fn neg(self) -> Option<Self> {
        if self.is_undefined() {
            return Some(Value::Undefined);
        }

        match self {
            Value::Integer(value) => Some(Value::Integer(-value)),
            Value::Float(value) => Some(Value::Float(-value)),
            _ => None,
        }
    }
}

impl Not for Value {
    type Output = Option<Self>;

    fn not(self) -> Option<Self> {
        if self.is_undefined() {
            return Some(Value::Undefined);
        }

        match self {
            Value::Integer(value) => Some(Value::Integer(!value)),
            Value::Float(value) => Some(Value::Integer((!value.to_bits()) as i64)),
            Value::Boolean(value) => Some(Value::Boolean(!value)),
            _ => None,
        }
    }
}

impl Add for Value {
    type Output = Option<Self>;

    fn add(self, rhs: Self) -> Option<Self> {
        &self + &rhs
    }
}

impl Add for &Value {
    #![allow(clippy::suspicious_arithmetic_impl)]

    type Output = Option<Value>;

    fn add(self, rhs: Self) -> Option<Value> {
        if self.is_undefined() || rhs.is_undefined() {
            return Some(Value::Undefined);
        }

        match (self, rhs) {
            (Value::Integer(lhs), Value::Integer(rhs)) => Some(Value::Integer(*lhs + *rhs)),
            (Value::Float(lhs), Value::Float(rhs)) => Some(Value::Float(*lhs + *rhs)),
            (Value::Integer(lhs), Value::Float(rhs)) => Some(Value::Float((*lhs as f64) + *rhs)),
            (Value::Float(lhs), Value::Integer(rhs)) => Some(Value::Float(*lhs + (*rhs as f64))),
            (Value::Duration(lhs), Value::Duration(rhs)) => Some(Value::Duration(*lhs + *rhs)),

            (Value::Date(date), Value::Duration(duration))
            | (Value::Duration(duration), Value::Date(date)) => {
                Some(Value::Date(*date + *duration))
            }

            (Value::DateTime(date_time), Value::Duration(duration))
            | (Value::Duration(duration), Value::DateTime(date_time)) => {
                Some(Value::DateTime(*date_time + *duration))
            }

            (Value::Duration(lhs), Value::String(rhs)) => {
                match Value::try_from_date_string(rhs.as_str()) {
                    Value::Date(rhs) => Some(Value::Date(rhs.clone() + *lhs)),
                    Value::DateTime(rhs) => Some(Value::DateTime(rhs.clone() + *lhs)),
                    _ => Some(Value::String(lhs.to_string() + rhs.as_str())),
                }
            }

            (Value::String(lhs), Value::Duration(rhs)) => {
                match Value::try_from_date_string(lhs.as_str()) {
                    Value::Date(lhs) => Some(Value::Date(lhs.clone() + *rhs)),
                    Value::DateTime(lhs) => Some(Value::DateTime(lhs.clone() + *rhs)),
                    _ => Some(Value::String(lhs.to_owned() + rhs.to_string().as_str())),
                }
            }

            (Value::Set(lhs), Value::Set(rhs)) => {
                let mut union = Vec::with_capacity(lhs.len() + rhs.len());

                union.append(&mut lhs.clone());
                union.append(&mut rhs.clone());

                Some(Value::Set(union))
            }

            (Value::Set(set), into_set) | (into_set, Value::Set(set)) => {
                let mut values = set.clone();
                values.push(into_set.clone());
                Some(Value::Set(values))
            }

            (Value::String(lhs), rhs) => {
                Some(Value::String(lhs.to_owned() + rhs.to_string().as_str()))
            }

            (lhs, Value::String(rhs)) => Some(Value::String(lhs.to_string() + rhs.as_str())),
            _ => None,
        }
    }
}

impl Sub for Value {
    #![allow(clippy::suspicious_arithmetic_impl)]

    type Output = Option<Self>;

    fn sub(self, rhs: Self) -> Option<Self> {
        if self.is_undefined() || rhs.is_undefined() {
            return Some(Value::Undefined);
        }

        match (self, rhs) {
            (Value::Integer(lhs), Value::Integer(rhs)) => Some(Value::Integer(lhs - rhs)),
            (Value::Float(lhs), Value::Float(rhs)) => Some(Value::Float(lhs - rhs)),
            (Value::Integer(lhs), Value::Float(rhs)) => Some(Value::Float((lhs as f64) - rhs)),
            (Value::Float(lhs), Value::Integer(rhs)) => Some(Value::Float(lhs - (rhs as f64))),
            (Value::Duration(lhs), Value::Duration(rhs)) => Some(Value::Duration(lhs - rhs)),

            (Value::Date(date), Value::Duration(duration)) => Some(Value::Date(date - duration)),
            (Value::DateTime(date_time), Value::Duration(duration)) => {
                Some(Value::DateTime(date_time - duration))
            }

            (Value::Date(lhs), Value::Date(rhs)) => Some(Value::Duration(lhs - rhs)),
            (Value::Date(lhs), Value::DateTime(rhs)) => {
                Some(Value::Duration(lhs.and_hms(0, 0, 0) - rhs))
            }
            (Value::DateTime(lhs), Value::DateTime(rhs)) => Some(Value::Duration(lhs - rhs)),
            (Value::DateTime(lhs), Value::Date(rhs)) => {
                Some(Value::Duration(lhs - rhs.and_hms(0, 0, 0)))
            }

            (Value::String(lhs), Value::Duration(rhs)) => {
                match Self::try_from_date_string(lhs.as_str()) {
                    Value::Date(lhs) => Some(Value::Date(lhs - rhs)),
                    Value::DateTime(lhs) => Some(Value::DateTime(lhs - rhs)),
                    _ => None,
                }
            }

            (Value::String(lhs), Value::String(rhs)) => {
                let lhs = Self::try_from_date_string(lhs.as_str());
                let rhs = Self::try_from_date_string(rhs.as_str());

                match (lhs, rhs) {
                    (Value::Date(lhs), Value::Date(rhs)) => Some(Value::Duration(lhs - rhs)),
                    (Value::Date(lhs), Value::DateTime(rhs)) => {
                        Some(Value::Duration(lhs.and_hms(0, 0, 0) - rhs))
                    }
                    (Value::DateTime(lhs), Value::DateTime(rhs)) => {
                        Some(Value::Duration(lhs - rhs))
                    }
                    (Value::DateTime(lhs), Value::Date(rhs)) => {
                        Some(Value::Duration(lhs - rhs.and_hms(0, 0, 0)))
                    }
                    _ => None,
                }
            }

            _ => None,
        }
    }
}

impl Mul for Value {
    #![allow(clippy::suspicious_arithmetic_impl)]

    type Output = Option<Self>;

    fn mul(self, rhs: Self) -> Option<Self> {
        if self.is_undefined() || rhs.is_undefined() {
            return Some(Value::Undefined);
        }

        match (self, rhs) {
            (Value::Integer(lhs), Value::Integer(rhs)) => Some(Value::Integer(lhs * rhs)),
            (Value::Float(lhs), Value::Float(rhs)) => Some(Value::Float(lhs * rhs)),
            (Value::Integer(lhs), Value::Float(rhs)) => Some(Value::Float((lhs as f64) * rhs)),
            (Value::Float(lhs), Value::Integer(rhs)) => Some(Value::Float(lhs * (rhs as f64))),
            _ => None,
        }
    }
}

impl Div for Value {
    #![allow(clippy::suspicious_arithmetic_impl)]

    type Output = Option<Self>;

    fn div(self, rhs: Self) -> Option<Self> {
        if self.is_undefined() || rhs.is_undefined() {
            return Some(Value::Undefined);
        }

        match (self, rhs) {
            (Value::Integer(lhs), Value::Integer(rhs)) if rhs != 0 => {
                Some(Value::Integer(lhs / rhs))
            }
            (Value::Float(lhs), Value::Float(rhs)) if (0f64 - rhs).abs() > std::f64::EPSILON => {
                Some(Value::Float(lhs / rhs))
            }
            (Value::Integer(lhs), Value::Float(rhs)) if (0f64 - rhs).abs() > std::f64::EPSILON => {
                Some(Value::Float((lhs as f64) / rhs))
            }
            (Value::Float(lhs), Value::Integer(rhs)) if rhs != 0 => {
                Some(Value::Float(lhs / (rhs as f64)))
            }
            _ => None,
        }
    }
}

impl Rem for Value {
    type Output = Option<Self>;

    fn rem(self, rhs: Self) -> Option<Self> {
        if self.is_undefined() || rhs.is_undefined() {
            return Some(Value::Undefined);
        }

        match (self, rhs) {
            (Value::Integer(lhs), Value::Integer(rhs)) => Some(Value::Integer(lhs % rhs)),
            (Value::Float(lhs), Value::Float(rhs)) => Some(Value::Float(lhs % rhs)),
            (Value::Integer(lhs), Value::Float(rhs)) => Some(Value::Float((lhs as f64) % rhs)),
            (Value::Float(lhs), Value::Integer(rhs)) => Some(Value::Float(lhs % (rhs as f64))),
            _ => None,
        }
    }
}

impl BitAnd for Value {
    type Output = Option<Self>;

    fn bitand(self, rhs: Self) -> Option<Self> {
        if self.is_undefined() || rhs.is_undefined() {
            return Some(Value::Undefined);
        }

        match (self, rhs) {
            (Value::Integer(lhs), Value::Integer(rhs)) => Some(Value::Integer(lhs & rhs)),
            (Value::Float(lhs), Value::Float(rhs)) => {
                Some(Value::Integer(f64_to_i64_bits(lhs) & f64_to_i64_bits(rhs)))
            }
            (Value::Integer(lhs), Value::Float(rhs)) => {
                Some(Value::Integer(lhs & f64_to_i64_bits(rhs)))
            }
            (Value::Float(lhs), Value::Integer(rhs)) => {
                Some(Value::Integer(f64_to_i64_bits(lhs) & rhs))
            }
            (Value::Boolean(lhs), Value::Boolean(rhs)) => Some(Value::Boolean(lhs & rhs)),
            _ => None,
        }
    }
}

impl BitOr for Value {
    type Output = Option<Self>;

    fn bitor(self, rhs: Self) -> Option<Self> {
        if self.is_undefined() || rhs.is_undefined() {
            return Some(Value::Undefined);
        }

        match (self, rhs) {
            (Value::Integer(lhs), Value::Integer(rhs)) => Some(Value::Integer(lhs | rhs)),
            (Value::Float(lhs), Value::Float(rhs)) => {
                Some(Value::Integer(f64_to_i64_bits(lhs) | f64_to_i64_bits(rhs)))
            }
            (Value::Integer(lhs), Value::Float(rhs)) => {
                Some(Value::Integer(lhs | f64_to_i64_bits(rhs)))
            }
            (Value::Float(lhs), Value::Integer(rhs)) => {
                Some(Value::Integer(f64_to_i64_bits(lhs) | rhs))
            }

            (Value::Boolean(lhs), Value::Boolean(rhs)) => Some(Value::Boolean(lhs | rhs)),
            _ => None,
        }
    }
}

impl BitXor for Value {
    type Output = Option<Self>;

    fn bitxor(self, rhs: Self) -> Option<Self> {
        if self.is_undefined() || rhs.is_undefined() {
            return Some(Value::Undefined);
        }

        match (self, rhs) {
            (Value::Integer(lhs), Value::Integer(rhs)) => Some(Value::Integer(lhs ^ rhs)),
            (Value::Float(lhs), Value::Float(rhs)) => {
                Some(Value::Integer(f64_to_i64_bits(lhs) ^ f64_to_i64_bits(rhs)))
            }
            (Value::Integer(lhs), Value::Float(rhs)) => {
                Some(Value::Integer(lhs ^ f64_to_i64_bits(rhs)))
            }
            (Value::Float(lhs), Value::Integer(rhs)) => {
                Some(Value::Integer(f64_to_i64_bits(lhs) ^ rhs))
            }
            (Value::Boolean(lhs), Value::Boolean(rhs)) => Some(Value::Boolean(lhs ^ rhs)),
            _ => None,
        }
    }
}

impl Shr for Value {
    type Output = Option<Self>;

    fn shr(self, rhs: Self) -> Option<Self> {
        if self.is_undefined() || rhs.is_undefined() {
            return Some(Value::Undefined);
        }

        match (self, rhs) {
            (Value::Integer(lhs), Value::Integer(rhs)) => Some(Value::Integer(lhs >> rhs)),
            (Value::Float(lhs), Value::Integer(rhs)) => {
                Some(Value::Integer(f64_to_i64_bits(lhs) >> rhs))
            }
            _ => None,
        }
    }
}

impl Shl for Value {
    type Output = Option<Self>;

    fn shl(self, rhs: Self) -> Option<Self> {
        if self.is_undefined() || rhs.is_undefined() {
            return Some(Value::Undefined);
        }

        match (self, rhs) {
            (Value::Integer(lhs), Value::Integer(rhs)) => Some(Value::Integer(lhs << rhs)),
            (Value::Float(lhs), Value::Integer(rhs)) => {
                Some(Value::Integer(f64_to_i64_bits(lhs) << rhs))
            }
            _ => None,
        }
    }
}

impl fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match self {
            Value::Undefined => write!(f, "(undefined)"),
            Value::Null => write!(f, "(null)"),
            Value::Date(value) => write!(f, "{}", value.format(DATE_FORMAT)),
            Value::DateTime(value) => {
                write!(f, "{}", value.to_rfc3339_opts(SecondsFormat::Secs, true))
            }
            Value::Duration(value) => write!(f, "{}", value),
            Value::String(value) => write!(f, "{}", value),
            Value::RegularExpression(value) => write!(f, "{}", value),
            Value::Integer(value) => write!(f, "{}", value),
            Value::Float(value) => write!(f, "{}", value),
            Value::Boolean(value) => write!(f, "{}", value),
            Value::Range(lhs, rhs) => write!(f, "{}..{}", lhs, rhs),
            Value::Set(values) => {
                write!(f, "[")?;

                let mut comma = false;
                for value in values {
                    if comma {
                        write!(f, ", ")?;
                    }

                    comma = true;
                    if let Value::String(_) = value {
                        write!(f, r#""{}""#, value)?;
                    } else {
                        write!(f, "{}", value)?;
                    }
                }
                write!(f, "]")?;

                Ok(())
            }
        }
    }
}

impl<T: Into<Value>, E> From<Result<T, E>> for Value {
    fn from(value: Result<T, E>) -> Self {
        match value {
            Ok(value) => value.into(),
            Err(_) => Value::Null,
        }
    }
}

impl<T: Into<Value>> From<Option<T>> for Value {
    fn from(value: Option<T>) -> Self {
        match value {
            Some(value) => value.into(),
            None => Value::Null,
        }
    }
}

impl From<repository::Time> for Value {
    fn from(value: repository::Time) -> Self {
        Value::DateTime(DateTime::<Utc>::from_utc(
            NaiveDateTime::from_timestamp(value.seconds(), 0),
            Utc,
        ))
    }
}

impl From<Date<Utc>> for Value {
    fn from(value: Date<Utc>) -> Self {
        Value::Date(value)
    }
}

impl From<NaiveDate> for Value {
    fn from(value: NaiveDate) -> Self {
        Date::from_utc(value, Utc).into()
    }
}

impl From<DateTime<Utc>> for Value {
    fn from(value: DateTime<Utc>) -> Self {
        Value::DateTime(value)
    }
}

impl From<NaiveDateTime> for Value {
    fn from(value: NaiveDateTime) -> Self {
        DateTime::from_utc(value, Utc).into()
    }
}

impl From<Duration> for Value {
    fn from(value: Duration) -> Self {
        Value::Duration(value)
    }
}

impl<T: Into<Value>> From<Vec<T>> for Value {
    fn from(value: Vec<T>) -> Self {
        let values = value.into_iter().map(|v| v.into()).collect();
        Value::Set(values)
    }
}

impl From<bool> for Value {
    fn from(value: bool) -> Self {
        Value::Boolean(value)
    }
}

impl From<String> for Value {
    fn from(value: String) -> Self {
        value.as_str().into()
    }
}

impl From<&str> for Value {
    fn from(value: &str) -> Self {
        Value::String(value.to_owned())
    }
}

impl From<i8> for Value {
    fn from(value: i8) -> Self {
        i64::from(value).into()
    }
}

impl From<u8> for Value {
    fn from(value: u8) -> Self {
        i64::from(value).into()
    }
}

impl From<i16> for Value {
    fn from(value: i16) -> Self {
        i64::from(value).into()
    }
}

impl From<u16> for Value {
    fn from(value: u16) -> Self {
        i64::from(value).into()
    }
}

impl From<i32> for Value {
    fn from(value: i32) -> Self {
        i64::from(value).into()
    }
}

impl From<u32> for Value {
    fn from(value: u32) -> Self {
        i64::from(value).into()
    }
}

impl From<i64> for Value {
    fn from(value: i64) -> Self {
        Value::Integer(value)
    }
}

impl From<u64> for Value {
    fn from(value: u64) -> Self {
        (value as i64).into()
    }
}

impl From<isize> for Value {
    fn from(value: isize) -> Self {
        (value as i64).into()
    }
}

impl From<usize> for Value {
    fn from(value: usize) -> Self {
        (value as i64).into()
    }
}

impl From<f32> for Value {
    fn from(value: f32) -> Self {
        f64::from(value).into()
    }
}

impl From<f64> for Value {
    fn from(value: f64) -> Self {
        Value::Float(value)
    }
}

impl From<repository::Oid> for Value {
    fn from(value: repository::Oid) -> Self {
        value.to_string().into()
    }
}

impl From<RegularExpression> for Value {
    fn from(value: RegularExpression) -> Self {
        Value::RegularExpression(value)
    }
}

#[derive(Debug, Clone)]
struct SearchContext {
    headers: Vec<String>,
    rows: SearchContextRows,
}

impl SearchContext {
    fn new(headers: Vec<String>) -> Self {
        Self {
            headers,
            rows: SearchContextRows::Regular(Arc::new(Vec::new())),
        }
    }

    fn push_row(&mut self, row: Arc<SearchRow>) {
        match &mut self.rows {
            SearchContextRows::Regular(rows) => match Arc::get_mut(rows) {
                Some(rows) => rows.push(row),
                None => {
                    unreachable!("Pushing rows while holding references to SearchContext::rows.")
                }
            },
            _ => {}
        }
    }

    fn push_rows(&mut self, rows: Vec<Arc<SearchRow>>) {
        for row in rows.into_iter() {
            self.push_row(row);
        }
    }
}

#[derive(Debug, Clone)]
enum SearchContextRows {
    Regular(Arc<Vec<Arc<SearchRow>>>),
    Aggregated(Arc<Vec<Vec<Arc<SearchRow>>>>),
}

#[derive(Eq, PartialEq, Debug, Clone)]
struct SearchRowDiffDelta {
    old_file_path: Option<String>,
    new_file_path: Option<String>,
    old_file_name: Option<String>,
    new_file_name: Option<String>,
    old_file_extension: Option<String>,
    new_file_extension: Option<String>,
}

impl SearchRowDiffDelta {
    fn new(delta: Arc<DiffDelta>) -> Result<Self, ApplicationError> {
        let old_file = delta.old_file();
        let new_file = delta.new_file();

        let old_path = old_file.path();
        let new_path = new_file.path();

        let old_file_path = old_path.map(|p| p.as_os_str());
        let new_file_path = new_path.map(|p| p.as_os_str());

        let old_file_name = old_path.and_then(|p| p.file_name());
        let new_file_name = new_path.and_then(|p| p.file_name());

        let old_file_extension = old_path.and_then(|p| p.extension());
        let new_file_extension = new_path.and_then(|p| p.extension());

        Ok(Self {
            old_file_path: old_file_path.and_then(|p| p.to_str()).map(|s| s.to_owned()),
            new_file_path: new_file_path.and_then(|p| p.to_str()).map(|s| s.to_owned()),
            old_file_name: old_file_name.and_then(|p| p.to_str()).map(|s| s.to_owned()),
            new_file_name: new_file_name.and_then(|p| p.to_str()).map(|s| s.to_owned()),
            old_file_extension: old_file_extension
                .and_then(|p| p.to_str())
                .map(|s| s.to_owned()),
            new_file_extension: new_file_extension
                .and_then(|p| p.to_str())
                .map(|s| s.to_owned()),
        })
    }
}

#[derive(Eq, PartialEq, Debug, Clone)]
struct SearchRowDiff {
    added_files: i64,
    deleted_files: i64,
    renamed_files: i64,
    modified_files: i64,
    insertions: i64,
    deletions: i64,
    files_changed: i64,
}

impl SearchRowDiff {
    fn new(diff: Arc<Diff>) -> Result<Self, ApplicationError> {
        let (added_files, deleted_files, renamed_files, modified_files) =
            Self::count_delta_files(diff.clone());

        let stats = diff.stats()?;
        let insertions = stats.insertions() as i64;
        let deletions = stats.deletions() as i64;
        let files_changed = stats.files_changed() as i64;

        Ok(Self {
            added_files,
            deleted_files,
            renamed_files,
            modified_files,
            insertions,
            deletions,
            files_changed,
        })
    }

    fn count_delta_files(diff: Arc<Diff>) -> (i64, i64, i64, i64) {
        let mut added = 0;
        let mut deleted = 0;
        let mut renamed = 0;
        let mut modified = 0;

        for delta in diff.deltas() {
            match delta.status() {
                Delta::Added => added += 1,
                Delta::Deleted => deleted += 1,
                Delta::Renamed => renamed += 1,
                Delta::Modified => modified += 1,
                _ => {}
            }
        }

        (added, deleted, renamed, modified)
    }
}

#[derive(Eq, PartialEq, Debug, Clone)]
struct SearchRowCommit {
    hash: String,
    message: String,
    tags: Option<Vec<String>>,
    author_name: Option<String>,
    author_email: Option<String>,
    date: DateTime<Utc>,
    is_merge: bool,
    is_root: bool,
}

impl SearchRowCommit {
    fn new(commit: Arc<Commit>) -> Result<Self, ApplicationError> {
        let hash = commit.id().to_string();
        let message = commit
            .message()
            .ok_or(ApplicationError::UnreachableCommitColumn)?;

        let author = commit.author();

        let author_name = author.name().map(|s| s.to_owned());
        let author_email = author.email().map(|s| s.to_owned());
        let date = from_time_to_date_time(commit.time());

        let parents_count = commit.parents().len();

        Ok(Self {
            hash,
            message: message.to_owned(),
            tags: None,
            author_name,
            author_email,
            date,
            is_merge: parents_count > 1,
            is_root: parents_count == 0,
        })
    }

    fn author(&self) -> Option<String> {
        match (self.author_name.as_ref(), self.author_email.as_ref()) {
            (None, None) => None,
            (Some(name), None) => Some(name.to_owned()),
            (None, Some(email)) => Some(email.to_owned()),
            (Some(name), Some(email)) => Some(format!("{} <{}>", name, email).to_owned()),
        }
    }
}

#[derive(Clone)]
struct SearchRow {
    scope: ScanScope,
    delta: Option<Arc<SearchRowDiffDelta>>,
    diff: Option<Arc<SearchRowDiff>>,
    commit: Option<Arc<SearchRowCommit>>,
    repository: Arc<Repository>,
}

unsafe impl Send for SearchRow {}
unsafe impl Sync for SearchRow {}

impl SearchRow {
    fn repository(repository: Arc<Repository>) -> Result<Self, ApplicationError> {
        Ok(Self {
            scope: ScanScope::Value,
            delta: None,
            diff: None,
            commit: None,
            repository,
        })
    }

    fn commit(&self, commit: Arc<Commit>) -> Result<Self, ApplicationError> {
        Ok(Self {
            scope: ScanScope::Commit,
            delta: self.delta.clone(),
            diff: self.diff.clone(),
            commit: Some(Arc::new(SearchRowCommit::new(commit)?)),
            repository: self.repository.clone(),
        })
    }

    fn diff(&self, diff: Arc<Diff>) -> Result<Self, ApplicationError> {
        Ok(Self {
            scope: ScanScope::Diff,
            delta: self.delta.clone(),
            diff: Some(Arc::new(SearchRowDiff::new(diff)?)),
            commit: self.commit.clone(),
            repository: self.repository.clone(),
        })
    }

    fn delta(&self, delta: Arc<DiffDelta>) -> Result<Self, ApplicationError> {
        Ok(Self {
            scope: ScanScope::Delta,
            delta: Some(Arc::new(SearchRowDiffDelta::new(delta)?)),
            diff: self.diff.clone(),
            commit: self.commit.clone(),
            repository: self.repository.clone(),
        })
    }

    fn get_commit(&self) -> Result<Arc<SearchRowCommit>, ApplicationError> {
        self.commit
            .clone()
            .ok_or(ApplicationError::UnreachableCommitColumn)
    }

    fn get_diff(&self) -> Result<Arc<SearchRowDiff>, ApplicationError> {
        self.diff
            .clone()
            .ok_or(ApplicationError::UnreachableCommitDiffColumn)
    }

    fn get_delta(&self) -> Result<Arc<SearchRowDiffDelta>, ApplicationError> {
        self.delta
            .clone()
            .ok_or(ApplicationError::UnreachableCommitDeltaColumn)
    }
}

impl fmt::Debug for SearchRow {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        writeln!(f, "SearchRow (")?;
        writeln!(f, "\tscope: {:?}", self.scope)?;
        writeln!(f, "\tdelta: {:?},", self.delta)?;
        writeln!(f, "\tdiff: {:?},", self.diff)?;
        writeln!(f, "\tcommit: {:?},", self.commit)?;
        writeln!(f, ")")?;
        Ok(())
    }
}

#[derive(Debug, Clone)]
enum ContextData {
    Row(Arc<SearchRow>),
    Aggregated(Option<Arc<SearchRow>>, Arc<Vec<Arc<SearchRow>>>),
}

#[derive(Eq, PartialEq, Debug, Clone, Copy, Hash)]
pub enum StatementScope {
    Select,
    Where,
    Having,
    Group,
    Sort,
}

struct Search {
    plan: ExecutionPlan,
    context: SearchContext,
    metadata: SearchMetadata,
}

impl Search {
    fn new(statement: &Statement, select: &[Selection]) -> Self {
        let plan = ExecutionPlan::new(statement);
        let headers: Vec<String> = select.iter().map(|s| s.as_header()).collect();
        Self {
            plan,
            context: SearchContext::new(headers),
            metadata: SearchMetadata::empty(),
        }
    }

    fn open_or_clone(
        statement: &Statement,
        request: &Request,
    ) -> Result<Repository, ApplicationError> {
        let repository = Repository::open_or_clone(request)?;
        if !statement.contains_directive(Directive::NoFetch) {
            repository.fetch_remote_pruning(request)?;
        }

        Ok(repository)
    }

    fn search(
        &mut self,
        statement: Statement,
        select: Vec<Selection>,
        request: &Request,
    ) -> Result<Vec<Vec<Value>>, ApplicationError> {
        if let Some(0) = statement.limit {
            return Ok(Vec::with_capacity(0));
        }

        if self.plan.min_scope() < ScanScope::Value {
            self.visit_odb(&statement, request)?;
        } else {
            let repository = Self::open_or_clone(&statement, request)?;
            let row = Arc::new(SearchRow::repository(Arc::new(repository))?);
            self.push_context(row);
        }

        self.aggregate(&statement)?;
        self.having(&statement)?;
        self.sort(&statement)?;
        let values = self.select(select, statement.is_distinct())?;
        let values = self.limit_and_offset(&statement, values);
        self.metadata.result_count = values.len();

        Ok(values)
    }

    pub(crate) fn ranges_per_thread(len: usize, mut threads: usize) -> Vec<Range<usize>> {
        if len < threads {
            threads = len;
        }
        if threads == 0 {
            return vec![];
        }

        let mut ranges = Vec::with_capacity(threads);
        let part = len / threads;
        for thread in 0..threads {
            let start = part * thread;
            let mut end = part * (thread + 1);

            if thread == (threads - 1) && end < len {
                end = len;
            }

            ranges.push(start..end)
        }

        ranges
    }

    fn visit_odb(
        &mut self,
        statement: &Statement,
        request: &Request,
    ) -> Result<(), ApplicationError> {
        let repository = Self::open_or_clone(&statement, request)?;
        let odb = repository.odb()?;

        let stopwatch = Stopwatch::start();
        let mut oids = Vec::new();
        odb.foreach(|oid| {
            let kind = odb.read_header(*oid).map(|(_size, kind)| kind);
            if kind == Ok(ObjectType::Commit) {
                oids.push(*oid);
            }
            true
        })?;

        drop(odb);
        drop(repository);

        let number_of_threads = statement.number_of_threads() as usize;
        let ranges = Self::ranges_per_thread(oids.len(), number_of_threads);

        let mut handlers = Vec::with_capacity(number_of_threads + 1);

        for range in ranges {
            let path = request.path.clone();
            let plan = self.plan;
            let statement = statement.clone();
            let oids = oids[range].to_vec();

            handlers.push(thread::spawn(move || {
                let statement = statement;
                let path = path;
                let repository = Repository::open(path.as_str())?;
                let row = Arc::new(SearchRow::repository(Arc::new(repository))?);

                let mut contexts = Vec::new();

                for oid in oids {
                    let mut result = Self::visit_object(oid, plan, &statement, row.clone());
                    match result {
                        Ok(None) => {}
                        Ok(Some(ref mut context)) => contexts.append(context),
                        Err(error) => return Err(error),
                    }
                }

                Ok(contexts)
            }));
        }

        for handler in handlers {
            let mut context = handler.join();
            match context {
                Ok(Ok(context)) => self.context.push_rows(context),
                Ok(Err(error)) => {
                    println!("Got Error {:?}", error);
                }
                Err(error) => {
                    println!("Got Error {:?}", error);
                }
            }
        }

        self.metadata.time_on_commits = stopwatch.stop();
        println!(
            "Took {}µs to iterate over odb.",
            self.metadata.time_on_commits
        );

        Ok(())
    }

    fn visit_object(
        oid: Oid,
        plan: ExecutionPlan,
        statement: &Statement,
        row: Arc<SearchRow>,
    ) -> Result<Option<Vec<Arc<SearchRow>>>, ApplicationError> {
        let repository = row.repository.clone();
        let commit = Arc::new(repository.find_commit(oid)?);

        let row = Arc::new(row.commit(commit.clone())?);
        //        self.metadata.scanned_commits += 1;

        if !Self::accepts(statement.condition.clone(), row.clone())? {
            return Ok(None);
        }

        let should_visit_diff = Self::should_visit(plan, row.clone());

        let (accepts_commit, rows) = if should_visit_diff {
            Self::visit_diff(plan, &statement, commit.as_ref(), row.clone())?
        } else {
            (true, None)
        };

        let mut rows = rows.unwrap_or(Vec::new());
        if accepts_commit {
            match Self::accepts_context(plan, row.clone()) {
                Some(row) => rows.push(row),
                None => {
                    if !should_visit_diff {
                        return Ok(None);
                    }
                }
            }
            Ok(Self::to_optional(rows))
        } else {
            Ok(None)
        }
    }

    fn visit_diff(
        plan: ExecutionPlan,
        statement: &Statement,
        commit: &Commit,
        row: Arc<SearchRow>,
    ) -> Result<(bool, Option<Vec<Arc<SearchRow>>>), ApplicationError> {
        let repository = row.repository.clone();
        let diff = Arc::new(repository.diff_commit_with_first_parent(commit)?);

        let row = Arc::new(row.diff(diff.clone())?);
        //        self.metadata.scanned_commit_diffs += 1;

        if !Self::accepts(statement.condition.clone(), row.clone())? {
            return Ok((false, None));
        }

        let (accepts_diff, mut rows) = if Self::should_visit(plan, row.clone()) {
            Self::visit_deltas(plan, statement, diff.as_ref(), row.clone())?
        } else {
            (true, None)
        };

        let mut rows = rows.unwrap_or(Vec::new());
        if accepts_diff {
            match Self::accepts_context(plan, row.clone()) {
                Some(row) => rows.push(row),
                _ => {}
            }
            Ok((true, Self::to_optional(rows)))
        } else {
            Ok((false, None))
        }
    }

    fn visit_deltas(
        plan: ExecutionPlan,
        statement: &Statement,
        diff: &Diff,
        row: Arc<SearchRow>,
    ) -> Result<(bool, Option<Vec<Arc<SearchRow>>>), ApplicationError> {
        let mut accepts_deltas = false;

        let mut rows = Vec::new();

        for delta in diff.deltas() {
            let row = Arc::new(row.delta(Arc::new(delta))?);
            //            self.metadata.scanned_commit_diff_deltas += 1;

            if !Self::accepts(statement.condition.clone(), row.clone())? {
                continue;
            }

            accepts_deltas = true;
            match Self::accepts_context(plan, row.clone()) {
                Some(row) => rows.push(row),
                None => {
                    // Early return because delta was only used for condition
                    return Ok((true, Self::to_optional(rows)));
                }
            }
        }

        Ok((accepts_deltas, Self::to_optional(rows)))
    }

    #[inline]
    fn to_optional<T>(vec: Vec<T>) -> Option<Vec<T>> {
        if vec.is_empty() {
            None
        } else {
            Some(vec)
        }
    }

    fn aggregate(&mut self, statement: &Statement) -> Result<(), ApplicationError> {
        if statement.group.is_none() {
            return Ok(());
        }

        if let SearchContextRows::Regular(rows) = &self.context.rows {
            let group = statement.group.as_ref().unwrap();
            let mut aggregated: HashMap<Vec<Arc<Value>>, Vec<Arc<SearchRow>>> = HashMap::new();

            for row in rows.as_ref() {
                let mut row_aggregated_values: Vec<Arc<Value>> = Vec::new();

                for expression in group {
                    let value = Self::evaluate_expression(
                        expression.clone(),
                        ContextData::Row(row.clone()),
                        StatementScope::Group,
                    )?;
                    row_aggregated_values.push(Arc::new(value));
                }

                if let Some(r) = aggregated.get_mut(&row_aggregated_values) {
                    r.push(row.clone());
                } else {
                    aggregated.insert(row_aggregated_values, vec![row.clone()]);
                }
            }

            self.context.rows = SearchContextRows::Aggregated(Arc::new(
                aggregated.into_iter().map(|tuple| tuple.1).collect(),
            ));
        }

        Ok(())
    }

    fn having(&mut self, statement: &Statement) -> Result<(), ApplicationError> {
        if statement.having.is_none() {
            return Ok(());
        }

        let having = statement.having.as_ref().unwrap();

        match &mut self.context.rows {
            SearchContextRows::Regular(rows) => {
                let mut filtered = Vec::new();

                for row in rows.as_ref() {
                    let data = ContextData::Aggregated(Some(row.clone()), rows.clone());
                    if Self::accepts_having(having.clone(), data)? {
                        filtered.push(row.clone());
                    }
                }

                self.context.rows = SearchContextRows::Regular(Arc::new(filtered));
            }
            SearchContextRows::Aggregated(rows) => {
                let mut filtered = Vec::new();
                for row in rows.as_ref() {
                    let data = ContextData::Aggregated(row.first().cloned(), Arc::new(row.clone()));
                    if Self::accepts_having(having.clone(), data)? {
                        filtered.push(row.clone());
                    }
                }

                self.context.rows = SearchContextRows::Aggregated(Arc::new(filtered));
            }
        }

        Ok(())
    }

    fn accepts_having(
        expression: Arc<Expression>,
        data: ContextData,
    ) -> Result<bool, ApplicationError> {
        let result = Self::evaluate_expression(expression.clone(), data, StatementScope::Having)?;
        match result {
            Value::Boolean(value) => return Ok(value),
            _ => return Err(ApplicationError::HavingExpressionMustBeBoolean(expression)),
        }
    }

    fn sort(&mut self, statement: &Statement) -> Result<(), ApplicationError> {
        if statement.order.is_none() {
            return Ok(());
        }

        let orders = statement.order.as_ref().unwrap();

        match &mut self.context.rows {
            SearchContextRows::Regular(rows) => {
                let mut sorted_rows = rows.as_ref().clone();

                merge_sort::sort_by(&mut sorted_rows, |lhs, rhs| {
                    Self::sort_by(
                        orders,
                        ContextData::Aggregated(Some(lhs.clone()), rows.clone()),
                        ContextData::Aggregated(Some(rhs.clone()), rows.clone()),
                    )
                })?;

                self.context.rows = SearchContextRows::Regular(Arc::new(sorted_rows));
            }
            SearchContextRows::Aggregated(rows) => {
                let mut sorted_rows = rows.as_ref().clone();

                merge_sort::sort_by(&mut sorted_rows, |lhs_aggregate, rhs_aggregate| {
                    match (lhs_aggregate.first(), rhs_aggregate.first()) {
                        (Some(lhs), Some(rhs)) => Self::sort_by(
                            orders,
                            ContextData::Aggregated(
                                Some(lhs.clone()),
                                Arc::new(lhs_aggregate.clone()),
                            ),
                            ContextData::Aggregated(
                                Some(rhs.clone()),
                                Arc::new(rhs_aggregate.clone()),
                            ),
                        ),
                        (Some(_), None) => Ok(Ordering::Greater),
                        (None, Some(_)) => Ok(Ordering::Less),
                        (None, None) => Ok(Ordering::Equal),
                    }
                })?;

                self.context.rows = SearchContextRows::Aggregated(Arc::new(sorted_rows));
            }
        }

        Ok(())
    }

    fn sort_by(
        orders: &[Order],
        lhs: ContextData,
        rhs: ContextData,
    ) -> Result<Ordering, ApplicationError> {
        for order in orders {
            let result = match order {
                Order::Asc(expression) => {
                    let lhs = Self::evaluate_expression(
                        expression.clone(),
                        lhs.clone(),
                        StatementScope::Sort,
                    )?;
                    let rhs = Self::evaluate_expression(
                        expression.clone(),
                        rhs.clone(),
                        StatementScope::Sort,
                    )?;

                    lhs.partial_cmp(&rhs)
                }
                Order::Desc(expression) => {
                    let lhs = Self::evaluate_expression(
                        expression.clone(),
                        lhs.clone(),
                        StatementScope::Sort,
                    )?;
                    let rhs = Self::evaluate_expression(
                        expression.clone(),
                        rhs.clone(),
                        StatementScope::Sort,
                    )?;

                    rhs.partial_cmp(&lhs)
                }
            };

            match result {
                Some(Ordering::Greater) | Some(Ordering::Less) => return Ok(result.unwrap()),
                Some(_) => {}
                None => return Err(ApplicationError::InvalidSortBy(Arc::new(order.clone()))),
            }
        }

        Ok(Ordering::Equal)
    }

    /// Slices the result based on offset and limit
    ///
    /// [1, 2, 3] => [1, 2, 3]
    /// [1, 2, 3] limit 0 => []
    /// [1, 2, 3] limit 1 => [1]
    /// [1, 2, 3] limit 2 => [1, 2]
    /// [1, 2, 3] limit 3 => [1, 2, 3]
    /// [1, 2, 3] limit 4 => [1, 2, 3]
    ///
    /// [1, 2, 3] offset 0 => [1, 2, 3]
    /// [1, 2, 3] offset 1 => [2, 3]
    /// [1, 2, 3] offset 2 => [3]
    /// [1, 2, 3] offset 3 => []
    /// [1, 2, 3] offset 4 => []
    ///
    /// [1, 2, 3] offset 0 limit 1 => [1]
    /// [1, 2, 3] offset 1 limit 1 => [2]
    /// [1, 2, 3] offset 2 limit 1 => [3]
    /// [1, 2, 3] offset 3 limit 1 => []
    fn limit_and_offset(
        &mut self,
        statement: &Statement,
        values: Vec<Vec<Value>>,
    ) -> Vec<Vec<Value>> {
        if statement.offset.is_none() && statement.limit.is_none() {
            return values;
        }

        let size = values.len();

        let min = statement.offset.unwrap_or(0) as usize;
        let mut max = min + statement.limit.unwrap_or(size as u64) as usize;

        if max > size {
            max = size;
        }

        if (max - min) != size {
            let slice = &values[min..max];
            slice.to_vec()
        } else {
            values
        }
    }

    fn select(
        &mut self,
        select: Vec<Selection>,
        distinct: bool,
    ) -> Result<Vec<Vec<Value>>, ApplicationError> {
        let mut selected_values = Vec::new();

        match &self.context.rows {
            SearchContextRows::Regular(rows) => {
                for row in rows.iter() {
                    let data = ContextData::Aggregated(Some(row.clone()), rows.clone());

                    let row = Self::select_row(&select, data)?;
                    if !distinct || !selected_values.contains(&row) {
                        selected_values.push(row);
                    }
                }
            }
            SearchContextRows::Aggregated(rows) => {
                for row in rows.iter() {
                    let data = ContextData::Aggregated(row.first().cloned(), Arc::new(row.clone()));

                    let row = Self::select_row(&select, data)?;
                    if !distinct || !selected_values.contains(&row) {
                        selected_values.push(row);
                    }
                }
            }
        }

        Ok(selected_values)
    }

    fn select_row(select: &[Selection], data: ContextData) -> Result<Vec<Value>, ApplicationError> {
        let mut columns = Vec::with_capacity(select.len());

        for selection in select {
            let value = Self::evaluate_expression(
                selection.expression.clone(),
                data.clone(),
                StatementScope::Select,
            )?;

            columns.push(value);
        }

        Ok(columns)
    }

    fn accepts(
        expression: Option<Arc<Expression>>,
        row: Arc<SearchRow>,
    ) -> Result<bool, ApplicationError> {
        if let Some(expression) = expression {
            let result = Self::evaluate_expression(
                expression.clone(),
                ContextData::Row(row),
                StatementScope::Where,
            )?;
            match result {
                Value::Undefined => return Ok(true),
                Value::Boolean(value) => return Ok(value),
                _ => {
                    return Err(ApplicationError::WhereExpressionMustBeBoolean(
                        expression.clone(),
                    ));
                }
            }
        }

        Ok(true)
    }

    fn accepts_context(plan: ExecutionPlan, row: Arc<SearchRow>) -> Option<Arc<SearchRow>> {
        let smallest_scope = plan.min_scope();
        if smallest_scope == row.scope {
            Some(row)
        } else {
            None
        }
    }

    #[inline]
    fn should_visit(plan: ExecutionPlan, row: Arc<SearchRow>) -> bool {
        plan.min_scope() < row.scope
    }

    fn push_context(&mut self, row: Arc<SearchRow>) {
        match Self::accepts_context(self.plan, row) {
            Some(row) => self.context.push_row(row),
            None => {}
        }
    }

    fn evaluate_expression(
        expression: Arc<Expression>,
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, ApplicationError> {
        match expression.as_ref() {
            Expression::Null => Ok(Value::Null),
            Expression::String(value) => Ok(Value::from(value.as_str())),
            Expression::Integer(value) => Ok(Value::from(*value)),
            Expression::Float(value) => Ok(Value::from(*value)),
            Expression::Boolean(value) => Ok(Value::from(*value)),
            Expression::RegularExpression(value) => {
                let regular_expression = RegularExpression::new(value)?;
                Ok(Value::RegularExpression(regular_expression))
            }

            Expression::InterpolatedString(parts) => {
                let mut string = String::new();

                for value in parts {
                    let value = Self::evaluate_expression(value.clone(), data.clone(), scope)?;
                    string += value.to_string().as_str();
                }

                Ok(Value::String(string))
            }
            Expression::Set(expressions) => {
                let mut result = Vec::new();
                for e in expressions {
                    result.push(Self::evaluate_expression(e.clone(), data.clone(), scope)?);
                }
                Ok(Value::Set(result))
            }
            Expression::Function(name, arguments) => {
                Self::evaluate_function_call(name, arguments, data, scope).map_err(|error| {
                    match error {
                        FunctionInvocationError::ApplicationError(e) => e,
                        FunctionInvocationError::ExpressionRequiresAggregateContextData => {
                            ApplicationError::ExpressionRequiresAggregateContextData(
                                expression.clone(),
                            )
                        }
                        FunctionInvocationError::InvalidScope(scope) => {
                            ApplicationError::InvalidFunctionCallScope(
                                scope,
                                (name.to_owned(), arguments.clone()),
                            )
                        }
                        FunctionInvocationError::Message(message) => {
                            ApplicationError::InvalidFunctionCall(
                                message,
                                (name.to_owned(), arguments.clone()),
                            )
                        }
                    }
                })
            }
            Expression::Identifier(value) => {
                let column = Column::from_name(value);

                match (column, data) {
                    (Some(column), ContextData::Row(row))
                    | (Some(column), ContextData::Aggregated(Some(row), _)) => {
                        Ok(column.as_value(row).unwrap_or(Value::Undefined))
                    }
                    _ => Err(ApplicationError::UnknownIdentifier(value.clone())),
                }
            }
            Expression::Not(rhs) | Expression::BitInverse(rhs) => {
                let value = Self::evaluate_expression(rhs.clone(), data, scope)?;
                (!value).ok_or_else(|| ApplicationError::InvalidExpression(expression.clone()))
            }
            Expression::Plus(rhs) => {
                let value = Self::evaluate_expression(rhs.clone(), data, scope)?;
                match value {
                    Value::Integer(_) | Value::Float(_) => Ok(value),
                    _ => Err(ApplicationError::InvalidExpression(expression.clone())),
                }
            }
            Expression::Minus(rhs) => {
                let value = Self::evaluate_expression(rhs.clone(), data, scope)?;
                (-value).ok_or_else(|| ApplicationError::InvalidExpression(expression.clone()))
            }
            Expression::Range(lhs, rhs) => {
                let lhs = Self::evaluate_expression(lhs.clone(), data.clone(), scope)?;
                let rhs = Self::evaluate_expression(rhs.clone(), data.clone(), scope)?;

                Value::new_range(lhs, rhs)
                    .ok_or_else(|| ApplicationError::InvalidExpression(expression.clone()))
            }
            Expression::Add(lhs, rhs) => {
                let lhs = Self::evaluate_expression(lhs.clone(), data.clone(), scope)?;
                let rhs = Self::evaluate_expression(rhs.clone(), data.clone(), scope)?;

                (lhs + rhs).ok_or_else(|| ApplicationError::InvalidExpression(expression.clone()))
            }
            Expression::Subtract(lhs, rhs) => {
                let lhs = Self::evaluate_expression(lhs.clone(), data.clone(), scope)?;
                let rhs = Self::evaluate_expression(rhs.clone(), data.clone(), scope)?;

                (lhs - rhs).ok_or_else(|| ApplicationError::InvalidExpression(expression.clone()))
            }
            Expression::Multiply(lhs, rhs) => {
                let lhs = Self::evaluate_expression(lhs.clone(), data.clone(), scope)?;
                let rhs = Self::evaluate_expression(rhs.clone(), data.clone(), scope)?;

                (lhs * rhs).ok_or_else(|| ApplicationError::InvalidExpression(expression.clone()))
            }
            Expression::Divide(lhs, rhs) => {
                let lhs = Self::evaluate_expression(lhs.clone(), data.clone(), scope)?;
                let rhs = Self::evaluate_expression(rhs.clone(), data.clone(), scope)?;

                (lhs / rhs).ok_or_else(|| ApplicationError::InvalidExpression(expression.clone()))
            }
            Expression::Modulo(lhs, rhs) => {
                let lhs = Self::evaluate_expression(lhs.clone(), data.clone(), scope)?;
                let rhs = Self::evaluate_expression(rhs.clone(), data.clone(), scope)?;

                (lhs % rhs).ok_or_else(|| ApplicationError::InvalidExpression(expression.clone()))
            }
            Expression::BitAnd(lhs, rhs) => {
                let lhs = Self::evaluate_expression(lhs.clone(), data.clone(), scope)?;
                let rhs = Self::evaluate_expression(rhs.clone(), data.clone(), scope)?;

                (lhs & rhs).ok_or_else(|| ApplicationError::InvalidExpression(expression.clone()))
            }
            Expression::BitOr(lhs, rhs) => {
                let lhs = Self::evaluate_expression(lhs.clone(), data.clone(), scope)?;
                let rhs = Self::evaluate_expression(rhs.clone(), data.clone(), scope)?;

                (lhs | rhs).ok_or_else(|| ApplicationError::InvalidExpression(expression.clone()))
            }
            Expression::BitXor(lhs, rhs) => {
                let lhs = Self::evaluate_expression(lhs.clone(), data.clone(), scope)?;
                let rhs = Self::evaluate_expression(rhs.clone(), data.clone(), scope)?;

                (lhs ^ rhs).ok_or_else(|| ApplicationError::InvalidExpression(expression.clone()))
            }
            Expression::BitShiftRight(lhs, rhs) => {
                let lhs = Self::evaluate_expression(lhs.clone(), data.clone(), scope)?;
                let rhs = Self::evaluate_expression(rhs.clone(), data.clone(), scope)?;

                (lhs >> rhs).ok_or_else(|| ApplicationError::InvalidExpression(expression.clone()))
            }
            Expression::BitShiftLeft(lhs, rhs) => {
                let lhs = Self::evaluate_expression(lhs.clone(), data.clone(), scope)?;
                let rhs = Self::evaluate_expression(rhs.clone(), data.clone(), scope)?;

                (lhs << rhs).ok_or_else(|| ApplicationError::InvalidExpression(expression.clone()))
            }
            Expression::Equal(lhs, rhs) => {
                let lhs = Self::evaluate_expression(lhs.clone(), data.clone(), scope)?;
                let rhs = Self::evaluate_expression(rhs.clone(), data.clone(), scope)?;

                if lhs.is_undefined() || rhs.is_undefined() {
                    return Ok(Value::Boolean(true));
                }

                Ok(Value::Boolean(lhs.equals(&rhs)))
            }
            Expression::NotEqual(lhs, rhs) => {
                let lhs = Self::evaluate_expression(lhs.clone(), data.clone(), scope)?;
                let rhs = Self::evaluate_expression(rhs.clone(), data.clone(), scope)?;

                if lhs.is_undefined() || rhs.is_undefined() {
                    return Ok(Value::Boolean(true));
                }

                Ok(Value::Boolean(!lhs.equals(&rhs)))
            }
            Expression::Contains(lhs, rhs) => {
                let lhs = Self::evaluate_expression(lhs.clone(), data.clone(), scope)?;
                let rhs = Self::evaluate_expression(rhs.clone(), data.clone(), scope)?;

                if lhs.is_undefined() || rhs.is_undefined() {
                    return Ok(Value::Boolean(true));
                }

                match lhs.contains(&rhs) {
                    Some(boolean) => Ok(boolean.into()),
                    None => Err(ApplicationError::InvalidExpression(expression.clone())),
                }
            }
            Expression::NotContains(lhs, rhs) => {
                let lhs = Self::evaluate_expression(lhs.clone(), data.clone(), scope)?;
                let rhs = Self::evaluate_expression(rhs.clone(), data.clone(), scope)?;

                if lhs.is_undefined() || rhs.is_undefined() {
                    return Ok(Value::Boolean(true));
                }

                match lhs.contains(&rhs) {
                    Some(boolean) => Ok(Value::Boolean(!boolean)),
                    None => Err(ApplicationError::InvalidExpression(expression.clone())),
                }
            }
            Expression::Lower(lhs, rhs) => {
                let lhs = Self::evaluate_expression(lhs.clone(), data.clone(), scope)?;
                let rhs = Self::evaluate_expression(rhs.clone(), data.clone(), scope)?;

                if lhs.is_undefined() || rhs.is_undefined() {
                    return Ok(Value::Boolean(true));
                }

                match lhs.partial_cmp(&rhs) {
                    Some(Ordering::Less) => Ok(Value::Boolean(true)),
                    Some(_) => Ok(Value::Boolean(false)),
                    _ => Err(ApplicationError::InvalidExpression(expression.clone())),
                }
            }
            Expression::LowerOrEqual(lhs, rhs) => {
                let lhs = Self::evaluate_expression(lhs.clone(), data.clone(), scope)?;
                let rhs = Self::evaluate_expression(rhs.clone(), data.clone(), scope)?;

                if lhs.is_undefined() || rhs.is_undefined() {
                    return Ok(Value::Boolean(true));
                }

                match lhs.partial_cmp(&rhs) {
                    Some(Ordering::Less) | Some(Ordering::Equal) => Ok(Value::Boolean(true)),
                    Some(_) => Ok(Value::Boolean(false)),
                    _ => Err(ApplicationError::InvalidExpression(expression.clone())),
                }
            }
            Expression::Greater(lhs, rhs) => {
                let lhs = Self::evaluate_expression(lhs.clone(), data.clone(), scope)?;
                let rhs = Self::evaluate_expression(rhs.clone(), data.clone(), scope)?;

                if lhs.is_undefined() || rhs.is_undefined() {
                    return Ok(Value::Boolean(true));
                }

                match lhs.partial_cmp(&rhs) {
                    Some(Ordering::Greater) => Ok(Value::Boolean(true)),
                    Some(_) => Ok(Value::Boolean(false)),
                    _ => Err(ApplicationError::InvalidExpression(expression.clone())),
                }
            }
            Expression::GreaterOrEqual(lhs, rhs) => {
                let lhs = Self::evaluate_expression(lhs.clone(), data.clone(), scope)?;
                let rhs = Self::evaluate_expression(rhs.clone(), data.clone(), scope)?;

                if lhs.is_undefined() || rhs.is_undefined() {
                    return Ok(Value::Boolean(true));
                }

                match lhs.partial_cmp(&rhs) {
                    Some(Ordering::Greater) | Some(Ordering::Equal) => Ok(Value::Boolean(true)),
                    Some(_) => Ok(Value::Boolean(false)),
                    _ => Err(ApplicationError::InvalidExpression(expression.clone())),
                }
            }
            Expression::And(lhs, rhs) => {
                let lhs = Self::evaluate_expression(lhs.clone(), data.clone(), scope)?;

                let lhs = match lhs {
                    Value::Boolean(lhs) => Some(lhs),
                    Value::Undefined => Some(true),
                    _ => None,
                };

                if let Some(lhs) = lhs {
                    if !lhs {
                        return Ok(Value::Boolean(false));
                    }

                    let rhs = Self::evaluate_expression(rhs.clone(), data.clone(), scope)?;

                    let rhs = match rhs {
                        Value::Boolean(rhs) => Some(rhs),
                        Value::Undefined => Some(true),
                        _ => None,
                    };

                    if let Some(rhs) = rhs {
                        return Ok(Value::Boolean(rhs));
                    }
                }

                Err(ApplicationError::InvalidExpression(expression.clone()))
            }
            Expression::Or(lhs, rhs) => {
                let lhs = Self::evaluate_expression(lhs.clone(), data.clone(), scope)?;

                let lhs = match lhs {
                    Value::Boolean(lhs) => Some(lhs),
                    Value::Undefined => Some(false),
                    _ => None,
                };

                if let Some(lhs) = lhs {
                    if lhs {
                        return Ok(Value::Boolean(true));
                    }

                    let rhs = Self::evaluate_expression(rhs.clone(), data.clone(), scope)?;

                    let rhs = match rhs {
                        Value::Boolean(rhs) => Some(rhs),
                        Value::Undefined => Some(true),
                        _ => None,
                    };

                    if let Some(rhs) = rhs {
                        return Ok(Value::Boolean(rhs));
                    }
                }

                Err(ApplicationError::InvalidExpression(expression.clone()))
            }
            _ => Ok(Value::Null),
        }
    }

    fn validate_arguments_len(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        size: usize,
    ) -> Result<(), FunctionInvocationError> {
        if arguments.len() != size {
            Err(format!("{} must have exactly {} arguments.", fn_name, size).into())
        } else {
            Ok(())
        }
    }

    fn validate_arguments_range(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        range: Range<usize>,
    ) -> Result<usize, FunctionInvocationError> {
        let length = arguments.len();

        if length >= range.start && length < range.end {
            Ok(length)
        } else {
            Err(format!(
                "{} must have between {} and {} arguments.",
                fn_name,
                range.start,
                range.end - 1
            )
            .into())
        }
    }

    fn get_argument(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        index: usize,
        argument_name: Option<&str>,
    ) -> Result<Arc<Argument>, FunctionInvocationError> {
        let argument_name = match argument_name {
            Some(argument_name) => vec![argument_name],
            None => vec![],
        };

        Self::get_argument_multiple_names(fn_name, arguments, index, argument_name)
    }

    fn get_argument_multiple_names(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        index: usize,
        argument_names: Vec<&str>,
    ) -> Result<Arc<Argument>, FunctionInvocationError> {
        let argument: Option<Arc<Argument>> = arguments.get(index).cloned();
        if let Some(argument) = argument {
            if argument.is_named() {
                let name = argument.name.as_ref().unwrap();

                if argument_names.is_empty() {
                    Err(format!(
                        "{} argument in the position number {} must be unnamed.",
                        fn_name, index
                    )
                    .into())
                } else if !argument_names.contains(&name.as_str()) {
                    Err(format!(
                        "{} argument in the position number {} must be one of [{}].",
                        fn_name,
                        index,
                        argument_names.join(", ")
                    )
                    .into())
                } else {
                    Ok(argument)
                }
            } else if argument_names.is_empty() || argument_names.contains(&"") {
                Ok(argument)
            } else {
                Err(format!(
                    "{} argument in the position number {} must be one of [{}].",
                    fn_name,
                    index,
                    argument_names.join(", ")
                )
                .into())
            }
        } else {
            Err(format!(
                "{} is missing the argument in the position number {}.",
                fn_name, index
            )
            .into())
        }
    }

    fn optional_function_result(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        result: Option<Value>,
    ) -> Result<Value, FunctionInvocationError> {
        result.ok_or_else(|| {
            let mut arguments_string = String::new();

            let mut comma = false;
            for argument in arguments {
                if comma {
                    arguments_string += ", ";
                }

                arguments_string += argument.to_string().as_str();
                comma = true;
            }

            format!(
                "{} could not process your request, double-check your arguments. {}({})",
                fn_name, fn_name, arguments_string
            )
            .into()
        })
    }

    fn evaluate_function_call(
        name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        let fn_name = name.to_lowercase();
        let fn_name = fn_name.as_str();

        match fn_name {
            // Overloaded functions
            "max" => Self::fn_max(fn_name, arguments, data, scope),
            "min" => Self::fn_min(fn_name, arguments, data, scope),
            "avg" => Self::fn_avg(fn_name, arguments, data, scope),
            "sum" => Self::fn_sum(fn_name, arguments, data, scope),

            // Aggregate functions
            "count" => Self::fn_count(fn_name, arguments, data, scope),

            // Value functions
            "is_null" => Self::fn_is_null(fn_name, arguments, data, scope),
            "to_string" => Self::fn_to_string(fn_name, arguments, data, scope),
            "type_of" => Self::fn_type_of(fn_name, arguments, data, scope),
            "nvl" | "coalesce" => Self::fn_nvl(fn_name, arguments, data, scope),
            "case" => Self::fn_case(fn_name, arguments, data, scope),

            // String functions
            "is_ascii" => Self::fn_is_ascii(fn_name, arguments, data, scope),
            "replace" => Self::fn_replace(fn_name, arguments, data, scope),
            "lower" => Self::fn_lower(fn_name, arguments, data, scope),
            "ascii_lower" => Self::fn_ascii_lower(fn_name, arguments, data, scope),
            "upper" => Self::fn_upper(fn_name, arguments, data, scope),
            "ascii_upper" => Self::fn_ascii_upper(fn_name, arguments, data, scope),
            "substring" | "subs" => Self::fn_substring(fn_name, arguments, data, scope),
            "len" => Self::fn_len(fn_name, arguments, data, scope),
            "trim" => Self::fn_trim(fn_name, arguments, data, scope),
            "trim_start" | "trim_left" | "l_trim" | "ltrim" => {
                Self::fn_trim_start(fn_name, arguments, data, scope)
            }
            "trim_end" | "trim_right" | "r_trim" | "rtrim" => {
                Self::fn_trim_end(fn_name, arguments, data, scope)
            }
            "index_of" => Self::fn_index_of(fn_name, arguments, data, scope),
            "repeat" | "replicate" => Self::fn_repeat(fn_name, arguments, data, scope),

            // Date functions
            "now" => Self::fn_now(fn_name, arguments, data, scope),
            "to_date" => Self::fn_to_date(fn_name, arguments, data, scope),
            "seconds" | "second" | "sec" => Self::fn_seconds(fn_name, arguments, data, scope),
            "minutes" | "minute" => Self::fn_minutes(fn_name, arguments, data, scope),
            "hours" | "hour" => Self::fn_hours(fn_name, arguments, data, scope),
            "days" | "day" => Self::fn_days(fn_name, arguments, data, scope),
            "weeks" | "week" => Self::fn_weeks(fn_name, arguments, data, scope),
            "seconds_between" => Self::fn_seconds_between(fn_name, arguments, data, scope),
            "minutes_between" => Self::fn_minutes_between(fn_name, arguments, data, scope),
            "hours_between" => Self::fn_hours_between(fn_name, arguments, data, scope),
            "days_between" => Self::fn_days_between(fn_name, arguments, data, scope),
            "weeks_between" => Self::fn_weeks_between(fn_name, arguments, data, scope),

            // Number functions
            "abs" => Self::fn_abs(fn_name, arguments, data, scope),
            "sqrt" => Self::fn_sqrt(fn_name, arguments, data, scope),
            "pow" => Self::fn_pow(fn_name, arguments, data, scope),
            "floor" => Self::fn_floor(fn_name, arguments, data, scope),
            "ceil" => Self::fn_ceil(fn_name, arguments, data, scope),
            "round" => Self::fn_round(fn_name, arguments, data, scope),
            "binary_string" | "as_binary" | "to_binary" | "bin_string" | "as_bin" | "to_bin" => {
                Self::fn_binary_string(fn_name, arguments, data, scope)
            }
            "octal_string" | "as_octal" | "to_octal" | "oct_string" | "as_oct" | "to_oct" => {
                Self::fn_octal_string(fn_name, arguments, data, scope)
            }
            "hexadecimal_string" | "as_hexadecimal" | "to_hexadecimal" | "hex_string"
            | "as_hex" | "to_hex" => Self::fn_hexadecimal_string(fn_name, arguments, data, scope),

            _ => Err(format!("{} is not a known function name.", name).into()),
        }
    }

    fn fn_max(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        if arguments.len() == 0 {
            return Err(format!("{} must have at least 1 argument.", fn_name).into());
        }

        match (scope, arguments.len()) {
            (StatementScope::Where, length) | (StatementScope::Group, length) if length == 1 => {
                let argument = Self::get_argument(fn_name, arguments, 0, None)?;
                let argument = Self::evaluate_expression(argument.expression.clone(), data, scope)?;

                let result = match argument {
                    Value::Set(_) => functions::max_set(argument),
                    _ => functions::max_values(vec![argument]),
                };

                Ok(result)
            }

            (StatementScope::Select, length)
            | (StatementScope::Having, length)
            | (StatementScope::Sort, length)
                if length == 1 =>
            {
                let argument = Self::get_argument(fn_name, arguments, 0, None)?;

                match data {
                    ContextData::Aggregated(_, rows) => {
                        Ok(Self::aggregate_max(argument, rows.as_ref(), scope)?)
                    }
                    _ => {
                        let argument =
                            Self::evaluate_expression(argument.expression.clone(), data, scope)?;

                        let result = match argument {
                            Value::Set(_) => functions::max_set(argument),
                            _ => functions::max_values(vec![argument]),
                        };

                        Ok(result)
                    }
                }
            }

            _ => {
                let expressions: Result<Vec<Value>, ApplicationError> = arguments
                    .iter()
                    .map(|argument| {
                        Self::evaluate_expression(argument.expression.clone(), data.clone(), scope)
                    })
                    .collect();
                Ok(functions::max_values(expressions?))
            }
        }
    }

    fn fn_min(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        if arguments.len() == 0 {
            return Err(format!("{} must have at least 1 argument.", fn_name).into());
        }

        match (scope, arguments.len()) {
            (StatementScope::Where, length) | (StatementScope::Group, length) if length == 1 => {
                let argument = Self::get_argument(fn_name, arguments, 0, None)?;
                let argument = Self::evaluate_expression(argument.expression.clone(), data, scope)?;

                let result = match argument {
                    Value::Set(_) => functions::min_set(argument),
                    _ => functions::min_values(vec![argument]),
                };

                Ok(result)
            }

            (StatementScope::Select, length)
            | (StatementScope::Having, length)
            | (StatementScope::Sort, length)
                if length == 1 =>
            {
                let argument = Self::get_argument(fn_name, arguments, 0, None)?;

                match data {
                    ContextData::Aggregated(_, rows) => {
                        Ok(Self::aggregate_min(argument, rows.as_ref(), scope)?)
                    }
                    _ => {
                        let argument =
                            Self::evaluate_expression(argument.expression.clone(), data, scope)?;

                        let result = match argument {
                            Value::Set(_) => functions::min_set(argument),
                            _ => functions::min_values(vec![argument]),
                        };

                        Ok(result)
                    }
                }
            }

            _ => {
                let expressions: Result<Vec<Value>, ApplicationError> = arguments
                    .iter()
                    .map(|argument| {
                        Self::evaluate_expression(argument.expression.clone(), data.clone(), scope)
                    })
                    .collect();
                Ok(functions::min_values(expressions?))
            }
        }
    }

    fn fn_avg(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        if arguments.len() == 0 {
            return Err(format!("{} must have at least 1 argument.", fn_name).into());
        }

        match (scope, arguments.len()) {
            (StatementScope::Where, length) | (StatementScope::Group, length) if length == 1 => {
                let argument = Self::get_argument(fn_name, arguments, 0, None)?;
                let argument = Self::evaluate_expression(argument.expression.clone(), data, scope)?;

                let result = match argument {
                    Value::Set(_) => functions::avg_set(argument),
                    _ => functions::avg_numbers(vec![argument]),
                };

                Self::optional_function_result(fn_name, arguments, result)
            }

            (StatementScope::Select, length)
            | (StatementScope::Having, length)
            | (StatementScope::Sort, length)
                if length == 1 =>
            {
                let argument = Self::get_argument(fn_name, arguments, 0, None)?;

                match data {
                    ContextData::Aggregated(_, rows) => {
                        let result = Self::aggregate_avg(argument, rows.as_ref(), scope)?;
                        Self::optional_function_result(fn_name, arguments, result)
                    }
                    _ => {
                        let argument =
                            Self::evaluate_expression(argument.expression.clone(), data, scope)?;

                        let result = match argument {
                            Value::Set(_) => functions::avg_set(argument),
                            _ => functions::avg_numbers(vec![argument]),
                        };

                        Self::optional_function_result(fn_name, arguments, result)
                    }
                }
            }

            _ => {
                let expressions: Result<Vec<Value>, ApplicationError> = arguments
                    .iter()
                    .map(|argument| {
                        Self::evaluate_expression(argument.expression.clone(), data.clone(), scope)
                    })
                    .collect();
                let result = functions::avg_numbers(expressions?);
                Self::optional_function_result(fn_name, arguments, result)
            }
        }
    }

    fn fn_sum(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        if arguments.len() == 0 {
            return Err(format!("{} must have at least 1 argument.", fn_name).into());
        }

        match (scope, arguments.len()) {
            (StatementScope::Where, length) | (StatementScope::Group, length) if length == 1 => {
                let argument = Self::get_argument(fn_name, arguments, 0, None)?;
                let argument = Self::evaluate_expression(argument.expression.clone(), data, scope)?;

                let result = match argument {
                    Value::Set(_) => functions::sum_set(argument),
                    _ => functions::sum_numbers(vec![argument]),
                };

                Self::optional_function_result(fn_name, arguments, result)
            }

            (StatementScope::Select, length)
            | (StatementScope::Having, length)
            | (StatementScope::Sort, length)
                if length == 1 =>
            {
                let argument = Self::get_argument(fn_name, arguments, 0, None)?;

                match data {
                    ContextData::Aggregated(_, rows) => {
                        let result = Self::aggregate_sum(argument, rows.as_ref(), scope)?;
                        Self::optional_function_result(fn_name, arguments, result)
                    }
                    _ => {
                        let argument =
                            Self::evaluate_expression(argument.expression.clone(), data, scope)?;

                        let result = match argument {
                            Value::Set(_) => functions::sum_set(argument),
                            _ => functions::sum_numbers(vec![argument]),
                        };

                        Self::optional_function_result(fn_name, arguments, result)
                    }
                }
            }

            _ => {
                let expressions: Result<Vec<Value>, ApplicationError> = arguments
                    .iter()
                    .map(|argument| {
                        Self::evaluate_expression(argument.expression.clone(), data.clone(), scope)
                    })
                    .collect();
                let result = functions::sum_numbers(expressions?);
                Self::optional_function_result(fn_name, arguments, result)
            }
        }
    }

    fn fn_count(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        match scope {
            StatementScope::Where | StatementScope::Group => {
                return Err(FunctionInvocationError::InvalidScope(scope));
            }
            _ => {}
        }
        match &data {
            ContextData::Aggregated(_, rows) => {
                let size = Self::validate_arguments_range(fn_name, arguments, 0..2)?;

                if size == 0 {
                    Ok(Self::aggregate_count(rows))
                } else {
                    let argument = Self::get_argument(fn_name, arguments, 0, Some("unique"))?;

                    Ok(Self::aggregate_count_unique(
                        argument,
                        rows.as_ref(),
                        scope,
                    )?)
                }
            }
            _ => Err(FunctionInvocationError::ExpressionRequiresAggregateContextData),
        }
    }

    fn fn_is_null(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;
        let argument = Self::evaluate_expression(argument.expression.clone(), data, scope)?;

        Ok(functions::is_null(argument))
    }

    fn fn_to_string(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;
        let argument = Self::evaluate_expression(argument.expression.clone(), data, scope)?;

        Ok(functions::to_string(argument))
    }

    fn fn_type_of(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;
        let argument = Self::evaluate_expression(argument.expression.clone(), data, scope)?;

        Ok(functions::type_of(&argument))
    }

    fn fn_nvl(
        _fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        let arguments: Result<Vec<Value>, ApplicationError> = arguments
            .iter()
            .map(|argument| {
                Self::evaluate_expression(argument.expression.clone(), data.clone(), scope)
            })
            .collect();
        Ok(functions::nvl(arguments?))
    }

    fn fn_case(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 3)?;
        let condition = Self::get_argument(fn_name, arguments, 0, Some("when"))?;
        let lhs = Self::get_argument(fn_name, arguments, 1, Some("then"))?;
        let rhs = Self::get_argument(fn_name, arguments, 2, Some("else"))?;

        let condition =
            Self::evaluate_expression(condition.expression.clone(), data.clone(), scope)?;
        let lhs = Self::evaluate_expression(lhs.expression.clone(), data.clone(), scope)?;
        let rhs = Self::evaluate_expression(rhs.expression.clone(), data.clone(), scope)?;

        Self::optional_function_result(fn_name, arguments, functions::case(condition, lhs, rhs))
    }

    fn fn_is_ascii(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;
        let argument = Self::evaluate_expression(argument.expression.clone(), data, scope)?;

        Ok(functions::is_ascii(argument))
    }

    fn fn_replace(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        let size = Self::validate_arguments_range(fn_name, arguments, 3..5)?;
        let string = Self::get_argument(fn_name, arguments, 0, None)?;
        let source = Self::get_argument(fn_name, arguments, 1, Some("from"))?;
        let target = Self::get_argument(fn_name, arguments, 2, Some("to"))?;

        let string = Self::evaluate_expression(string.expression.clone(), data.clone(), scope)?;
        let source = Self::evaluate_expression(source.expression.clone(), data.clone(), scope)?;
        let target = Self::evaluate_expression(target.expression.clone(), data.clone(), scope)?;

        if size == 3 {
            Ok(functions::replace(string, source, target))
        } else {
            let count = Self::get_argument(fn_name, arguments, 3, Some("count"))?;
            let count = Self::evaluate_expression(count.expression.clone(), data.clone(), scope)?;

            Self::optional_function_result(
                fn_name,
                arguments,
                functions::replace_count(string, source, target, count),
            )
        }
    }

    fn fn_lower(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;
        let argument = Self::evaluate_expression(argument.expression.clone(), data, scope)?;

        Ok(functions::lower(argument))
    }

    fn fn_ascii_lower(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;
        let argument = Self::evaluate_expression(argument.expression.clone(), data, scope)?;

        Ok(functions::ascii_lower(argument))
    }

    fn fn_upper(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;
        let argument = Self::evaluate_expression(argument.expression.clone(), data, scope)?;

        Ok(functions::upper(argument))
    }

    fn fn_ascii_upper(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;
        let argument = Self::evaluate_expression(argument.expression.clone(), data, scope)?;

        Ok(functions::ascii_upper(argument))
    }

    fn fn_substring(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_range(fn_name, arguments, 2..3)?;
        let value = Self::get_argument(fn_name, arguments, 0, None)?;
        let from_or_to =
            Self::get_argument_multiple_names(fn_name, arguments, 1, vec!["from", "to"])?;

        let name = from_or_to.name.as_ref().map(|s| s.as_str());

        match name {
            Some("from") => {
                let to = Self::get_argument(fn_name, arguments, 2, Some("to"));

                let value =
                    Self::evaluate_expression(value.expression.clone(), data.clone(), scope)?;
                let from =
                    Self::evaluate_expression(from_or_to.expression.clone(), data.clone(), scope)?;

                if let Ok(to) = to {
                    let to = Self::evaluate_expression(to.expression.clone(), data.clone(), scope)?;
                    Self::optional_function_result(
                        fn_name,
                        arguments,
                        functions::substring(value, from, Some(to)),
                    )
                } else {
                    Self::optional_function_result(
                        fn_name,
                        arguments,
                        functions::substring(value, from, None),
                    )
                }
            }
            Some("to") => {
                let value =
                    Self::evaluate_expression(value.expression.clone(), data.clone(), scope)?;
                let to =
                    Self::evaluate_expression(from_or_to.expression.clone(), data.clone(), scope)?;

                Self::optional_function_result(
                    fn_name,
                    arguments,
                    functions::substring(value, Value::Integer(0), Some(to)),
                )
            }
            _ => unreachable!(),
        }
    }

    fn fn_len(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;
        let argument = Self::evaluate_expression(argument.expression.clone(), data, scope)?;

        Ok(functions::len(argument))
    }

    fn fn_trim(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;
        let argument = Self::evaluate_expression(argument.expression.clone(), data, scope)?;

        Ok(functions::trim(argument))
    }

    fn fn_trim_start(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;
        let argument = Self::evaluate_expression(argument.expression.clone(), data, scope)?;

        Ok(functions::trim_start(argument))
    }

    fn fn_trim_end(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;
        let argument = Self::evaluate_expression(argument.expression.clone(), data, scope)?;

        Ok(functions::trim_end(argument))
    }

    fn fn_index_of(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 2)?;
        let search = Self::get_argument(fn_name, arguments, 0, None)?;
        let into = Self::get_argument(fn_name, arguments, 1, Some("into"))?;

        let search = Self::evaluate_expression(search.expression.clone(), data.clone(), scope)?;
        let into = Self::evaluate_expression(into.expression.clone(), data.clone(), scope)?;

        Ok(functions::index_of(search, into))
    }

    fn fn_repeat(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 2)?;
        let text = Self::get_argument(fn_name, arguments, 0, None)?;
        let count = Self::get_argument(fn_name, arguments, 1, Some("count"))?;

        let text = Self::evaluate_expression(text.expression.clone(), data.clone(), scope)?;
        let count = Self::evaluate_expression(count.expression.clone(), data.clone(), scope)?;

        Ok(functions::repeat(text, count))
    }

    fn fn_now(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        _data: ContextData,
        _scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 0)?;
        Ok(functions::now())
    }

    fn fn_to_date(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 2)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;
        let format = Self::get_argument(fn_name, arguments, 1, Some("format"))?;

        let argument = Self::evaluate_expression(argument.expression.clone(), data.clone(), scope)?;
        let format = Self::evaluate_expression(format.expression.clone(), data.clone(), scope)?;

        Ok(functions::to_date(argument, format))
    }

    fn fn_seconds(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;

        let argument = Self::evaluate_expression(argument.expression.clone(), data.clone(), scope)?;

        Self::optional_function_result(fn_name, arguments, functions::seconds(argument))
    }

    fn fn_minutes(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;

        let argument = Self::evaluate_expression(argument.expression.clone(), data.clone(), scope)?;

        Self::optional_function_result(fn_name, arguments, functions::minutes(argument))
    }

    fn fn_hours(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;

        let argument = Self::evaluate_expression(argument.expression.clone(), data.clone(), scope)?;

        Self::optional_function_result(fn_name, arguments, functions::hours(argument))
    }

    fn fn_days(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;

        let argument = Self::evaluate_expression(argument.expression.clone(), data.clone(), scope)?;

        Self::optional_function_result(fn_name, arguments, functions::days(argument))
    }

    fn fn_weeks(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;

        let argument = Self::evaluate_expression(argument.expression.clone(), data.clone(), scope)?;

        Self::optional_function_result(fn_name, arguments, functions::weeks(argument))
    }

    fn fn_seconds_between(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 2)?;
        let from = Self::get_argument(fn_name, arguments, 0, Some("from"))?;
        let to = Self::get_argument(fn_name, arguments, 1, Some("to"))?;

        let from = Self::evaluate_expression(from.expression.clone(), data.clone(), scope)?;
        let to = Self::evaluate_expression(to.expression.clone(), data.clone(), scope)?;

        Self::optional_function_result(fn_name, arguments, functions::seconds_between(from, to))
    }

    fn fn_minutes_between(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 2)?;
        let from = Self::get_argument(fn_name, arguments, 0, Some("from"))?;
        let to = Self::get_argument(fn_name, arguments, 1, Some("to"))?;

        let from = Self::evaluate_expression(from.expression.clone(), data.clone(), scope)?;
        let to = Self::evaluate_expression(to.expression.clone(), data.clone(), scope)?;

        Self::optional_function_result(fn_name, arguments, functions::minutes_between(from, to))
    }

    fn fn_hours_between(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 2)?;
        let from = Self::get_argument(fn_name, arguments, 0, Some("from"))?;
        let to = Self::get_argument(fn_name, arguments, 1, Some("to"))?;

        let from = Self::evaluate_expression(from.expression.clone(), data.clone(), scope)?;
        let to = Self::evaluate_expression(to.expression.clone(), data.clone(), scope)?;

        Self::optional_function_result(fn_name, arguments, functions::hours_between(from, to))
    }

    fn fn_days_between(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 2)?;
        let from = Self::get_argument(fn_name, arguments, 0, Some("from"))?;
        let to = Self::get_argument(fn_name, arguments, 1, Some("to"))?;

        let from = Self::evaluate_expression(from.expression.clone(), data.clone(), scope)?;
        let to = Self::evaluate_expression(to.expression.clone(), data.clone(), scope)?;

        Self::optional_function_result(fn_name, arguments, functions::days_between(from, to))
    }

    fn fn_weeks_between(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 2)?;
        let from = Self::get_argument(fn_name, arguments, 0, Some("from"))?;
        let to = Self::get_argument(fn_name, arguments, 1, Some("to"))?;

        let from = Self::evaluate_expression(from.expression.clone(), data.clone(), scope)?;
        let to = Self::evaluate_expression(to.expression.clone(), data.clone(), scope)?;

        Self::optional_function_result(fn_name, arguments, functions::weeks_between(from, to))
    }

    fn fn_abs(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;

        let argument = Self::evaluate_expression(argument.expression.clone(), data.clone(), scope)?;

        Self::optional_function_result(fn_name, arguments, functions::abs(argument))
    }

    fn fn_sqrt(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;

        let argument = Self::evaluate_expression(argument.expression.clone(), data.clone(), scope)?;

        Self::optional_function_result(fn_name, arguments, functions::sqrt(argument))
    }

    fn fn_pow(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 2)?;
        let base = Self::get_argument_multiple_names(fn_name, arguments, 0, vec!["", "base", "b"])?;
        let exponent =
            Self::get_argument_multiple_names(fn_name, arguments, 1, vec!["", "exponent", "e"])?;

        let base = Self::evaluate_expression(base.expression.clone(), data.clone(), scope)?;
        let exponent = Self::evaluate_expression(exponent.expression.clone(), data.clone(), scope)?;

        Self::optional_function_result(fn_name, arguments, functions::pow(base, exponent))
    }

    fn fn_floor(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;

        let argument = Self::evaluate_expression(argument.expression.clone(), data.clone(), scope)?;

        Self::optional_function_result(fn_name, arguments, functions::floor(argument))
    }

    fn fn_ceil(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;

        let argument = Self::evaluate_expression(argument.expression.clone(), data.clone(), scope)?;

        Self::optional_function_result(fn_name, arguments, functions::ceil(argument))
    }

    fn fn_round(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;

        let argument = Self::evaluate_expression(argument.expression.clone(), data.clone(), scope)?;

        Self::optional_function_result(fn_name, arguments, functions::round(argument))
    }

    fn fn_binary_string(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;

        let argument = Self::evaluate_expression(argument.expression.clone(), data.clone(), scope)?;

        Self::optional_function_result(fn_name, arguments, functions::binary_string(argument))
    }

    fn fn_octal_string(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;

        let argument = Self::evaluate_expression(argument.expression.clone(), data.clone(), scope)?;

        Self::optional_function_result(fn_name, arguments, functions::octal_string(argument))
    }

    fn fn_hexadecimal_string(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument(fn_name, arguments, 0, None)?;

        let argument = Self::evaluate_expression(argument.expression.clone(), data.clone(), scope)?;

        Self::optional_function_result(fn_name, arguments, functions::hexadecimal_string(argument))
    }

    fn fn_is_merge_commit(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument_multiple_names(
            fn_name,
            arguments,
            0,
            vec!["", "hash", "commit", "commit_hash"],
        )?;

        let argument = Self::evaluate_expression(argument.expression.clone(), data.clone(), scope)?;

        let result = Self::commit_hash_argument(argument, data, |commit| {
            Some(Value::Boolean(commit.parent_ids().len() > 1))
        });

        Self::optional_function_result(fn_name, arguments, result)
    }

    fn fn_is_root_commit(
        fn_name: &str,
        arguments: &[Arc<Argument>],
        data: ContextData,
        scope: StatementScope,
    ) -> Result<Value, FunctionInvocationError> {
        Self::validate_arguments_len(fn_name, arguments, 1)?;
        let argument = Self::get_argument_multiple_names(
            fn_name,
            arguments,
            0,
            vec!["", "hash", "commit", "commit_hash"],
        )?;

        let argument = Self::evaluate_expression(argument.expression.clone(), data.clone(), scope)?;

        let result = Self::commit_hash_argument(argument, data, |commit| {
            Some(Value::Boolean(commit.parent_ids().len() == 0))
        });

        Self::optional_function_result(fn_name, arguments, result)
    }

    fn commit_hash_argument<F>(value: Value, data: ContextData, function: F) -> Option<Value>
    where
        F: Fn(Commit) -> Option<Value>,
    {
        match value {
            Value::String(hash) => {
                let oid = Oid::from_str(hash.as_str()).ok();

                let row = match data {
                    ContextData::Row(row) => Some(row.clone()),
                    ContextData::Aggregated(row, _) => row.clone(),
                };

                match (oid, row) {
                    (Some(oid), Some(row)) => {
                        row.repository.find_commit(oid).ok().and_then(function)
                    }
                    _ => None,
                }
            }
            _ => None,
        }
    }

    fn aggregate_count(rows: &[Arc<SearchRow>]) -> Value {
        rows.len().into()
    }

    fn aggregate_count_unique(
        argument: Arc<Argument>,
        rows: &[Arc<SearchRow>],
        scope: StatementScope,
    ) -> Result<Value, ApplicationError> {
        let mut set = HashSet::with_capacity(rows.len());

        for row in rows {
            set.insert(Self::evaluate_expression(
                argument.expression.clone(),
                ContextData::Row(row.clone()),
                scope,
            )?);
        }

        Ok(set.len().into())
    }

    fn aggregate_max(
        argument: Arc<Argument>,
        rows: &[Arc<SearchRow>],
        scope: StatementScope,
    ) -> Result<Value, ApplicationError> {
        let mut max: Option<Value> = None;

        for row in rows {
            let value = Self::evaluate_expression(
                argument.expression.clone(),
                ContextData::Row(row.clone()),
                scope,
            )?;

            let m = match max {
                Some(max) => max.max(value),
                None => value,
            };

            max = Some(m);
        }

        Ok(max.into())
    }

    fn aggregate_min(
        argument: Arc<Argument>,
        rows: &[Arc<SearchRow>],
        scope: StatementScope,
    ) -> Result<Value, ApplicationError> {
        let mut min: Option<Value> = None;

        for row in rows {
            let value = Self::evaluate_expression(
                argument.expression.clone(),
                ContextData::Row(row.clone()),
                scope,
            )?;

            let m = match min {
                Some(min) => min.min(value),
                None => value,
            };

            min = Some(m);
        }

        Ok(min.into())
    }

    fn aggregate_avg(
        argument: Arc<Argument>,
        rows: &[Arc<SearchRow>],
        scope: StatementScope,
    ) -> Result<Option<Value>, ApplicationError> {
        let mut sum = Value::Integer(0);

        for row in rows {
            let value = Self::evaluate_expression(
                argument.expression.clone(),
                ContextData::Row(row.clone()),
                scope,
            )?;

            match sum + value {
                Some(value) => sum = value,
                None => return Ok(None),
            }
        }

        Ok(sum / Value::Float(rows.len() as f64))
    }

    fn aggregate_sum(
        argument: Arc<Argument>,
        rows: &[Arc<SearchRow>],
        scope: StatementScope,
    ) -> Result<Option<Value>, ApplicationError> {
        let mut sum = Value::Integer(0);

        for row in rows {
            let value = Self::evaluate_expression(
                argument.expression.clone(),
                ContextData::Row(row.clone()),
                scope,
            )?;

            match sum + value {
                Some(value) => sum = value,
                None => return Ok(None),
            }
        }

        Ok(Some(sum))
    }
}

#[derive(Eq, PartialEq, Debug, Clone, Copy, Serialize, Deserialize)]
pub struct SearchMetadata {
    pub scanned_commits: usize,
    pub scanned_commit_diffs: usize,
    pub scanned_commit_diff_deltas: usize,
    pub time: u128,
    pub time_on_commits: u128,
    pub average_time_per_commit_diff: u128,
    pub average_time_per_commit_diff_delta: u128,
    pub result_count: usize,
}

impl SearchMetadata {
    fn empty() -> Self {
        Self {
            scanned_commits: 0,
            scanned_commit_diffs: 0,
            scanned_commit_diff_deltas: 0,
            time: 0,
            time_on_commits: 0,
            average_time_per_commit_diff: 0,
            average_time_per_commit_diff_delta: 0,
            result_count: 0,
        }
    }
}

#[derive(PartialEq, Debug, Clone, Serialize)]
pub struct SearchResult {
    pub headers: Vec<String>,
    pub data: Vec<Vec<Value>>,
    pub metadata: SearchMetadata,
}

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct Stopwatch {
    start: SystemTime,
}

impl Stopwatch {
    fn start() -> Self {
        Self {
            start: SystemTime::now(),
        }
    }

    fn stop(self) -> u128 {
        SystemTime::now()
            .duration_since(self.start)
            .map(|d| d.as_micros())
            .unwrap_or(0)
    }
}

pub fn filter(request: Request) -> Result<SearchResult, ApplicationError> {
    let stopwatch = Stopwatch::start();

    let statement = ast::generate_ast(request.query.as_str())?;

    let select = statement.get_select();

    let mut search = Search::new(&statement, &select);
    let values = search.search(statement, select, &request)?;

    search.metadata.time = stopwatch.stop();

    let headers = replace(&mut search.context.headers, Vec::new());
    let metadata = search.metadata;

    drop(search);

    Ok(SearchResult {
        headers,
        data: values,
        metadata,
    })
}

#[cfg(test)]
mod tests {
    use super::{repository::RequestCredentials, *};
    use std::fs::remove_dir_all;
    use std::io::Result as IOResult;
    use std::panic::{catch_unwind, UnwindSafe};

    const TEST_URL: &str = "https://github.com/rafaelGuerreiro/rustup.rs.git";
    const TEST_PATH: &str = "./temp/rustup";

    fn filter(gql: &str, url: &str, path: &str) -> Result<SearchResult, ApplicationError> {
        super::filter(Request {
            url: url.to_owned(),
            path: path.to_owned(),
            credentials: RequestCredentials::None,
            query: gql.to_owned(),
        })
    }

    fn run_test<T>(path: &str, test: T) -> Result<(), ApplicationError>
    where
        T: FnOnce() -> Result<(), ApplicationError> + UnwindSafe,
    {
        let result = catch_unwind(|| test());
        let _ = teardown(path);
        assert!(result.is_ok());
        result.unwrap()
    }

    fn teardown(path: &str) -> IOResult<()> {
        remove_dir_all(path)
    }

    #[test]
    fn test_ranges_per_thread() {
        assert_eq!(Search::ranges_per_thread(1000, 0), vec![]);
        assert_eq!(Search::ranges_per_thread(0, 1), vec![]);
        assert_eq!(Search::ranges_per_thread(2000, 1), vec![0..2000]);
        assert_eq!(
            Search::ranges_per_thread(2000, 4),
            vec![0..500, 500..1000, 1000..1500, 1500..2000]
        );
        assert_eq!(
            Search::ranges_per_thread(2057, 4),
            vec![0..514, 514..1028, 1028..1542, 1542..2057]
        );
    }

    #[test]
    fn test_commit_scoped_select_and_condition() -> Result<(), ApplicationError> {
        let path = TEST_PATH.to_owned() + "_test_commit_scoped_select_and_condition";
        let path = path.as_str();

        run_test(path, || {
            let result = filter(
                "select commit_hash as hash, author where author_name = 'Nick Cameron' limit 1",
                TEST_URL,
                path,
            )?;

            assert_eq!(result.headers, vec!["hash".to_owned(), "author".to_owned()]);
            assert_eq!(
                result.data,
                vec![vec![
                    Value::String("170d7113fedee62839e4139dc63dcd5832c65a56".to_owned()),
                    Value::String("Nick Cameron <nrc@ncameron.org>".to_owned())
                ]]
            );

            Ok(())
        })
    }

    #[test]
    fn test_commit_scoped_group_by() -> Result<(), ApplicationError> {
        let path = TEST_PATH.to_owned() + "_test_commit_scoped_group_by";
        let path = path.as_str();

        run_test(path, || {
            let result = filter(
                r#"select
                        author
                    where
                        author_name ~ 'Chris' and
                        !is_merge_commit
                    group by
                        author_name"#,
                TEST_URL,
                path,
            )?;

            assert_eq!(result.headers, vec!["author".to_owned()]);
            assert_eq!(result.data.len(), 4);

            assert!(result.data.contains(&vec![Value::String(
                "Christian Muirhead <christian.muirhead@canonical.com>".to_owned()
            )]));
            assert!(result.data.contains(&vec![Value::String(
                "Christoph Wurst <ChristophWurst@users.noreply.github.com>".to_owned()
            )]));
            assert!(result.data.contains(&vec![Value::String(
                "Christopher Armstrong <radix@twistedmatrix.com>".to_owned()
            )]));
            assert!(result.data.contains(&vec![Value::String(
                "Kim Christensen <kimworking@gmail.com>".to_owned()
            )]));

            Ok(())
        })
    }

    #[test]
    fn test_date_comparison() -> Result<(), ApplicationError> {
        let path = TEST_PATH.to_owned() + "_test_date_comparison";
        let path = path.as_str();

        run_test(path, || {
            let result = filter(
                "select commit_hash, commit_date where commit_date > '2019-03-05T23:50:00Z'",
                TEST_URL,
                path,
            )?;

            let date_time = "2019-03-05T23:50:40Z";
            let date_time = NaiveDateTime::parse_from_str(date_time, DATE_TIME_FORMAT).unwrap();

            assert_eq!(
                result.headers,
                vec!["commit_hash".to_owned(), "commit_date".to_owned()]
            );
            assert_eq!(
                result.data,
                vec![vec![
                    Value::String("069c88ed6265fe0d3b8bf219316098cba5b75730".to_owned()),
                    Value::DateTime(DateTime::from_utc(date_time, Utc))
                ]]
            );

            Ok(())
        })
    }

    #[test]
    fn test_date_range() -> Result<(), ApplicationError> {
        let path = TEST_PATH.to_owned() + "_test_date_range";
        let path = path.as_str();

        run_test(path, || {
            let result = filter(
                "select commit_hash, commit_date where commit_date ~ '2019-03-04T10:30:00Z'..'2019-03-05T12:50:00Z'",
                TEST_URL, path
            )?;

            let commit_hash = "35d6f4bc96e8ca1b11b737adbf03c687ebbbbe03";
            let date_time = "2019-03-05T12:29:32Z";
            let date_time = NaiveDateTime::parse_from_str(date_time, DATE_TIME_FORMAT).unwrap();

            assert_eq!(
                result.headers,
                vec!["commit_hash".to_owned(), "commit_date".to_owned(),]
            );
            assert_eq!(
                result.data,
                vec![vec![
                    Value::String(commit_hash.to_owned()),
                    Value::DateTime(DateTime::from_utc(date_time, Utc)),
                ]]
            );

            Ok(())
        })
    }

    #[test]
    fn test_diff_scoped_select_and_commit_scoped_condition() -> Result<(), ApplicationError> {
        let path = TEST_PATH.to_owned() + "_test_diff_scoped_select_and_commit_scoped_condition";
        let path = path.as_str();

        run_test(path, || {
            let result = filter(
                r#"select
                            commit_hash, author,
                            added_files, deleted_files, renamed_files, modified_files
                        where
                            !is_merge_commit
                        limit 2"#,
                TEST_URL,
                path,
            )?;

            assert_eq!(
                result.headers,
                vec![
                    "commit_hash".to_owned(),
                    "author".to_owned(),
                    "added_files".to_owned(),
                    "deleted_files".to_owned(),
                    "renamed_files".to_owned(),
                    "modified_files".to_owned()
                ]
            );
            assert_eq!(
                result.data,
                vec![
                    vec![
                        Value::String("069c88ed6265fe0d3b8bf219316098cba5b75730".to_owned()),
                        Value::String("Daniel Silverstone <dsilvers@digital-scurf.org>".to_owned()),
                        Value::Integer(0),
                        Value::Integer(0),
                        Value::Integer(0),
                        Value::Integer(1),
                    ],
                    vec![
                        Value::String("edb52ac600249cee21368b4c863684d5641cb518".to_owned()),
                        Value::String("Daniel Silverstone <dsilvers@digital-scurf.org>".to_owned()),
                        Value::Integer(0),
                        Value::Integer(0),
                        Value::Integer(0),
                        Value::Integer(1),
                    ],
                ]
            );

            Ok(())
        })
    }

    #[test]
    fn test_diff_scoped_select_and_condition() -> Result<(), ApplicationError> {
        let path = TEST_PATH.to_owned() + "_test_diff_scoped_select_and_condition";
        let path = path.as_str();

        run_test(path, || {
            let result = filter(
                r#"select
                            commit_hash, author,
                            added_files, deleted_files, renamed_files, modified_files
                        where
                            deleted_files ~ 1..5 and
                            !is_merge_commit
                        limit 2"#,
                TEST_URL,
                path,
            )?;

            assert_eq!(
                result.headers,
                vec![
                    "commit_hash".to_owned(),
                    "author".to_owned(),
                    "added_files".to_owned(),
                    "deleted_files".to_owned(),
                    "renamed_files".to_owned(),
                    "modified_files".to_owned()
                ]
            );
            assert_eq!(
                result.data,
                vec![
                    vec![
                        Value::String("f84b79f8b4c011160cd57f78985b8ecde86d4297".to_owned()),
                        Value::String("Dale Wijnand <dale.wijnand@gmail.com>".to_owned()),
                        Value::Integer(0),
                        Value::Integer(2),
                        Value::Integer(0),
                        Value::Integer(17),
                    ],
                    vec![
                        Value::String("a699b6d3e6e3901ed7e82f9b091eefaa5620280e".to_owned()),
                        Value::String("Alex Crichton <alex@alexcrichton.com>".to_owned()),
                        Value::Integer(7),
                        Value::Integer(1),
                        Value::Integer(0),
                        Value::Integer(14),
                    ],
                ]
            );

            Ok(())
        })
    }

    #[test]
    fn test_delta_scoped_select_and_condition() -> Result<(), ApplicationError> {
        let path = TEST_PATH.to_owned() + "_test_delta_scoped_select_and_condition";
        let path = path.as_str();

        run_test(path, || {
            let result = filter(
                r#"select
                        commit_hash, new_file_path, new_file_name, new_file_extension
                    where
                        new_file_path = old_file_path and
                        new_file_path = [
                            'Cargo.toml',
                            'src/rustup-cli/common.rs',
                        ] and
                        !is_merge_commit
                    limit 3"#,
                TEST_URL,
                path,
            )?;

            assert_eq!(
                result.headers,
                vec![
                    "commit_hash".to_owned(),
                    "new_file_path".to_owned(),
                    "new_file_name".to_owned(),
                    "new_file_extension".to_owned(),
                ]
            );
            assert_eq!(
                result.data,
                vec![
                    vec![
                        Value::String("ec110b7dfa18e76c893c8c745382e8948fce8896".to_owned()),
                        Value::String("Cargo.toml".to_owned()),
                        Value::String("Cargo.toml".to_owned()),
                        Value::String("toml".to_owned()),
                    ],
                    vec![
                        Value::String("e40e42a80d54ea3500ccd99e35123247b83b77d8".to_owned()),
                        Value::String("Cargo.toml".to_owned()),
                        Value::String("Cargo.toml".to_owned()),
                        Value::String("toml".to_owned()),
                    ],
                    vec![
                        Value::String("8f58bb08214d233649f87bb7b64c5bd5a133ba19".to_owned()),
                        Value::String("src/rustup-cli/common.rs".to_owned()),
                        Value::String("common.rs".to_owned()),
                        Value::String("rs".to_owned()),
                    ],
                ]
            );

            Ok(())
        })
    }

    #[test]
    fn test_limit_and_offset() -> Result<(), ApplicationError> {
        let path = TEST_PATH.to_owned() + "_test_limit_and_offset";
        let path = path.as_str();

        run_test(path, || {
            let result = filter(
                r#"select
                            commit_hash
                        where
                            author_name ~ 'Nick' and
                            !is_merge_commit
                        limit 2"#,
                TEST_URL,
                path,
            )?;

            assert_eq!(result.headers, vec!["commit_hash".to_owned()]);
            assert_eq!(
                result.data,
                vec![
                    vec![Value::String(
                        "eaab76ec4d574850077e6e4f8a524a789e7a09eb".to_owned()
                    ),],
                    vec![Value::String(
                        "b1324856e6f10a3a73cef80a3da806a4418ffdde".to_owned()
                    ),]
                ]
            );

            let result = filter(
                r#"select
                            commit_hash
                        where
                            author_name ~ 'Nick' and
                            !is_merge_commit
                        offset 1
                        limit 1"#,
                TEST_URL,
                path,
            )?;

            assert_eq!(result.headers, vec!["commit_hash".to_owned()]);
            assert_eq!(
                result.data,
                vec![vec![Value::String(
                    "b1324856e6f10a3a73cef80a3da806a4418ffdde".to_owned()
                ),],]
            );

            let result = filter(
                r#"select
                            commit_hash
                        where
                            commit_hash = 'b1324856e6f10a3a73cef80a3da806a4418ffdde'
                        offset 1
                        limit 10"#,
                TEST_URL,
                path,
            )?;

            assert_eq!(result.headers, vec!["commit_hash".to_owned()]);
            assert_eq!(result.data, Vec::<Vec<Value>>::new());

            let result = filter(
                r#"select
                            commit_hash
                        where
                            commit_hash = 'b1324856e6f10a3a73cef80a3da806a4418ffdde'
                        offset 0
                        limit 10"#,
                TEST_URL,
                path,
            )?;

            assert_eq!(result.headers, vec!["commit_hash".to_owned()]);
            assert_eq!(
                result.data,
                vec![vec![Value::String(
                    "b1324856e6f10a3a73cef80a3da806a4418ffdde".to_owned()
                ),],]
            );

            Ok(())
        })
    }

    #[test]
    fn test_commit_scoped_order_by() -> Result<(), ApplicationError> {
        let path = TEST_PATH.to_owned() + "_test_commit_scoped_order_by";
        let path = path.as_str();

        run_test(path, || {
            let result = filter(
                r#"select
                    commit_hash, commit_date, author
                where
                    author ~ ['Nick', 'Alex'] and
                    !is_merge_commit
                order by
                    author_name desc, commit_date asc
                limit 2
                "#,
                TEST_URL,
                path,
            )?;

            let first_date_time = "2017-02-26T22:46:48Z";
            let first_date_time =
                NaiveDateTime::parse_from_str(first_date_time, DATE_TIME_FORMAT).unwrap();

            let second_date_time = "2017-08-16T22:40:13Z";
            let second_date_time =
                NaiveDateTime::parse_from_str(second_date_time, DATE_TIME_FORMAT).unwrap();

            assert_eq!(
                result.headers,
                vec![
                    "commit_hash".to_owned(),
                    "commit_date".to_owned(),
                    "author".to_owned()
                ]
            );
            assert_eq!(
                result.data,
                vec![
                    vec![
                        Value::String("79b93f902b2652e0df10ac979a8466661e55f361".to_owned()),
                        Value::DateTime(DateTime::from_utc(first_date_time, Utc)),
                        Value::String("Nick Cameron <ncameron@mozilla.com>".to_owned()),
                    ],
                    vec![
                        Value::String("80cfe5dfff14f7d49abe19932c6e932175e0b458".to_owned()),
                        Value::DateTime(DateTime::from_utc(second_date_time, Utc)),
                        Value::String("Nick Cameron <ncameron@mozilla.com>".to_owned()),
                    ]
                ]
            );

            Ok(())
        })
    }

    #[test]
    fn test_select_distinct() -> Result<(), ApplicationError> {
        let path = TEST_PATH.to_owned() + "_test_select_distinct";
        let path = path.as_str();

        run_test(path, || {
            let result = filter(
                r#"select distinct author_name
                        where
                            author_name ~ ['Nick', 'Alex Burka']
                        order by
                            author_name
                        "#,
                TEST_URL,
                path,
            )?;

            assert_eq!(result.headers, vec!["author_name".to_owned()]);
            assert_eq!(
                result.data,
                vec![
                    vec![Value::String("Alex Burka".to_owned())],
                    vec![Value::String("Nick Cameron".to_owned()),]
                ]
            );

            Ok(())
        })
    }

    #[test]
    fn test_function_is_null() -> Result<(), ApplicationError> {
        let path = TEST_PATH.to_owned() + "_test_function_is_null";
        let path = path.as_str();

        run_test(path, || {
            let result = filter(
                r#"select
                            is_null(commit_hash) as null_hash,
                            is_null(null)
                        where
                            is_null(null)
                        limit 1
                        "#,
                TEST_URL,
                path,
            )?;

            assert_eq!(
                result.headers,
                vec!["null_hash".to_owned(), "is_null(null)".to_owned(),]
            );
            assert_eq!(
                result.data,
                vec![vec![Value::Boolean(false), Value::Boolean(true)],]
            );

            Ok(())
        })
    }

    #[test]
    fn test_function_to_string() -> Result<(), ApplicationError> {
        let path = TEST_PATH.to_owned() + "_test_function_to_string";
        let path = path.as_str();

        run_test(path, || {
            let result = filter(
                r#"select
                            to_string([added_files, modified_files]) as set,
                        where
                            to_string([added_files, modified_files]) = "[7, 14]"
                        limit 1
                        "#,
                TEST_URL,
                path,
            )?;

            assert_eq!(result.headers, vec!["set".to_owned()]);
            assert_eq!(
                result.data,
                vec![vec![Value::String("[7, 14]".to_owned())],]
            );

            Ok(())
        })
    }

    #[test]
    fn test_function_type_of() -> Result<(), ApplicationError> {
        let path = TEST_PATH.to_owned() + "_test_function_type_of";
        let path = path.as_str();

        run_test(path, || {
            let result = filter(
                r#"select
                            type_of(added_files) as added_files_type,
                            type_of(commit_date) as commit_date_type,
                            type_of(commit_hash) as commit_hash_type,
                        where
                            type_of(added_files) = "Integer" and
                            commit_hash = '79b93f902b2652e0df10ac979a8466661e55f361'
                        limit 1
                        "#,
                TEST_URL,
                path,
            )?;

            assert_eq!(
                result.headers,
                vec![
                    "added_files_type".to_owned(),
                    "commit_date_type".to_owned(),
                    "commit_hash_type".to_owned()
                ]
            );
            assert_eq!(
                result.data,
                vec![vec![
                    Value::String("Integer".to_owned()),
                    Value::String("DateTime".to_owned()),
                    Value::String("String".to_owned())
                ],]
            );

            Ok(())
        })
    }

    #[test]
    fn test_function_nvl() -> Result<(), ApplicationError> {
        let path = TEST_PATH.to_owned() + "_test_function_nvl";
        let path = path.as_str();

        run_test(path, || {
            let result = filter(
                r#"select
                            nvl(null, "NULL VALUE") as nvl_desc,
                            nvl(commit_hash, "NULL COMMIT HASH") as commit_desc,
                        where
                            commit_hash = '79b93f902b2652e0df10ac979a8466661e55f361' and
                            nvl(author, "(unknown)") ~ 'Nick'
                        limit 1
                        "#,
                TEST_URL,
                path,
            )?;

            assert_eq!(
                result.headers,
                vec!["nvl_desc".to_owned(), "commit_desc".to_owned()]
            );
            assert_eq!(
                result.data,
                vec![vec![
                    Value::String("NULL VALUE".to_owned()),
                    Value::String("79b93f902b2652e0df10ac979a8466661e55f361".to_owned())
                ],]
            );

            Ok(())
        })
    }

    #[test]
    fn test_function_case() -> Result<(), ApplicationError> {
        let path = TEST_PATH.to_owned() + "_test_function_case";
        let path = path.as_str();

        run_test(path, || {
            let result = filter(
                r#"select
                            case(when: author_name ~ "Nick", then: "nick, nick", else: author_name) as greetings,
                        where
                            author_name ~ ['Nick', 'Alex Burka']
                        group by
                            author_name
                        order by
                            author_name
                        limit 2
                        "#,
                TEST_URL,
                path,
            )?;

            assert_eq!(result.headers, vec!["greetings".to_owned()]);
            assert_eq!(
                result.data,
                vec![
                    vec![Value::String("Alex Burka".to_owned())],
                    vec![Value::String("nick, nick".to_owned())]
                ]
            );

            Ok(())
        })
    }

    #[test]
    fn test_function_count_without_group() -> Result<(), ApplicationError> {
        let path = TEST_PATH.to_owned() + "_test_function_count_without_group";
        let path = path.as_str();

        run_test(path, || {
            let result = filter(
                r#"select
                            count()
                        where
                            is_merge_commit
                        limit 1
                        "#,
                TEST_URL,
                path,
            )?;

            assert_eq!(result.headers, vec!["count()".to_owned()]);
            assert_eq!(result.data, vec![vec![Value::Integer(647)],]);

            Ok(())
        })
    }

    #[test]
    fn test_function_count_with_group() -> Result<(), ApplicationError> {
        let path = TEST_PATH.to_owned() + "_test_function_count_with_group";
        let path = path.as_str();

        run_test(path, || {
            let result = filter(
                r#"select
                            author, count()
                        where
                            author_name ~ ['Nick', 'Alex Burka']
                        group by
                            author_email
                        order by
                            author_email
                        "#,
                TEST_URL,
                path,
            )?;

            assert_eq!(
                result.headers,
                vec!["author".to_owned(), "count()".to_owned()]
            );
            assert_eq!(
                result.data,
                vec![
                    vec![
                        Value::String("Alex Burka <aburka@seas.upenn.edu>".to_owned()),
                        Value::Integer(7)
                    ],
                    vec![
                        Value::String("Alex Burka <durka42+github@gmail.com>".to_owned()),
                        Value::Integer(3)
                    ],
                    vec![
                        Value::String("Nick Cameron <ncameron@mozilla.com>".to_owned()),
                        Value::Integer(11)
                    ],
                    vec![
                        Value::String("Nick Cameron <nrc@ncameron.org>".to_owned()),
                        Value::Integer(78)
                    ],
                ]
            );

            Ok(())
        })
    }

    #[test]
    fn test_function_count_with_group_and_having() -> Result<(), ApplicationError> {
        let path = TEST_PATH.to_owned() + "_test_function_count_with_group_and_having";
        let path = path.as_str();

        run_test(path, || {
            let result = filter(
                r#"select
                            author, count()
                        where
                            author_name ~ ['Nick', 'Alex Burka']
                        group by
                            author_email
                        having count() > 10
                        order by
                            author_email
                        "#,
                TEST_URL,
                path,
            )?;

            assert_eq!(
                result.headers,
                vec!["author".to_owned(), "count()".to_owned()]
            );
            assert_eq!(
                result.data,
                vec![
                    vec![
                        Value::String("Nick Cameron <ncameron@mozilla.com>".to_owned()),
                        Value::Integer(11)
                    ],
                    vec![
                        Value::String("Nick Cameron <nrc@ncameron.org>".to_owned()),
                        Value::Integer(78)
                    ],
                ]
            );

            Ok(())
        })
    }

    #[test]
    fn test_function_count_with_having_and_no_group() -> Result<(), ApplicationError> {
        let path = TEST_PATH.to_owned() + "_test_function_count_with_having_and_no_group";
        let path = path.as_str();

        run_test(path, || {
            let result = filter(
                r#"select count()
                        where
                            author_name ~ ['Nick', 'Alex Burka']
                        having count() = 100
                        limit 1
                        "#,
                TEST_URL,
                path,
            )?;

            assert_eq!(result.headers, vec!["count()".to_owned()]);
            assert_eq!(result.data, Vec::<Vec<Value>>::new());

            Ok(())
        })
    }

    #[test]
    fn test_function_max() -> Result<(), ApplicationError> {
        let path = TEST_PATH.to_owned() + "_test_function_max";
        let path = path.as_str();

        run_test(path, || {
            let result = filter(
                r#"select author_name, max(max(added_files), 1)
                        where
                            author_name ~ max(['Nick', 'Alex Burka']) and
                            max(10, 9, 8) = 10 and
                            max(commit_date, '2019-01-01') = commit_date
                        group by
                            author_name
                        "#,
                TEST_URL,
                path,
            )?;

            assert_eq!(
                result.headers,
                vec![
                    "author_name".to_owned(),
                    "max(max(added_files), 1)".to_owned()
                ]
            );
            assert_eq!(
                result.data,
                vec![vec![
                    Value::String("Nick Cameron".to_owned()),
                    Value::Integer(2)
                ]]
            );

            Ok(())
        })
    }

    #[test]
    fn test_function_min() -> Result<(), ApplicationError> {
        let path = TEST_PATH.to_owned() + "_test_function_min";
        let path = path.as_str();

        run_test(path, || {
            let result = filter(
                r#"select author_name, min(min(added_files), 1)
                        where
                            author_name ~ min(['Nick', 'Alex Burka']) and
                            min(10, 9, 8) = 8 and
                            min(commit_date, '2020-01-01') = commit_date
                        group by
                            author_name
                        "#,
                TEST_URL,
                path,
            )?;

            assert_eq!(
                result.headers,
                vec![
                    "author_name".to_owned(),
                    "min(min(added_files), 1)".to_owned()
                ]
            );
            assert_eq!(
                result.data,
                vec![vec![
                    Value::String("Alex Burka".to_owned()),
                    Value::Integer(0)
                ]]
            );

            Ok(())
        })
    }

    #[test]
    fn test_function_avg() -> Result<(), ApplicationError> {
        let path = TEST_PATH.to_owned() + "_test_function_avg";
        let path = path.as_str();

        run_test(path, || {
            let result = filter(
                r#"select author_name, round(avg(modified_files))
                        where
                            author_name ~ ['Nick', 'Alex Burka'] and
                            avg(10, 9, 8) = 9 and
                            avg([10, 9, 8]) = 9
                        group by
                            author_name
                        order by
                            author_name
                        "#,
                TEST_URL,
                path,
            )?;

            assert_eq!(
                result.headers,
                vec![
                    "author_name".to_owned(),
                    "round(avg(modified_files))".to_owned()
                ]
            );
            assert_eq!(
                result.data,
                vec![
                    vec![Value::String("Alex Burka".to_owned()), Value::Float(1f64)],
                    vec![Value::String("Nick Cameron".to_owned()), Value::Float(5f64)]
                ]
            );

            Ok(())
        })
    }

    #[test]
    fn test_function_sum() -> Result<(), ApplicationError> {
        let path = TEST_PATH.to_owned() + "_test_function_sum";
        let path = path.as_str();

        run_test(path, || {
            let result = filter(
                r#"select author_name, sum(modified_files)
                        where
                            author_name ~ ['Nick', 'Alex Burka'] and
                            sum(10, 9, 8) = 27 and
                            sum([10, 9, 8]) = 27
                        group by
                            author_name
                        order by
                            author_name
                        "#,
                TEST_URL,
                path,
            )?;

            assert_eq!(
                result.headers,
                vec!["author_name".to_owned(), "sum(modified_files)".to_owned()]
            );
            assert_eq!(
                result.data,
                vec![
                    vec![Value::String("Alex Burka".to_owned()), Value::Integer(10)],
                    vec![
                        Value::String("Nick Cameron".to_owned()),
                        Value::Integer(477)
                    ]
                ]
            );

            Ok(())
        })
    }

    #[test]
    fn test_count_added_files() -> Result<(), ApplicationError> {
        let path = TEST_PATH.to_owned() + "_test_count_added_files";
        let path = path.as_str();

        run_test(path, || {
            let result = filter(
                r#"select distinct count()
                        where author_name = 'Diggory Blake' and
                            added_files > 10
                "#,
                TEST_URL,
                path,
            )?;

            assert_eq!(result.headers, vec!["count()".to_owned()]);
            assert_eq!(result.data, vec![vec![Value::Integer(2)],]);

            Ok(())
        })
    }

    #[test]
    fn test_function_count_unique() -> Result<(), ApplicationError> {
        let path = TEST_PATH.to_owned() + "_test_function_count_unique";
        let path = path.as_str();

        run_test(path, || {
            let result = filter(
                r#"select distinct count(unique: author_name)
                        where lower(author_name) ~ 'dig'
                "#,
                TEST_URL,
                path,
            )?;

            assert_eq!(
                result.headers,
                vec!["count(unique: author_name)".to_owned()]
            );
            assert_eq!(result.data, vec![vec![Value::Integer(2)],]);

            Ok(())
        })
    }
}
