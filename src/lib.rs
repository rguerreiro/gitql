use self::repository::{CredentialType, RequestCredentials};
use git2::Cred;
use serde::{Deserialize, Serialize};

pub const DEFAULT_HOST: &str = "0.0.0.0";
pub const DEFAULT_PORT: &str = "44875";

#[derive(Debug, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub struct Request {
    pub url: String,
    pub path: String,
    pub credentials: RequestCredentials,
    pub query: String,
}

impl Request {
    pub fn credential_callback(
        &self,
        url: &str,
        username: Option<&str>,
        credential_type: CredentialType,
    ) -> repository::Result<Cred> {
        self.credentials
            .credential_callback(url, username, credential_type)
    }
}

pub mod application;
pub mod ast;
pub mod execution;
pub mod functions;
pub mod gql;
mod merge_sort;
pub mod repository;
