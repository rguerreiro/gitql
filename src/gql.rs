#![allow(clippy::range_plus_one)]

use std::mem::replace;
use std::num::{ParseFloatError, ParseIntError};
use std::ops::Range;

#[derive(PartialEq, Debug, Clone)]
pub struct LexerError {
    value: String,
    range: Range<usize>,
    message: String,
}

impl LexerError {
    fn new(value: String, range: Range<usize>, message: String) -> Self {
        Self {
            value,
            range,
            message,
        }
    }

    pub fn get_value(&self) -> &String {
        &self.value
    }

    pub fn get_range(&self) -> &Range<usize> {
        &self.range
    }

    pub fn get_message(&self) -> &String {
        &self.message
    }
}

#[derive(PartialEq, Debug, Copy, Clone)]
pub enum Number {
    Float(f64),
    Integer(u64),
}

#[derive(PartialEq, Debug, Clone)]
pub enum TokenType {
    Unknown(LexerError),
    Directive(String),
    ParenthesisLeft,
    ParenthesisRight,
    SquareBraceLeft,
    SquareBraceRight,
    CurlyBraceLeft,
    CurlyBraceRight,
    RegularExpression(String),
    String(String),
    InterpolatedStringOpen,
    InterpolatedStringClose,
    StringExpressionOpen,
    StringExpressionClose,
    Number(Number),
    Null,
    Select,
    As,
    Where,
    Distinct,
    Limit,
    Offset,
    Or,
    And,
    OrderBy,
    GroupBy,
    Having,
    Identifier(String),
    Asc,
    Desc,
    True,
    False,

    Range,
    Colon,
    Plus,
    Minus,
    Asterisk,
    Slash,
    Percent,
    Ampersand,
    Pipe,
    Caret,
    BitShiftLeft,
    BitShiftRight,
    Comma,
    Semicolon,

    Exclamation,
    Tilde,

    Equal,
    NotEqual,
    NotContains,
    Greater,
    GreaterOrEqual,
    Lower,
    LowerOrEqual,
}

impl From<&str> for TokenType {
    fn from(keyword: &str) -> Self {
        match keyword.to_lowercase().as_str() {
            "null" => TokenType::Null,
            "select" => TokenType::Select,
            "as" => TokenType::As,
            "where" => TokenType::Where,
            "distinct" => TokenType::Distinct,
            "limit" => TokenType::Limit,
            "offset" => TokenType::Offset,
            "or" => TokenType::Or,
            "and" => TokenType::And,
            "order by" => TokenType::OrderBy,
            "group by" => TokenType::GroupBy,
            "having" => TokenType::Having,
            "asc" => TokenType::Asc,
            "desc" => TokenType::Desc,
            "true" => TokenType::True,
            "false" => TokenType::False,
            _ => TokenType::Identifier(keyword.to_owned()),
        }
    }
}

#[derive(PartialEq, Debug, Clone)]
pub struct Token {
    category: TokenType,
    range: Range<usize>,
}

impl Token {
    pub fn get_category(&self) -> &TokenType {
        &self.category
    }

    pub fn get_range(&self) -> &Range<usize> {
        &self.range
    }
}

impl From<Token> for Result<Token, LexerError> {
    fn from(token: Token) -> Self {
        match token.category {
            TokenType::Unknown(ref e) => Err(e.clone()),
            _ => Ok(token),
        }
    }
}

impl<'a> From<&'a Token> for Result<&'a Token, &'a LexerError> {
    fn from(token: &'a Token) -> Self {
        match token.category {
            TokenType::Unknown(ref e) => Err(e),
            _ => Ok(token),
        }
    }
}

impl<'a> From<&'a mut Token> for Result<&'a mut Token, &'a mut LexerError> {
    fn from(token: &'a mut Token) -> Self {
        match token.category {
            TokenType::Unknown(ref mut e) => Err(e),
            _ => Ok(token),
        }
    }
}

#[derive(PartialEq, Debug, Clone)]
struct TokenBuilder {
    value: String,
    start_index: usize,
    radix: Option<u8>,
    is_float: Option<bool>,
}

impl TokenBuilder {
    fn new(start_index: usize) -> Self {
        Self {
            value: "".to_string(),
            start_index,
            radix: None,
            is_float: None,
        }
    }

    fn is_dirty(&self) -> bool {
        !self.value.is_empty()
    }

    fn build_error_token(&self, message: String, end_index: usize) -> Token {
        self.build_error_token_with_range(message, self.start_index..end_index)
    }

    fn build_error_token_with_range(&self, message: String, range: Range<usize>) -> Token {
        self.build_with_range(Err((None, message)), range)
    }

    fn build_error_token_with_range_and_value(
        &self,
        value: String,
        message: String,
        range: Range<usize>,
    ) -> Token {
        self.build_with_range(Err((Some(value), message)), range)
    }

    fn build(
        &self,
        category: Result<TokenType, (Option<String>, String)>,
        end_index: usize,
    ) -> Token {
        self.build_with_range(category, self.start_index..end_index)
    }

    fn build_with_range(
        &self,
        category: Result<TokenType, (Option<String>, String)>,
        range: Range<usize>,
    ) -> Token {
        match category {
            Ok(category) => Token {
                category,
                range: range.clone(),
            },
            Err(tuple) => Token {
                category: TokenType::Unknown(LexerError::new(
                    tuple.0.unwrap_or_else(|| self.value.clone()),
                    range.clone(),
                    tuple.1,
                )),
                range: range.clone(),
            },
        }
    }

    fn push(&mut self, character: char) {
        self.value.push(character);
    }

    fn push_str(&mut self, string: &str) {
        self.value.push_str(string);
    }

    fn is_number(&self) -> bool {
        self.radix.is_some() && self.is_float.is_some()
    }

    fn is_float(&self) -> bool {
        self.is_float.unwrap_or(false)
    }

    fn set_as_float(&mut self) {
        self.is_float = Some(true);
        self.radix = Some(10);
    }

    fn set_as_number(&mut self, radix: u8) -> (u8, bool) {
        self.is_float = self.is_float.or_else(|| Some(false));
        self.radix = self.radix.or_else(|| Some(radix));

        (self.radix.unwrap(), self.is_float.unwrap())
    }

    fn set_as_binary(&mut self) -> u8 {
        self.set_as_number(2).0
    }

    fn set_as_octal(&mut self) -> u8 {
        self.set_as_number(8).0
    }

    fn set_as_decimal(&mut self) -> u8 {
        self.set_as_number(10).0
    }

    fn set_as_hexadecimal(&mut self) -> u8 {
        self.set_as_number(16).0
    }

    fn build_number_token(&self, end_index: usize) -> Token {
        if !self.is_number() {
            return self.build_error_token(
                "Unable to build a number token because the builder is not set as a number."
                    .to_string(),
                end_index,
            );
        }

        let radix = self.radix.unwrap();
        let mut value = self.value.clone();

        if radix != 10 {
            if value.starts_with('-') {
                value = value[3..].to_string();
                value = "-".to_string() + value.as_str();
            } else if value.starts_with('+') {
                value = value[3..].to_string();
            } else {
                value = value[2..].to_string();
            };
        }

        value = value.replace("_", "");

        if self.is_float() {
            let parsed: Result<f64, (Option<String>, String)> = value
                .as_str()
                .parse()
                .map_err(|e: ParseFloatError| (None, e.to_string()));
            let number = parsed.map(Number::Float);
            self.build(number.map(TokenType::Number), end_index)
        } else {
            let parsed: Result<u64, (Option<String>, String)> =
                u64::from_str_radix(value.as_str(), radix.into())
                    .map_err(|e: ParseIntError| (None, e.to_string()));
            let number = parsed.map(Number::Integer);
            self.build(number.map(TokenType::Number), end_index)
        }
    }

    fn build_identifier_token(&self, end_index: usize) -> Token {
        let category = TokenType::from(self.value.as_str());
        self.build(Ok(category), end_index)
    }

    fn build_directive_token(&self, end_index: usize) -> Token {
        let category = TokenType::Directive(self.value.clone());
        self.build(Ok(category), end_index)
    }

    fn build_string_token(&self, end_index: usize) -> Token {
        let category = TokenType::String(self.value.clone());
        self.build(Ok(category), end_index)
    }
}

#[derive(PartialEq, Debug, Copy, Clone)]
enum LexerState {
    Start,
    Parenthesis,
    Identifier,
    RequiresBy,
    Number,
    NumberStartsWithZero,
    NumberExponential,
    RegularExpression,
    RegularExpressionEscape,
    String(char),
    StringEscaping,
    StringInterpolating,
    StringEscapingUnicode,
    Exclamation,
    AngleBracketLeft,
    AngleBracketRight,
    Directive,
    Dot,
}

#[derive(PartialEq, Debug, Clone)]
struct Lexer {
    chars: Vec<char>,
    index: usize,
    tokens: Vec<Token>,
    states: Vec<LexerState>,
    builder: TokenBuilder,
    string_count: usize,
    interpolating_string_count: usize,
}

impl Lexer {
    fn new(mut gql: String) -> Self {
        gql.push(' ');
        Self {
            chars: gql.chars().collect(),
            index: 0,
            tokens: Vec::new(),
            states: vec![LexerState::Start],
            builder: TokenBuilder::new(0),
            string_count: 0,
            interpolating_string_count: 0,
        }
    }

    fn len(&self) -> usize {
        self.chars.len()
    }

    fn get_char(&self) -> char {
        self.unsafe_char_at(self.index)
    }

    fn next_char(&self) -> Option<char> {
        self.char_at(self.index + 1)
    }

    fn previous_char(&self) -> Option<char> {
        if self.index == 0 {
            None
        } else {
            self.char_at(self.index - 1)
        }
    }

    fn unsafe_char_at(&self, index: usize) -> char {
        self.chars[index]
    }

    fn char_at(&self, index: usize) -> Option<char> {
        if index >= self.len() {
            None
        } else {
            Some(self.unsafe_char_at(index))
        }
    }

    fn parse(&mut self) {
        loop {
            let state = self.states.last();

            if let Some(state) = state {
                match state {
                    LexerState::StringInterpolating | LexerState::Start => self.lexer_state_start(),
                    LexerState::Parenthesis => self.lexer_state_start(),
                    LexerState::Identifier => self.lexer_state_identifier(),
                    LexerState::RequiresBy => self.lexer_state_requires_by(),
                    LexerState::Number => self.lexer_state_number(),
                    LexerState::NumberStartsWithZero => self.lexer_state_number_starts_with_zero(),
                    LexerState::NumberExponential => self.lexer_state_number_exponential(),
                    LexerState::RegularExpression => self.lexer_state_regular_expression(),
                    LexerState::RegularExpressionEscape => {
                        self.lexer_state_regular_expression_escape()
                    }
                    LexerState::String(boundary) => self.lexer_state_string(*boundary),
                    LexerState::StringEscaping => self.lexer_state_string_escaping(),
                    LexerState::StringEscapingUnicode => self.lexer_state_string_escaping_unicode(),
                    LexerState::Exclamation => self.lexer_state_exclamation(),
                    LexerState::AngleBracketLeft => self.lexer_state_angle_bracket(true),
                    LexerState::AngleBracketRight => self.lexer_state_angle_bracket(false),
                    LexerState::Directive => self.lexer_state_directive(),
                    LexerState::Dot => self.lexer_state_dot(),
                }
            } else {
                dbg!("No state in the Lexer.");
                break;
            }

            self.index += 1;
            if self.index >= self.len() {
                break;
            }
        }

        if self.builder.is_dirty() {
            let error_token = self
                .builder
                .build_error_token("Last token is unknown".to_string(), self.index);
            self.token_end(error_token);
        }
    }

    fn lexer_state_start(&mut self) {
        let current_character = self.get_char();

        if current_character.is_whitespace() {
            return;
        }

        let mut found = self.lexer_state_start_other_states(current_character);
        found = found || self.lexer_state_start_symbols(current_character);

        if !found {
            let error_token = self.builder.build_error_token_with_range(
                "Unknown token.".to_string(),
                self.index..self.index + 1,
            );
            self.token_end(error_token);
        }
    }

    fn lexer_state_start_other_states(&mut self, current_character: char) -> bool {
        if current_character == '(' {
            self.push_single_character_token(current_character, TokenType::ParenthesisLeft);
            self.enter_state(LexerState::Parenthesis);
        } else if current_character == ')' {
            if self.states.last() == Some(&LexerState::StringInterpolating) {
                self.token_end(self.builder.build_with_range(
                    Ok(TokenType::StringExpressionClose),
                    self.index - 1..self.index,
                ));
            } else {
                self.push_single_character_token(current_character, TokenType::ParenthesisRight);
            }

            self.leave_state();
        } else if current_character == '"' || current_character == '\'' {
            self.builder.start_index += 1;
            self.string_count += 1;
            self.enter_state(LexerState::String(current_character));
        } else if current_character.is_alphabetic() {
            self.builder.push(current_character);
            self.enter_state(LexerState::Identifier);
        } else if current_character == '0' {
            self.builder.push(current_character);
            self.enter_state(LexerState::NumberStartsWithZero);
        } else if current_character.is_digit(10) {
            self.builder.push(current_character);
            self.enter_state(LexerState::Number);
        } else if current_character == '`' {
            self.enter_state(LexerState::RegularExpression);
        } else if current_character == '!' {
            self.builder.push(current_character);
            self.enter_state(LexerState::Exclamation);
        } else if current_character == '<' {
            self.builder.push(current_character);
            self.enter_state(LexerState::AngleBracketLeft);
        } else if current_character == '>' {
            self.builder.push(current_character);
            self.enter_state(LexerState::AngleBracketRight);
        } else if current_character == '#' {
            self.enter_state(LexerState::Directive);
        } else if current_character == '.' {
            self.builder.push(current_character);
            self.enter_state(LexerState::Dot);
        } else {
            return false;
        }

        true
    }

    fn lexer_state_start_symbols(&mut self, current_character: char) -> bool {
        match current_character {
            '+' => self.push_single_character_token(current_character, TokenType::Plus),
            '-' => self.push_single_character_token(current_character, TokenType::Minus),
            '[' => self.push_single_character_token(current_character, TokenType::SquareBraceLeft),
            ']' => self.push_single_character_token(current_character, TokenType::SquareBraceRight),
            '{' => self.push_single_character_token(current_character, TokenType::CurlyBraceLeft),
            '}' => self.push_single_character_token(current_character, TokenType::CurlyBraceRight),
            ',' => self.push_single_character_token(current_character, TokenType::Comma),
            '=' => self.push_single_character_token(current_character, TokenType::Equal),
            '~' => self.push_single_character_token(current_character, TokenType::Tilde),
            '&' => self.push_single_character_token(current_character, TokenType::Ampersand),
            '|' => self.push_single_character_token(current_character, TokenType::Pipe),
            '^' => self.push_single_character_token(current_character, TokenType::Caret),
            '*' => self.push_single_character_token(current_character, TokenType::Asterisk),
            ';' => self.push_single_character_token(current_character, TokenType::Semicolon),
            '%' => self.push_single_character_token(current_character, TokenType::Percent),
            '/' => self.push_single_character_token(current_character, TokenType::Slash),
            ':' => self.push_single_character_token(current_character, TokenType::Colon),
            _ => return false,
        }

        true
    }

    fn enter_state(&mut self, state: LexerState) {
        self.states.push(state);
    }

    fn leave_state(&mut self) -> Option<LexerState> {
        self.states.pop()
    }

    fn switch_state(&mut self, state: LexerState) -> Option<LexerState> {
        let old = self.leave_state();
        self.enter_state(state);
        old
    }

    fn token_end_from_category(&mut self, category: TokenType) {
        self.token_end(self.builder.build(Ok(category), self.index + 1));
    }

    fn token_end(&mut self, token: Token) {
        self.tokens.push(token);
        self.builder = TokenBuilder::new(self.index);
    }

    fn push_single_character_token(&mut self, character: char, category: TokenType) {
        let mut atomic_builder = TokenBuilder::new(self.index);
        atomic_builder.push(character);

        self.tokens
            .push(atomic_builder.build(Ok(category), self.index + 1));

        self.builder = TokenBuilder::new(self.index);
    }

    fn re_read_current_char(&mut self) {
        self.index -= 1;
    }

    fn lexer_state_string(&mut self, boundary: char) {
        let current_character = self.get_char();

        if current_character == '\\' {
            self.enter_state(LexerState::StringEscaping);
        } else if current_character != boundary {
            self.builder.push(current_character);
        } else {
            self.token_end(self.builder.build_string_token(self.index));
            if self.interpolating_string_count > 0 {
                self.token_end(self.builder.build_with_range(
                    Ok(TokenType::InterpolatedStringClose),
                    self.index..self.index + 1,
                ));
                self.interpolating_string_count -= 1;
            }

            self.string_count -= 1;
            self.leave_state();
        }
    }

    fn lexer_state_string_escaping(&mut self) {
        let current_character = self.get_char();

        match current_character {
            '(' => {
                let should_open = self.interpolating_string_count < self.string_count;
                let string = self.builder.build_string_token(self.index - 2);

                if should_open {
                    let star_index = string.range.start;

                    self.interpolating_string_count += 1;
                    self.token_end(self.builder.build_with_range(
                        Ok(TokenType::InterpolatedStringOpen),
                        star_index - 1..star_index,
                    ));
                }
                self.token_end(string);
                self.token_end(self.builder.build_with_range(
                    Ok(TokenType::StringExpressionOpen),
                    self.index - 1..self.index + 1,
                ));

                self.switch_state(LexerState::StringInterpolating);
            }
            'n' => {
                self.builder.push('\n');
                self.leave_state();
            }
            't' => {
                self.builder.push('\t');
                self.leave_state();
            }
            'r' => {
                self.builder.push('\r');
                self.leave_state();
            }
            '"' => {
                self.builder.push('"');
                self.leave_state();
            }
            '\'' => {
                self.builder.push('\'');
                self.leave_state();
            }
            '`' => {
                self.builder.push('`');
                self.leave_state();
            }
            '\\' => {
                self.builder.push('\\');
                self.leave_state();
            }
            'u' => {
                self.switch_state(LexerState::StringEscapingUnicode);
            }
            _ => {
                let error_message =
                    r#"Invalid escape character. Use \n, \t, \r, \", \', \`, \\, \u{7FFF}"#;
                self.token_end(self.builder.build_error_token_with_range(
                    error_message.to_string(),
                    self.index - 1..self.index + 1,
                ));
                self.leave_state();
            }
        };
    }

    fn lexer_state_string_escaping_unicode(&mut self) {
        let start_index = self.index - 2;
        // 12 chars is the max length of the unicode character.
        let limit_index = start_index + 10;

        let mut opened = false;
        let mut unicode: String = String::with_capacity(6);

        loop {
            let current_character = self.get_char();

            if self.index < limit_index && !opened && current_character == '{' {
                opened = true;
            } else if self.index < limit_index && !unicode.is_empty() && current_character == '}' {
                match unicode_to_utf8(unicode.as_str()) {
                    Ok(ut8) => self.builder.push_str(&ut8),

                    Err(message) => self.token_end(
                        self.builder
                            .build_error_token_with_range(message, start_index..self.index),
                    ),
                };
                self.leave_state();
                return;
            } else if self.index < limit_index && opened && current_character.is_digit(16) {
                unicode.push(current_character);
            } else {
                let mut value = r"\u".to_string();
                value.push_str(unicode.as_str());

                let error_message =
                    r#"Invalid unicode representation. Use \u{7FFF}, where it expects between 1 and 6 hexadecimal characters to form the code."#;
                self.token_end(self.builder.build_error_token_with_range_and_value(
                    value,
                    error_message.to_string(),
                    start_index..self.index,
                ));
                self.leave_state();
                return;
            }

            self.index += 1;
        }
    }

    fn lexer_state_identifier(&mut self) {
        let current_character = self.get_char();
        let lowercase_value = self.builder.value.to_lowercase();

        if current_character.is_alphabetic() || current_character == '_' {
            self.builder.push(current_character);
        } else if current_character.is_whitespace()
            && (lowercase_value == "order" || lowercase_value == "group")
        {
            self.builder.push(current_character);
            self.enter_state(LexerState::RequiresBy);
        } else {
            let new_token = self.builder.build_identifier_token(self.index);

            self.token_end(new_token);
            self.leave_state();
            self.re_read_current_char();
        }
    }

    fn lexer_state_directive(&mut self) {
        let current_character = self.get_char();

        if current_character.is_alphanumeric() || current_character == '_' {
            self.builder.push(current_character);
        } else {
            let new_token = self.builder.build_directive_token(self.index);

            self.token_end(new_token);
            self.leave_state();
            self.re_read_current_char();
        }
    }

    fn lexer_state_dot(&mut self) {
        let current_character = self.get_char();

        let new_token = if current_character == '.' {
            self.builder.push(current_character);
            self.builder.build(Ok(TokenType::Range), self.index)
        } else {
            let end_index = self.index;

            self.re_read_current_char();
            self.builder.build_error_token(
                "Invalid character following a dot operator.".to_owned(),
                end_index,
            )
        };

        self.token_end(new_token);
        self.leave_state();
    }

    fn lexer_state_requires_by(&mut self) {
        let current_character = self.get_char();
        let next_character = self.next_char();

        if next_character.is_none()
            || current_character.to_lowercase().to_string() != "b"
            || next_character.unwrap().to_lowercase().to_string() != "y"
        {
            let error_token = self.builder.build_error_token(
                "Unknown token. Expected 'by' after `order` or `group`.".to_string(),
                self.index + 1,
            );
            self.token_end(error_token);
        } else {
            self.builder.push('b');
            self.builder.push('y');
            self.index += 1;
            self.leave_state();
        }
    }

    fn lexer_state_exclamation(&mut self) {
        let current_character = self.get_char();

        if current_character == '=' {
            self.builder.push(current_character);
            self.token_end_from_category(TokenType::NotEqual);
        } else if current_character == '~' {
            self.builder.push(current_character);
            self.token_end_from_category(TokenType::NotContains);
        } else {
            self.re_read_current_char();
            self.token_end_from_category(TokenType::Exclamation);
        }
        self.leave_state();
    }

    fn lexer_state_angle_bracket(&mut self, is_left: bool) {
        let current_character = self.get_char();

        if current_character == '=' {
            self.builder.push(current_character);
            let category = if is_left {
                TokenType::LowerOrEqual
            } else {
                TokenType::GreaterOrEqual
            };
            self.token_end_from_category(category);
        } else if is_left && current_character == '<' {
            self.builder.push(current_character);
            self.token_end_from_category(TokenType::BitShiftLeft);
        } else if !is_left && current_character == '>' {
            self.builder.push(current_character);
            self.token_end_from_category(TokenType::BitShiftRight);
        } else {
            let category = if is_left {
                TokenType::Lower
            } else {
                TokenType::Greater
            };
            self.re_read_current_char();
            self.token_end_from_category(category);
        }
        self.leave_state();
    }

    fn lexer_state_number_starts_with_zero(&mut self) {
        let current_character = self.get_char();
        self.switch_state(LexerState::Number);

        if current_character == 'x' {
            self.builder.set_as_hexadecimal();
            self.builder.push(current_character);
        } else if current_character == 'o' {
            self.builder.set_as_octal();
            self.builder.push(current_character);
        } else if current_character == 'b' {
            self.builder.set_as_binary();
            self.builder.push(current_character);
        } else if !self.builder.is_float() && current_character == '.' {
            self.builder.set_as_float();
            self.builder.push(current_character);
        } else if current_character == '_' || current_character.is_digit(10) {
            self.builder.push(current_character);
        } else {
            // Just the number zero alone.
            self.builder.set_as_decimal();

            self.number_token_end();
            self.re_read_current_char();
        }
    }

    fn lexer_state_number(&mut self) {
        let current_character = self.get_char();
        let radix = self.builder.set_as_decimal();

        if current_character == '_' {
            self.builder.push(current_character);
        } else if !self.builder.is_float() && current_character == '.' {
            // println!("Found a DOT!");

            let next_character = self.next_char();

            // println!("next_character: {:?}", next_character);

            if next_character.is_some()
                && (next_character.unwrap().is_digit(u32::from(radix))
                    || next_character.unwrap() == '_')
            {
                self.builder.set_as_float();
                self.builder.push('.');
                self.builder.push(next_character.unwrap());
                self.index += 1;
            } else {
                self.number_token_end();
                self.re_read_current_char();
            }
        } else if current_character.is_digit(u32::from(radix)) {
            self.builder.push(current_character);
        } else if current_character == 'e' || current_character == 'E' {
            self.builder.push(current_character);
            self.builder.set_as_float();
            self.switch_state(LexerState::NumberExponential);
        } else {
            self.number_token_end();
            self.re_read_current_char();
        }
    }

    fn lexer_state_number_exponential(&mut self) {
        let current_character = self.get_char();

        if current_character.is_digit(10) || current_character == '_' {
            self.builder.push(current_character);
        } else if current_character == '-' || current_character == '+' {
            let last_character = self.previous_char();
            if last_character.is_some()
                && (last_character.unwrap() == 'e' || last_character.unwrap() == 'E')
            {
                self.builder.push(current_character);
            } else {
                self.number_token_end();
                self.re_read_current_char();
            }
        } else {
            self.number_token_end();
            self.re_read_current_char();
        }
    }

    fn number_token_end(&mut self) {
        self.token_end(self.builder.build_number_token(self.index));
        self.leave_state();
    }

    fn lexer_state_regular_expression(&mut self) {
        let current_character = self.get_char();

        if current_character == '\\' {
            self.builder.push(current_character);
            self.enter_state(LexerState::RegularExpressionEscape);
        } else if current_character == '`' {
            self.token_end_from_category(TokenType::RegularExpression(self.builder.value.clone()));
            self.leave_state();
        } else {
            self.builder.push(current_character);
        }
    }

    fn lexer_state_regular_expression_escape(&mut self) {
        let current_character = self.get_char();
        self.builder.push(current_character);
        self.leave_state();
    }
}

pub fn parse(gql: &str) -> Vec<Token> {
    let mut lexer = Lexer::new(gql.to_owned());
    lexer.parse();
    replace(&mut lexer.tokens, Vec::new())
}

fn unicode_to_utf8(unicode: &str) -> Result<String, String> {
    if unicode.is_empty() || unicode.len() > 6 {
        return Err("Invalid unicode.".to_string());
    }

    let mut utf8: Vec<u8> = Vec::with_capacity(4);
    let unicode: u32 = i64::from_str_radix(unicode, 16).unwrap() as u32;

    if unicode <= 0x7F {
        // Plain ASCII
        utf8.push(unicode as u8);
    } else if unicode <= 0x07FF {
        // 2-byte unicode
        utf8.push((((unicode >> 6) & 0x1F) | 0xC0) as u8);
        utf8.push(((unicode & 0x3F) | 0x80) as u8);
    } else if unicode <= 0xFFFF {
        // 3-byte unicode
        utf8.push((((unicode >> 12) & 0x0F) | 0xE0) as u8);
        utf8.push((((unicode >> 6) & 0x3F) | 0x80) as u8);
        utf8.push(((unicode & 0x3F) | 0x80) as u8);
    } else if unicode <= 0x10_FFFF {
        // 4-byte unicode
        utf8.push((((unicode >> 18) & 0x07) | 0xF0) as u8);
        utf8.push((((unicode >> 12) & 0x3F) | 0x80) as u8);
        utf8.push((((unicode >> 6) & 0x3F) | 0x80) as u8);
        utf8.push(((unicode & 0x3F) | 0x80) as u8);
    } else {
        return Err("Invalid unicode.".to_string());
    }

    String::from_utf8(utf8).map_err(|e| e.to_string())
}

#[cfg(test)]
mod tests {
    use super::*;

    fn assert_single_token(gql: &str, expected: TokenType) {
        assert_tokens(
            gql,
            vec![Token {
                category: expected,
                range: 0..gql.len(),
            }],
        );
    }

    fn assert_tokens_type(gql: &str, expected: Vec<TokenType>) {
        let tokens: Vec<TokenType> = parse(gql).into_iter().map(|t| t.category).collect();

        assert_eq!(
            expected.len(),
            tokens.len(),
            "Assertion and parsed length does not match.\n\nexpected: {:?}\n\nactual: {:?}\n\n",
            expected,
            tokens
        );

        for (index, token_type) in expected.iter().enumerate() {
            assert_eq!(
                *token_type, tokens[index],
                "TokenType does not match for index {}, expected {:?} and got {:?}.\n\nexpected tokens: {:?}\n\nactual tokens: {:?}\n\n",
                index, token_type, tokens[index],
                expected, tokens
            );
        }
    }

    fn assert_tokens(gql: &str, expected: Vec<Token>) {
        let tokens = parse(gql);

        assert_eq!(
            tokens.len(),
            expected.len(),
            "Assertion and parsed length does not match.\n\nexpected: {:?}\n\nactual: {:?}\n\n",
            expected,
            tokens
        );

        for (index, token) in expected.iter().enumerate() {
            assert_eq!(
                tokens[index], *token,
                "Token does not match for index {}, expected {:?} and got {:?}.",
                index, token, tokens[index]
            );
        }
    }

    #[test]
    fn test_integer_number_parsing() {
        assert_single_token("1000", TokenType::Number(Number::Integer(1000)));
        assert_tokens_type(
            "-1000",
            vec![TokenType::Minus, TokenType::Number(Number::Integer(1000))],
        );
        assert_tokens_type(
            "+1000",
            vec![TokenType::Plus, TokenType::Number(Number::Integer(1000))],
        );
        assert_single_token("1_000", TokenType::Number(Number::Integer(1000)));
        assert_tokens_type(
            "-1_000",
            vec![TokenType::Minus, TokenType::Number(Number::Integer(1000))],
        );
        assert_tokens_type(
            "+1_000",
            vec![TokenType::Plus, TokenType::Number(Number::Integer(1000))],
        );

        assert_single_token("0b0101", TokenType::Number(Number::Integer(0b0101)));
        assert_tokens_type(
            "-0b0101",
            vec![TokenType::Minus, TokenType::Number(Number::Integer(0b0101))],
        );
        assert_tokens_type(
            "+0b0101",
            vec![TokenType::Plus, TokenType::Number(Number::Integer(0b0101))],
        );
        assert_single_token("0b0_101", TokenType::Number(Number::Integer(0b0101)));
        assert_tokens_type(
            "-0b0_101",
            vec![TokenType::Minus, TokenType::Number(Number::Integer(0b0101))],
        );
        assert_tokens_type(
            "+0b0_101",
            vec![TokenType::Plus, TokenType::Number(Number::Integer(0b0101))],
        );

        assert_single_token("0o14567", TokenType::Number(Number::Integer(0o14567)));
        assert_tokens_type(
            "-0o14567",
            vec![
                TokenType::Minus,
                TokenType::Number(Number::Integer(0o14567)),
            ],
        );
        assert_tokens_type(
            "+0o14567",
            vec![TokenType::Plus, TokenType::Number(Number::Integer(0o14567))],
        );
        assert_single_token("0o14_567", TokenType::Number(Number::Integer(0o14567)));
        assert_tokens_type(
            "-0o14_567",
            vec![
                TokenType::Minus,
                TokenType::Number(Number::Integer(0o14567)),
            ],
        );
        assert_tokens_type(
            "+0o14_567",
            vec![TokenType::Plus, TokenType::Number(Number::Integer(0o14567))],
        );

        assert_single_token("0xaff45", TokenType::Number(Number::Integer(0xaff45)));
        assert_tokens_type(
            "-0xaff45",
            vec![
                TokenType::Minus,
                TokenType::Number(Number::Integer(0xaff45)),
            ],
        );
        assert_tokens_type(
            "+0xaff45",
            vec![TokenType::Plus, TokenType::Number(Number::Integer(0xaff45))],
        );
        assert_single_token("0xaf_f45", TokenType::Number(Number::Integer(0xaff45)));
        assert_tokens_type(
            "-0xaf_f45",
            vec![
                TokenType::Minus,
                TokenType::Number(Number::Integer(0xaff45)),
            ],
        );
        assert_tokens_type(
            "+0xaf_f45",
            vec![TokenType::Plus, TokenType::Number(Number::Integer(0xaff45))],
        );
    }

    #[test]
    fn test_float_number_parsing() {
        assert_single_token("10.0", TokenType::Number(Number::Float(10.0)));
        assert_tokens_type(
            "-10.0",
            vec![TokenType::Minus, TokenType::Number(Number::Float(10.0))],
        );
        assert_tokens_type(
            "+10.0",
            vec![TokenType::Plus, TokenType::Number(Number::Float(10.0))],
        );
        assert_single_token("1_000.0", TokenType::Number(Number::Float(1_000.0)));
        assert_tokens_type(
            "-1_000.0",
            vec![TokenType::Minus, TokenType::Number(Number::Float(1_000.0))],
        );
        assert_tokens_type(
            "+1_000.0",
            vec![TokenType::Plus, TokenType::Number(Number::Float(1_000.0))],
        );

        assert_single_token("2e12", TokenType::Number(Number::Float(2e12)));
        assert_tokens_type(
            "-2e12",
            vec![TokenType::Minus, TokenType::Number(Number::Float(2e12))],
        );
        assert_tokens_type(
            "+2e12",
            vec![TokenType::Plus, TokenType::Number(Number::Float(2e12))],
        );
        assert_single_token("2_000e1_2", TokenType::Number(Number::Float(2000e12)));
        assert_tokens_type(
            "-2_000e1_2",
            vec![TokenType::Minus, TokenType::Number(Number::Float(2000e12))],
        );
        assert_tokens_type(
            "+2_000e1_2",
            vec![TokenType::Plus, TokenType::Number(Number::Float(2000e12))],
        );

        assert_single_token("2e-12", TokenType::Number(Number::Float(2e-12)));
        assert_tokens_type(
            "-2e-12",
            vec![TokenType::Minus, TokenType::Number(Number::Float(2e-12))],
        );
        assert_tokens_type(
            "+2e-12",
            vec![TokenType::Plus, TokenType::Number(Number::Float(2e-12))],
        );
        assert_single_token("2_000e-1_2", TokenType::Number(Number::Float(2000e-12)));
        assert_tokens_type(
            "-2_000e-1_2",
            vec![TokenType::Minus, TokenType::Number(Number::Float(2000e-12))],
        );
        assert_tokens_type(
            "+2_000e-1_2",
            vec![TokenType::Plus, TokenType::Number(Number::Float(2000e-12))],
        );

        assert_single_token("2e+12", TokenType::Number(Number::Float(2e12)));
        assert_tokens_type(
            "-2e+12",
            vec![TokenType::Minus, TokenType::Number(Number::Float(2e12))],
        );
        assert_tokens_type(
            "+2e+12",
            vec![TokenType::Plus, TokenType::Number(Number::Float(2e12))],
        );
        assert_single_token("2_000e+1_2", TokenType::Number(Number::Float(2000e12)));
        assert_tokens_type(
            "-2_000e+1_2",
            vec![TokenType::Minus, TokenType::Number(Number::Float(2000e12))],
        );
        assert_tokens_type(
            "+2_000e+1_2",
            vec![TokenType::Plus, TokenType::Number(Number::Float(2000e12))],
        );
    }

    #[test]
    fn test_identifier_parsing() {
        assert_single_token("or", TokenType::Or);
        assert_single_token("OR", TokenType::Or);

        assert_single_token("and", TokenType::And);
        assert_single_token("AND", TokenType::And);

        assert_single_token("order by", TokenType::OrderBy);
        assert_single_token("ORDER BY", TokenType::OrderBy);

        assert_single_token("asc", TokenType::Asc);
        assert_single_token("ASC", TokenType::Asc);

        assert_single_token("desc", TokenType::Desc);
        assert_single_token("DESC", TokenType::Desc);

        assert_single_token("author", TokenType::Identifier("author".to_owned()));
    }

    #[test]
    fn test_symbols_parsing() {
        assert_single_token("&", TokenType::Ampersand);
        assert_single_token("|", TokenType::Pipe);
        assert_single_token("<", TokenType::Lower);
        assert_single_token("<<", TokenType::BitShiftLeft);
        assert_single_token("<=", TokenType::LowerOrEqual);
        assert_single_token(">", TokenType::Greater);
        assert_single_token(">>", TokenType::BitShiftRight);
        assert_single_token(">=", TokenType::GreaterOrEqual);
        assert_single_token("=", TokenType::Equal);
        assert_single_token("!=", TokenType::NotEqual);
        assert_single_token("~", TokenType::Tilde);
        assert_single_token("!~", TokenType::NotContains);
        assert_single_token("!", TokenType::Exclamation);
        assert_single_token("^", TokenType::Caret);
        assert_single_token("+", TokenType::Plus);
        assert_single_token("-", TokenType::Minus);
        assert_single_token(",", TokenType::Comma);
        assert_single_token("*", TokenType::Asterisk);
        assert_single_token("/", TokenType::Slash);
        assert_single_token("%", TokenType::Percent);
        assert_single_token(";", TokenType::Semicolon);
    }

    #[test]
    fn test_raw_string_parsing() {
        let contents = vec!["", "abc", "''", r#""""#, " testing delimiters "];
        let delimiters = vec!['\'', '"'];

        for delimiter in delimiters.iter() {
            for content in contents.iter() {
                if content.contains(|part| part == *delimiter) {
                    continue;
                }

                {
                    let mut gql = String::new();
                    gql.push(*delimiter);
                    gql.push_str(content);
                    gql.push(*delimiter);

                    assert_tokens(
                        gql.as_str(),
                        vec![Token {
                            category: TokenType::String(content.to_string()),
                            range: 1..(gql.len() - 1),
                        }],
                    );
                }
            }
        }
    }

    #[test]
    fn test_unicode_to_utf8() {
        assert_eq!(unicode_to_utf8("24"), Ok("\u{24}".to_string()));
        assert_eq!(unicode_to_utf8("aeff"), Ok("\u{aeff}".to_string()));
        assert_eq!(unicode_to_utf8("9090"), Ok("\u{9090}".to_string()));
        assert_eq!(unicode_to_utf8("103456"), Ok("\u{103456}".to_string()));
        assert_eq!(unicode_to_utf8("000022"), Ok("\u{22}".to_string()));
        assert_eq!(unicode_to_utf8("3a9"), Ok("\u{3a9}".to_string()));
        assert_eq!(unicode_to_utf8("10ffff"), Ok("\u{10FFFF}".to_string()));
    }

    #[test]
    fn test_no_space() {
        assert_tokens_type(
            "4>=1",
            vec![
                TokenType::Number(Number::Integer(4)),
                TokenType::GreaterOrEqual,
                TokenType::Number(Number::Integer(1)),
            ],
        );
        assert_tokens_type(
            "4>1",
            vec![
                TokenType::Number(Number::Integer(4)),
                TokenType::Greater,
                TokenType::Number(Number::Integer(1)),
            ],
        );
        assert_tokens_type(
            "4=1",
            vec![
                TokenType::Number(Number::Integer(4)),
                TokenType::Equal,
                TokenType::Number(Number::Integer(1)),
            ],
        );
        assert_tokens_type(
            "4<1",
            vec![
                TokenType::Number(Number::Integer(4)),
                TokenType::Lower,
                TokenType::Number(Number::Integer(1)),
            ],
        );
        assert_tokens_type(
            "4!=1",
            vec![
                TokenType::Number(Number::Integer(4)),
                TokenType::NotEqual,
                TokenType::Number(Number::Integer(1)),
            ],
        );
        assert_tokens_type(
            "4&1",
            vec![
                TokenType::Number(Number::Integer(4)),
                TokenType::Ampersand,
                TokenType::Number(Number::Integer(1)),
            ],
        );
        assert_tokens_type(
            "4|1",
            vec![
                TokenType::Number(Number::Integer(4)),
                TokenType::Pipe,
                TokenType::Number(Number::Integer(1)),
            ],
        );
        assert_tokens_type(
            "4^1",
            vec![
                TokenType::Number(Number::Integer(4)),
                TokenType::Caret,
                TokenType::Number(Number::Integer(1)),
            ],
        );
        assert_tokens_type(
            "4,1",
            vec![
                TokenType::Number(Number::Integer(4)),
                TokenType::Comma,
                TokenType::Number(Number::Integer(1)),
            ],
        );
        assert_tokens_type(
            "4;1",
            vec![
                TokenType::Number(Number::Integer(4)),
                TokenType::Semicolon,
                TokenType::Number(Number::Integer(1)),
            ],
        );
        assert_tokens_type(
            "4+1",
            vec![
                TokenType::Number(Number::Integer(4)),
                TokenType::Plus,
                TokenType::Number(Number::Integer(1)),
            ],
        );
        assert_tokens_type(
            "4-1",
            vec![
                TokenType::Number(Number::Integer(4)),
                TokenType::Minus,
                TokenType::Number(Number::Integer(1)),
            ],
        );
        assert_tokens_type(
            "4/1",
            vec![
                TokenType::Number(Number::Integer(4)),
                TokenType::Slash,
                TokenType::Number(Number::Integer(1)),
            ],
        );
        assert_tokens_type(
            "4*1",
            vec![
                TokenType::Number(Number::Integer(4)),
                TokenType::Asterisk,
                TokenType::Number(Number::Integer(1)),
            ],
        );
        assert_tokens_type(
            "4%1",
            vec![
                TokenType::Number(Number::Integer(4)),
                TokenType::Percent,
                TokenType::Number(Number::Integer(1)),
            ],
        );
        assert_tokens_type(
            "4~1",
            vec![
                TokenType::Number(Number::Integer(4)),
                TokenType::Tilde,
                TokenType::Number(Number::Integer(1)),
            ],
        );
        assert_tokens_type(
            "4!~1",
            vec![
                TokenType::Number(Number::Integer(4)),
                TokenType::NotContains,
                TokenType::Number(Number::Integer(1)),
            ],
        );
    }

    #[test]
    fn test_escaped_string_parsing() {
        let contents = vec![
            r"'\n'",
            "\n",
            r"'\\'",
            "\\",
            r"'\''",
            "'",
            r"'\`'",
            "`",
            r#"'\"'"#,
            "\"",
            r"'\r'",
            "\r",
            r"'\t'",
            "\t",
            r"'\u{24}'",
            "\u{24}",
            r"'\u{A2}'",
            "\u{A2}",
            r"'\u{0939}'",
            "\u{0939}",
            r"'\u{20AC}'",
            "\u{20AC}",
            r"'\u{10348}'",
            "\u{10348}",
            r"'\u{10FFFF}'",
            "\u{10FFFF}",
            r"'\u{59}'",
            "Y",
            r"'\u{03A9}'",
            "\u{3A9}",
        ];

        let mut iterator = contents.iter();

        loop {
            if let Some(gql) = iterator.next() {
                let content = iterator.next().unwrap();

                assert_tokens(
                    gql,
                    vec![Token {
                        category: TokenType::String(content.to_string()),
                        range: 1..(gql.len() - 1),
                    }],
                );
            } else {
                break;
            }
        }
    }

    #[test]
    fn test_string_interpolation() {
        assert_tokens_type(
            r#""Branch: \(branch), author: \(author)""#,
            vec![
                TokenType::InterpolatedStringOpen,
                TokenType::String("Branch: ".to_owned()),
                TokenType::StringExpressionOpen,
                TokenType::Identifier("branch".to_owned()),
                TokenType::StringExpressionClose,
                TokenType::String(", author: ".to_owned()),
                TokenType::StringExpressionOpen,
                TokenType::Identifier("author".to_owned()),
                TokenType::StringExpressionClose,
                TokenType::String("".to_owned()),
                TokenType::InterpolatedStringClose,
            ],
        );
    }

    #[test]
    fn test_nested_string_interpolation() {
        assert_tokens_type(
            r#""Branch: \(branch + " concatenated \(repository + date)"), author: \(author)""#,
            vec![
                TokenType::InterpolatedStringOpen,
                TokenType::String("Branch: ".to_owned()),
                TokenType::StringExpressionOpen,
                TokenType::Identifier("branch".to_owned()),
                TokenType::Plus,
                TokenType::InterpolatedStringOpen,
                TokenType::String(" concatenated ".to_owned()),
                TokenType::StringExpressionOpen,
                TokenType::Identifier("repository".to_owned()),
                TokenType::Plus,
                TokenType::Identifier("date".to_owned()),
                TokenType::StringExpressionClose,
                TokenType::String("".to_owned()),
                TokenType::InterpolatedStringClose,
                TokenType::StringExpressionClose,
                TokenType::String(", author: ".to_owned()),
                TokenType::StringExpressionOpen,
                TokenType::Identifier("author".to_owned()),
                TokenType::StringExpressionClose,
                TokenType::String("".to_owned()),
                TokenType::InterpolatedStringClose,
            ],
        );
    }

    #[test]
    fn test_directives() {
        assert_tokens_type(
            r#"#thread8 #no_fetch where branch ~ "master";"#,
            vec![
                TokenType::Directive("thread8".to_owned()),
                TokenType::Directive("no_fetch".to_owned()),
                TokenType::Where,
                TokenType::Identifier("branch".to_owned()),
                TokenType::Tilde,
                TokenType::String("master".to_owned()),
                TokenType::Semicolon,
            ],
        );
    }

    #[test]
    fn test_valid_gql_1() {
        assert_tokens_type(
            r#"where (
                        author !~ "Rafael Guerreiro"
                        and date > days(10)
                    ) or (
                        file = `^.*?\.rs$`
                        and modified_files < 1_000
                    )
                    order by repository, commit asc, commit_message desc"#,
            vec![
                TokenType::Where,
                TokenType::ParenthesisLeft,
                TokenType::Identifier("author".to_owned()),
                TokenType::NotContains,
                TokenType::String("Rafael Guerreiro".to_string()),
                TokenType::And,
                TokenType::Identifier("date".to_owned()),
                TokenType::Greater,
                TokenType::Identifier("days".to_owned()),
                TokenType::ParenthesisLeft,
                TokenType::Number(Number::Integer(10)),
                TokenType::ParenthesisRight,
                TokenType::ParenthesisRight,
                TokenType::Or,
                TokenType::ParenthesisLeft,
                TokenType::Identifier("file".to_owned()),
                TokenType::Equal,
                TokenType::RegularExpression(r"^.*?\.rs$".to_string()),
                TokenType::And,
                TokenType::Identifier("modified_files".to_owned()),
                TokenType::Lower,
                TokenType::Number(Number::Integer(1000)),
                TokenType::ParenthesisRight,
                TokenType::OrderBy,
                TokenType::Identifier("repository".to_owned()),
                TokenType::Comma,
                TokenType::Identifier("commit".to_owned()),
                TokenType::Asc,
                TokenType::Comma,
                TokenType::Identifier("commit_message".to_owned()),
                TokenType::Desc,
            ],
        );
    }

    #[test]
    fn test_valid_gql_2() {
        assert_tokens_type(
            r#"where (
                        (author < '\u{27}')
                        and (
                            date < "2018-01-01T23:59:30.560Z"
                            or date >= '2019-01-01T00:00:00Z'
                            or date = "2018-06-09"
                        )
                    ) or (
                        file = [`^.*?\.rs$`, ".gitignore"]
                        and removed_files < added_files
                    )
                    order by date desc"#,
            vec![
                TokenType::Where,
                TokenType::ParenthesisLeft,
                TokenType::ParenthesisLeft,
                TokenType::Identifier("author".to_owned()),
                TokenType::Lower,
                TokenType::String("\u{27}".to_string()),
                TokenType::ParenthesisRight,
                TokenType::And,
                TokenType::ParenthesisLeft,
                TokenType::Identifier("date".to_owned()),
                TokenType::Lower,
                TokenType::String("2018-01-01T23:59:30.560Z".to_string()),
                TokenType::Or,
                TokenType::Identifier("date".to_owned()),
                TokenType::GreaterOrEqual,
                TokenType::String("2019-01-01T00:00:00Z".to_string()),
                TokenType::Or,
                TokenType::Identifier("date".to_owned()),
                TokenType::Equal,
                TokenType::String("2018-06-09".to_string()),
                TokenType::ParenthesisRight,
                TokenType::ParenthesisRight,
                TokenType::Or,
                TokenType::ParenthesisLeft,
                TokenType::Identifier("file".to_owned()),
                TokenType::Equal,
                TokenType::SquareBraceLeft,
                TokenType::RegularExpression(r"^.*?\.rs$".to_string()),
                TokenType::Comma,
                TokenType::String(".gitignore".to_string()),
                TokenType::SquareBraceRight,
                TokenType::And,
                TokenType::Identifier("removed_files".to_owned()),
                TokenType::Lower,
                TokenType::Identifier("added_files".to_owned()),
                TokenType::ParenthesisRight,
                TokenType::OrderBy,
                TokenType::Identifier("date".to_owned()),
                TokenType::Desc,
            ],
        );
    }
}
