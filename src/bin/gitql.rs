use clap::{App, Arg, ArgMatches};
use gitql::application::{filter, SearchMetadata, SearchResult};
use gitql::repository::RequestCredentials;
use gitql::{Request, DEFAULT_HOST, DEFAULT_PORT};
use prettytable::format::Alignment;
use prettytable::{Attr, Cell, Row, Table};
use serde::Deserialize;
use serde_json::value::Value::Number;
use serde_json::Value;
use std::env;
use std::io::{self, prelude::*, Error};
use std::net::{SocketAddr, TcpListener, TcpStream};
use std::thread;

pub const DEFAULT_PATH: &str = ".";

#[derive(Deserialize)]
struct Result {
    headers: Vec<String>,
    data: Value,
    metadata: SearchMetadata,
}

fn main() {
    let app = configure_application();
    let matches = app.get_matches();

    let host = matches.value_of("host").unwrap_or(DEFAULT_HOST);
    let port = matches.value_of("port").unwrap_or(DEFAULT_PORT);
    let upstream = matches.value_of("upstream").unwrap();
    let path = matches.value_of("path").unwrap_or(DEFAULT_PATH);

    let address = format!("{}:{}", host, port);

    println!("Connecting to {}", address);

    let mut connection = TcpStream::connect(address).expect("Unable to open connection.");

    println!("Welcome to GitQL.");
    println!("Type `q;`, `quit;`, `e;`, `exit;`, `c;`, or `close;`, to exit.");
    println!();

    loop {
        print!("> ");
        let _ = std::io::stdout().flush();

        let mut buffer = vec![0u8; 16384];

        match io::stdin().read(&mut buffer) {
            Ok(bytes_read) => {
                let query = String::from_utf8_lossy(&buffer[0..bytes_read]).to_string();
                if is_quit(&query) {
                    break;
                }

                process_request(&mut connection, upstream, path, query);
            }
            Err(error) => println!("Unable to read input. {}", error),
        }
    }
}

fn is_quit(query: &str) -> bool {
    let lowercase = query.to_lowercase();
    match lowercase.trim() {
        "q" | "quit" | "q;" | "quit;" | "e" | "exit" | "e;" | "exit;" | "c" | "close" | "c;"
        | "close;" => true,
        _ => false,
    }
}

fn process_request(connection: &mut TcpStream, upstream: &str, path: &str, query: String) {
    let request = Request {
        url: upstream.to_owned(),
        path: path.to_owned(),
        credentials: RequestCredentials::Default,
        query,
    };

    let json = serde_json::to_string(&request).expect("Unable to generate json.");

    let _ = connection
        .write(json.as_bytes())
        .expect("Unable to send request.");

    let mut buffer = vec![0u8; 16384];
    let bytes_read = connection
        .read(&mut buffer)
        .expect("Unable to read response.");

    let result: Result =
        serde_json::from_slice(&buffer[0..bytes_read]).expect("Unable to parse result json.");

    let mut table = Table::new();

    table.add_row(Row::new(
        result
            .headers
            .iter()
            .map(|h| Cell::new_align(h.as_str(), Alignment::CENTER).with_style(Attr::Bold))
            .collect(),
    ));

    let data = result.data.as_array().unwrap();

    for values in data {
        let row = values
            .as_array()
            .unwrap()
            .iter()
            .map(|v| match v {
                Value::Number(value) => {
                    Cell::new_align(format!("{}", v).as_str(), Alignment::RIGHT)
                }
                Value::String(value) => Cell::new(value.as_str()),
                Value::Bool(value) => Cell::new(format!("{}", value).as_str()),
                Value::Null => Cell::new_align("(null)", Alignment::CENTER),
                _ => Cell::new(format!("{}", v).as_str()),
            })
            .collect();

        table.add_row(Row::new(row));
    }

    table.printstd();

    println!(
        "Returned {} rows in {} ms.",
        result.metadata.result_count,
        result.metadata.time / 1_000
    );
}

fn configure_application<'a, 'b>() -> App<'a, 'b> {
    App::new("gitql")
            .version("1.0.0")
            .arg(
                Arg::with_name("upstream")
                    .short("u")
                    .long("upstream")
                    .takes_value(true)
                    .required(true)
                    .help("Defines repository url"),
            )
            .arg(
                Arg::with_name("path")
                    .long("path")
                    .default_value(DEFAULT_PATH)
                    .help("Defines repository url"),
            )
            .arg(Arg::with_name("username").long("user").requires("password").takes_value(true).help(
                "Git username, this requires the password property and cannot be used with SSH.",
            ))
            .arg(
                Arg::with_name("password")
                    .long("pass").takes_value(true)
                    .help("Git username."),
            )
            .arg(
                Arg::with_name("public_key")
                    .long("pub").takes_value(true)
                    .help("Git SSH public key path."),
            )
            .arg(
                Arg::with_name("private_key")
                    .long("pri").takes_value(true)
                    .help("Git SSH private key path, this requires the password property.").requires("password"),
            )
            .arg(
                Arg::with_name("host")
                    .long("host")
                    .default_value(DEFAULT_HOST)
                    .help("Sets the server host"),
            )
            .arg(
                Arg::with_name("port")
                    .short("p")
                    .long("port")
                    .default_value(DEFAULT_PORT)
                    .help("Sets the server port"),
            )
}
