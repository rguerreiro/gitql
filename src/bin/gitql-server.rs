use clap::{App, Arg};
use gitql::application::filter;
use gitql::{Request, DEFAULT_HOST, DEFAULT_PORT};
use std::env;
use std::io::{self, prelude::*, Error};
use std::net::{TcpListener, TcpStream};
use std::thread;

fn main() {
    let matches = App::new("gitql")
        .version("1.0.0")
        .arg(
            Arg::with_name("host")
                .long("host")
                .default_value(DEFAULT_HOST)
                .help("Sets the server host"),
        )
        .arg(
            Arg::with_name("port")
                .short("p")
                .long("port")
                .default_value(DEFAULT_PORT)
                .help("Sets the server port"),
        )
        .arg(
            Arg::with_name("verbose")
                .short("v")
                .long("verbose")
                .help("Makes it verbose"),
        )
        .get_matches();

    let host = matches.value_of("host").unwrap_or(DEFAULT_HOST);
    let port = matches.value_of("port").unwrap_or(DEFAULT_PORT);

    match tcp_server(host, port) {
        Ok(_) => {}
        Err(error) => println!("Unable to bind tcp listener. {:?}", error),
    }
}

fn tcp_server(host: &str, port: &str) -> io::Result<()> {
    let url = format!("{}:{}", host, port);
    println!("Binding tcp listener to {}", url);

    let listener =
        TcpListener::bind(url.as_str()).expect(format!("Unable to bind to {}", url).as_str());

    for stream in listener.incoming() {
        match stream {
            Err(e) => eprintln!("failed: {}", e),
            Ok(stream) => {
                thread::spawn(move || {
                    handle_client(stream).unwrap_or_else(|error| eprintln!("{:?}", error));
                });
            }
        }
    }

    Ok(())
}

fn handle_client(mut stream: TcpStream) -> Result<(), Error> {
    println!("Incoming connection from: {}", stream.peer_addr()?);
    println!("Stream: {:?}", stream);
    let mut buffer = vec![0; 16384]; // 16 kb
    loop {
        let bytes_read = stream.read(&mut buffer)?;

        println!("Read {} bytes.", bytes_read);
        if bytes_read == 0 {
            return Ok(());
        }

        let request: Request = serde_json::from_slice(&buffer[0..bytes_read])
            .expect("Unable to convert bytes into utf8 string.");

        let result = filter(request);
        match result {
            Ok(result) => {
                if let Ok(json) = serde_json::to_string(&result) {
                    let _ = stream.write(json.as_bytes())?;
                } else {
                    println!("Unable to generate json response");
                }
            }
            Err(error) => {
                println!("Unable to get the result. {:?}", error);
                let _ =
                    stream.write(format!("Unable to get the result. {:?}", error).as_bytes())?;
            }
        }
    }
}

//
//fn handle_client(mut stream: TcpStream) -> io::Result<()> {
//    let mut string = String::new();
//    let size = stream.read_to_string(&mut string)?;
//
//    let parts: Vec<&str> = string.splitn(2, " ").collect();
//
//    println!("filter({}, {}, {})", parts[2], parts[0], parts[1]);
//
//    let result = filter(parts[2], parts[0], parts[1]);
//    //        "./temp/rustup_test_run",
//    if let Ok(result) = result {
//        if let Ok(json) = serde_json::to_string(&result) {
//            stream.write(json.as_bytes())?;
//        } else {
//            println!("Unable to generate json response");
//        }
//    } else {
//        println!("Unable to get the result");
//    }
//
//    Ok(())
//}
