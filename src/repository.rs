use crate::Request;
pub use git2::build::*;
pub use git2::*;
use serde::{Deserialize, Serialize};
use std::path::Path;

pub type GitError = git2::Error;
pub type Result<T> = core::result::Result<T, GitError>;

const TEMPORARY_BRANCH_NAME: &str = "temp_branch_head";

#[derive(Debug, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub enum RequestCredentials {
    None,
    Default,
    SshKey {
        public_key: String,
        private_key: Option<String>,
        username: Option<String>,
        password: String,
    },
    SshMemory,
    SshInteractive,
    UsernamePassword {
        username: String,
        password: String,
    },
    Username {
        username: String,
    },
}

impl RequestCredentials {
    pub fn credential_callback(
        &self,
        url: &str,
        username: Option<&str>,
        credential_type: CredentialType,
    ) -> Result<Cred> {
        match self {
            RequestCredentials::None | RequestCredentials::Default => return Cred::default(),
            _ => return Cred::default(),
        }
    }
}

pub trait GitqlRepository {
    fn open_or_clone(request: &Request) -> Result<Repository>;

    fn clone_with_credentials(request: &Request) -> Result<Repository>;

    fn fetch_remote_pruning(&self, request: &Request) -> Result<()>;

    fn checkout_local_branch_from_commit(&self, branch_name: &str, commit: &Commit) -> Result<()>;
    fn checkout_local_branch_from_head(&self, branch_name: &str) -> Result<()>;
    fn checkout_remote_branch(&self, branch_name: &str) -> Result<()>;
    fn branches_ahead_behind(
        &self,
        first_branch_name: &str,
        second_branch_name: &str,
    ) -> Result<(usize, usize)>;

    fn checkout_temporary_branch(&self) -> Result<()>;
    fn cleanup_local_branches(&self) -> Result<()>;
    fn pull(&self) -> Result<Vec<(String, Branch)>>;
    fn local_branches(&self) -> Result<Vec<(String, Branch)>>;

    fn iter_branches<F>(
        &self,
        branch_type: Option<BranchType>,
        action: F,
    ) -> Result<Vec<(String, Branch)>>
    where
        F: Fn(&str, &Branch) -> Result<()>;

    fn diff_commit_with_first_parent(&self, commit: &Commit) -> Result<Diff>;
}

pub fn no_credentials(
    _url: &str,
    _username: Option<&str>,
    _credential_type: CredentialType,
) -> Result<Cred> {
    unimplemented!("No credentials registered.")
}
/*
pub fn open(url: &str, path: &str) -> Result<Repository> {
    // Credentials
    //        |url, username, credential_type| {
    //            let username = username.unwrap_or("git");
    //            println!("Connecting to SSH {} using username {}", url, username);
    //
    //            //        Cred::userpass_plaintext()
    //
    //            // Username is the same value as the name before the '@' in the SSH name.
    //            let ssh = Cred::ssh_key(
    //                username,
    //                Some("path/to/id_rsa.pub".as_ref()),
    //                "path/to/id_rsa".as_ref(),
    //                Some("id_rsa_passphrase"),
    //            )?;
    //
    //            println!("Found SSH");
    //            Ok(ssh)
    //        });

    let repository = Repository::open_or_clone(url, path, no_credentials)?;

    repository.fetch_remote_pruning(no_credentials)?;

    work(&repository)?;

    Ok(repository)
}

fn work(repository: &Repository) -> Result<()> {
    let remote_branches = repository.branches(Some(BranchType::Remote))?;
    for remote_branch in remote_branches {
        let remote_branch = remote_branch?.0;
        println!("###############################################################################");

        if let Ok(Some(name)) = remote_branch.name() {
            println!("Remote branch: {}", name);
        } else {
            println!("Remote branch has no name.");
        }

        let mut commit = remote_branch.get().peel_to_commit()?;
        let mut index = 0;
        loop {
            let mut parents = commit.parents();
            let parent = parents.next();
            if parent.is_none() {
                println!("{} has no parent", commit.id());
                break;
            }

            let parent = parent.unwrap();

            println!("---------------------------------------------------------------------------");
            println!("{} is parent of {}", parent.id(), commit.id());

            //            let commit_tree = commit.tree()?;
            //            let parent_tree = parent.tree()?;

            //            let diff =
            //                repository.diff_tree_to_tree(Some(&parent_tree), Some(&commit_tree), None)?;
            //
            //            diff.foreach(
            //                &mut |delta, _| {
            //                    println!("###### FILE CALLBACK ######");
            //                    println!("Status: {:?}", delta.status());
            //                    println!("Old path: {:?}", delta.old_file().path());
            //                    println!("New path: {:?}", delta.new_file().path());
            //                    println!(
            //                        "Old size: {}, New size: {}",
            //                        delta.old_file().size(),
            //                        delta.new_file().size()
            //                    );
            //
            //                    true
            //                },
            //                None,
            //                None,
            //                None,
            //            );

            commit = parent;
            index += 1;
            if index >= 3 {
                break;
            }
        }
    }

    //    repository.cleanup_local_branches()?;
    //    let branches = repository.pull()?;

    //    let branch_names: Vec<String> = branches
    //        .into_iter()
    //        .map(|(branch_name, _branch)| branch_name)
    //        .collect();
    //
    //    dbg!(branch_names);.

    //    {
    //        let branches = repository.branches(Some(BranchType::Remote))?;
    //
    //        for remote_branch in branches {
    //            let (remote_branch, _) = remote_branch?;
    //
    //            let remote_branch_reference = remote_branch.get();
    //            if let Some(remote_branch_name) = remote_branch_reference.shorthand() {
    //                if remote_branch_name.contains(TEMPORARY_BRANCH_NAME) {
    //                    continue;
    //                }
    //
    //                repository.checkout_remote_branch(remote_branch_name)?;
    //
    //                let ahead_behind =
    //                    repository.branches_ahead_behind(remote_branch_name, "origin/master")?;
    //
    //                dbg!(remote_branch_name);
    //                dbg!(ahead_behind);

    /*
    {
        let mut index = 0;
        let mut last_commit: Option<Commit> = None;

        let mut revwalk = repository.revwalk()?;
        revwalk.push_head()?;
        //                revwalk.set_sorting(Sort::TOPOLOGICAL | Sort::REVERSE);
        while let Some(actual_oid) = revwalk.next() {
            let actual_oid = actual_oid?;
            let actual_commit = repository.find_commit(actual_oid)?;

            println!("\n\n###################################################################################");
            println!("\nid: {}", actual_commit.id());
            println!("author: {}", actual_commit.author().to_string());
            println!(
                "time: {} {}",
                actual_commit.time().seconds(),
                actual_commit.time().offset_minutes()
            );
            println!("description: {:?}\n", actual_commit.message());

            if let Some(last_commit) = last_commit {
                let actual_tree = actual_commit.tree()?;
                let last_tree = last_commit.tree()?;

                // Reverse walk, so last_tree is actually newer than the current tree.
                let diff = repository.diff_tree_to_tree(
                    Some(&actual_tree),
                    Some(&last_tree),
                    None,
                )?;

                //                        file_cb: &mut FileCb,
                //                   binary_cb: Option<&mut BinaryCb>,
                //                   hunk_cb: Option<&mut HunkCb>,
                //                   line_cb: Option<&mut LineCb>

                //                        pub type FileCb<'a> = FnMut(DiffDelta, f32) -> bool + 'a;
                //pub type BinaryCb<'a> = FnMut(DiffDelta, DiffBinary) -> bool + 'a;
                //pub type HunkCb<'a> = FnMut(DiffDelta, DiffHunk) -> bool + 'a;
                //pub type LineCb<'a> = FnMut(DiffDelta, Option<DiffHunk>, DiffLine) -> bool + 'a;

                diff.foreach(
                    &mut |delta: DiffDelta, _| {
                        println!("###### FILE CALLBACK ######");
                        println!("Number of files: {}", delta.nfiles());
                        println!("Status: {:?}", delta.status());
                        println!("Old path: {:?}", delta.old_file().path());
                        println!("New path: {:?}", delta.new_file().path());
                        println!(
                            "Old size: {}, New size: {}",
                            delta.old_file().size(),
                            delta.new_file().size()
                        );

                        true
                    },
                    Some(&mut |delta: DiffDelta, _binary: DiffBinary| {
                        println!("###### BINARY CALLBACK ######");
                        println!("Number of files: {}", delta.nfiles());
                        println!("Status: {:?}", delta.status());
                        println!("Old path: {:?}", delta.old_file().path());
                        println!("New path: {:?}", delta.new_file().path());
                        println!(
                            "Old size: {}, New size: {}",
                            delta.old_file().size(),
                            delta.new_file().size()
                        );

                        true
                    }),
                    Some(&mut |delta: DiffDelta, hunk: DiffHunk| {
                        println!("###### HUNK CALLBACK ######");
                        println!("Number of files: {}", delta.nfiles());
                        println!("Status: {:?}", delta.status());
                        println!("Old path: {:?}", delta.old_file().path());
                        println!("New path: {:?}", delta.new_file().path());
                        println!(
                            "Old size: {}, New size: {}",
                            delta.old_file().size(),
                            delta.new_file().size()
                        );

                        println!(
                            "Header: {}",
                            String::from_utf8(Vec::from(hunk.header())).unwrap()
                        );
                        println!(
                            "Old lines: {}, Old start: {}",
                            hunk.old_lines(),
                            hunk.old_start()
                        );
                        println!(
                            "New lines: {}, New start: {}",
                            hunk.new_lines(),
                            hunk.new_start()
                        );

                        true
                    }),
                    Some(
                        &mut |delta: DiffDelta,
                              hunk: Option<DiffHunk>,
                              line: DiffLine| {
                            if line.content_offset() < 0 {
                                print!(
                                    "{}",
                                    String::from_utf8(Vec::from(line.content()))
                                        .unwrap()
                                );
                                return true;
                            }

                            println!("###### LINE CALLBACK ######");
                            println!("Number of files: {}", delta.nfiles());
                            println!("Status: {:?}", delta.status());
                            println!("Old path: {:?}", delta.old_file().path());
                            println!("New path: {:?}", delta.new_file().path());
                            println!(
                                "Old size: {}, New size: {}",
                                delta.old_file().size(),
                                delta.new_file().size()
                            );

                            println!(
                                "Content {} -> {}",
                                line.content_offset(),
                                String::from_utf8(Vec::from(line.content())).unwrap()
                            );
                            println!("Origin: {}", line.origin());
                            println!("Number of lines: {}", line.num_lines());
                            println!("Old line number: {:?}", line.old_lineno());
                            println!("New line number: {:?}", line.new_lineno());

                            if let Some(hunk) = hunk {
                                println!(
                                    "Header: {}",
                                    String::from_utf8(Vec::from(hunk.header()))
                                        .unwrap()
                                );
                                println!(
                                    "Old lines: {}, Old start: {}",
                                    hunk.old_lines(),
                                    hunk.old_start()
                                );
                                println!(
                                    "New lines: {}, New start: {}",
                                    hunk.new_lines(),
                                    hunk.new_start()
                                );
                            }

                            true
                        },
                    ),
                )?;
            }

            index += 1;
            last_commit = Some(actual_commit);

            if index > 1 {
                break;
            }
        }

        println!("FINISHED walking");
    }*/
//            } else {
//                println!("NO BRANCH NAME FOUND");
//            }
//        }
//
//        {
//            let mut temporary_branch =
//                repository.find_branch(TEMPORARY_BRANCH_NAME, BranchType::Local)?;
//            temporary_branch.delete()?;
//        }
//    }

Ok(())
}
*/

impl GitqlRepository for Repository {
    fn open_or_clone(request: &Request) -> Result<Repository> {
        match Self::open(request.path.as_str()) {
            Ok(repository) => Ok(repository),
            Err(_error) => Self::clone_with_credentials(request),
        }
    }

    fn clone_with_credentials(request: &Request) -> Result<Repository> {
        let mut fetch_options = FetchOptions::new();

        let mut callbacks = RemoteCallbacks::new();
        callbacks.credentials(|url, username, credential_type| {
            request.credential_callback(url, username, credential_type)
        });
        fetch_options.remote_callbacks(callbacks);

        let mut builder = RepoBuilder::new();
        builder
            .fetch_options(fetch_options)
            .clone(request.url.as_str(), Path::new(request.path.as_str()))
    }

    fn fetch_remote_pruning(&self, request: &Request) -> Result<()> {
        let mut fetch_options = FetchOptions::new();
        fetch_options.prune(FetchPrune::On);

        let mut callbacks = RemoteCallbacks::new();
        callbacks.credentials(|url, username, credential_type| {
            request.credential_callback(url, username, credential_type)
        });
        fetch_options.remote_callbacks(callbacks);

        let mut remote = self.find_remote("origin")?;
        let refspecs = remote.fetch_refspecs()?;

        let mut fetch_refspecs = Vec::with_capacity(refspecs.len());

        for refspec in refspecs.iter() {
            if let Some(name) = refspec {
                fetch_refspecs.push(name)
            }
        }

        remote.fetch(fetch_refspecs.as_slice(), Some(&mut fetch_options), None)
    }

    fn checkout_local_branch_from_commit(&self, branch_name: &str, commit: &Commit) -> Result<()> {
        let _local_branch = self.branch(branch_name, &commit, true)?;

        let treeish = self.revparse_single(branch_name)?;

        let mut checkout_builder = CheckoutBuilder::new();
        checkout_builder.safe();

        self.checkout_tree(&treeish, Some(&mut checkout_builder))?;

        self.set_head(format!("refs/heads/{}", branch_name).as_str())
    }

    fn checkout_local_branch_from_head(&self, branch_name: &str) -> Result<()> {
        let commit = self.head()?.peel_to_commit()?;
        self.checkout_local_branch_from_commit(branch_name, &commit)
    }

    fn checkout_remote_branch(&self, branch_name: &str) -> Result<()> {
        let remote_branch = self.find_branch(branch_name, BranchType::Remote)?;

        let simple_branch_name = branch_name.replacen("origin/", "", 1);

        let commit = remote_branch.get().peel_to_commit()?;

        let mut local_branch = self.branch(simple_branch_name.as_str(), &commit, true)?;
        local_branch.set_upstream(Some(branch_name))?;

        self.checkout_local_branch_from_commit(simple_branch_name.as_str(), &commit)
    }

    fn branches_ahead_behind(
        &self,
        first_branch_name: &str,
        second_branch_name: &str,
    ) -> Result<(usize, usize)> {
        let first_branch = self.find_branch(first_branch_name, BranchType::Remote)?;
        let first_branch_commit = first_branch.get().peel_to_commit()?;

        let second_branch = self.find_branch(second_branch_name, BranchType::Remote)?;
        let second_branch_commit = second_branch.get().peel_to_commit()?;

        self.graph_ahead_behind(first_branch_commit.id(), second_branch_commit.id())
    }

    fn checkout_temporary_branch(&self) -> Result<()> {
        let temporary_branch = self.find_branch(TEMPORARY_BRANCH_NAME, BranchType::Local);
        if temporary_branch.is_err() || !temporary_branch.unwrap().is_head() {
            return self.checkout_local_branch_from_head(TEMPORARY_BRANCH_NAME);
        }

        Ok(())
    }

    fn cleanup_local_branches(&self) -> Result<()> {
        self.checkout_temporary_branch()?;

        let branches = self.branches(Some(BranchType::Local))?;
        for local_branch in branches {
            let mut local_branch = local_branch?.0;
            if !local_branch.is_head() {
                local_branch.delete()?;
            }
        }

        Ok(())
    }

    fn pull(&self) -> Result<Vec<(String, Branch)>> {
        self.iter_branches(Some(BranchType::Remote), |remote_branch_name, _| {
            self.checkout_remote_branch(remote_branch_name)
        })
    }

    fn local_branches(&self) -> Result<Vec<(String, Branch)>> {
        self.iter_branches(Some(BranchType::Local), |_, _| Ok(()))
    }

    fn iter_branches<F>(
        &self,
        branch_filter: Option<BranchType>,
        action: F,
    ) -> Result<Vec<(String, Branch)>>
    where
        F: Fn(&str, &Branch) -> Result<()>,
    {
        let mut response: Vec<(String, Branch)> = Vec::new();

        let branches = self.branches(branch_filter)?;
        for remote_branch in branches {
            let (remote_branch, _) = remote_branch?;

            let remote_branch_reference = remote_branch.get();
            if let Some(remote_branch_name) = remote_branch_reference.shorthand() {
                if remote_branch_name.contains(TEMPORARY_BRANCH_NAME) {
                    continue;
                }

                action(remote_branch_name, &remote_branch)?;
                response.push((remote_branch_name.to_string(), remote_branch));
            }
        }

        Ok(response)
    }

    fn diff_commit_with_first_parent(&self, commit: &Commit) -> Result<Diff> {
        let mut parents = commit.parents();

        let commit = commit.tree()?;
        if let Some(parent) = parents.next() {
            let parent = parent.tree()?;
            self.diff_tree_to_tree(Some(&parent), Some(&commit), None)
        } else {
            self.diff_tree_to_tree(None, Some(&commit), None)
        }
    }
}
