use super::application::*;
use chrono::prelude::*;
use std::ops::Neg;
use time::Duration;

/// Value functions
pub fn is_null(value: Value) -> Value {
    if value.is_undefined() {
        return Value::Undefined;
    }

    (value == Value::Null).into()
}

pub fn to_string(value: Value) -> Value {
    if value.is_undefined() {
        return Value::Undefined;
    }

    match value {
        Value::String(_) => value,
        _ => value.to_string().into(),
    }
}

pub fn type_of(value: &Value) -> Value {
    if value.is_undefined() {
        return Value::Undefined;
    }

    match value {
        Value::Undefined => Value::Undefined,
        Value::Null => "Null".into(),
        Value::Date(_) => "Date".into(),
        Value::DateTime(_) => "DateTime".into(),
        Value::Duration(_) => "Duration".into(),
        Value::String(_) => "String".into(),
        Value::RegularExpression(_) => "RegularExpression".into(),
        Value::Integer(_) => "Integer".into(),
        Value::Float(_) => "Float".into(),
        Value::Boolean(_) => "Boolean".into(),
        Value::Set(_) => "Set".into(),
        Value::Range(_, _) => "Range".into(),
    }
}

pub fn nvl(values: Vec<Value>) -> Value {
    let mut v = None;

    for value in values.into_iter() {
        if value.is_undefined() {
            return Value::Undefined;
        } else if v.is_none() && value != Value::Null {
            v = Some(value);
        }
    }

    v.unwrap_or(Value::Null)
}

pub fn case(condition: Value, lhs: Value, rhs: Value) -> Option<Value> {
    if condition.is_undefined() || lhs.is_undefined() || rhs.is_undefined() {
        return Some(Value::Undefined);
    }

    match condition {
        Value::Boolean(condition) => {
            if condition {
                Some(lhs)
            } else {
                Some(rhs)
            }
        }
        _ => None,
    }
}

/// String functions
pub fn is_ascii(string: Value) -> Value {
    if string.is_undefined() {
        return Value::Undefined;
    }

    string.to_string().is_ascii().into()
}

pub fn replace(string: Value, source: Value, target: Value) -> Value {
    if string.is_undefined() || source.is_undefined() || target.is_undefined() {
        return Value::Undefined;
    }

    let string = string.to_string();
    let source = source.to_string();
    let target = target.to_string();
    Value::String(string.replace(source.as_str(), target.as_str()))
}

pub fn replace_count(string: Value, source: Value, target: Value, count: Value) -> Option<Value> {
    if string.is_undefined()
        || source.is_undefined()
        || target.is_undefined()
        || count.is_undefined()
    {
        return Some(Value::Undefined);
    }

    match count {
        Value::Integer(count) => {
            let string = string.to_string();
            let source = source.to_string();
            let target = target.to_string();
            let count = count as usize;
            Some(Value::String(string.replacen(
                source.as_str(),
                target.as_str(),
                count,
            )))
        }
        _ => None,
    }
}

pub fn lower(string: Value) -> Value {
    if string.is_undefined() {
        return Value::Undefined;
    }

    Value::String(string.to_string().to_lowercase())
}

pub fn ascii_lower(string: Value) -> Value {
    if string.is_undefined() {
        return Value::Undefined;
    }

    Value::String(string.to_string().to_ascii_lowercase())
}

pub fn upper(string: Value) -> Value {
    if string.is_undefined() {
        return Value::Undefined;
    }

    Value::String(string.to_string().to_uppercase())
}

pub fn ascii_upper(string: Value) -> Value {
    if string.is_undefined() {
        return Value::Undefined;
    }

    Value::String(string.to_string().to_ascii_uppercase())
}

pub fn substring(string: Value, from: Value, to: Option<Value>) -> Option<Value> {
    if string.is_undefined() {
        return Some(Value::Undefined);
    }

    match (from, to) {
        (Value::Undefined, Some(Value::Undefined)) => Some(Value::Undefined),

        (Value::Integer(from), None) => {
            if from < 0 {
                return Some(string);
            }

            let from = from as usize;
            let string = string.to_string();

            if from >= string.len() {
                Some("".into())
            } else {
                Some(string[from..].into())
            }
        }
        (Value::Integer(from), Some(Value::Integer(to))) => {
            if from < 0 {
                return Some(string);
            }

            let from = from as usize;
            let string = string.to_string();

            if from >= string.len() || to < 0 {
                Some("".into())
            } else {
                let to = (to as usize).min(string.len());
                Some(string[from..to].into())
            }
        }
        (_, _) => None,
    }
}

pub fn len(value: Value) -> Value {
    if value.is_undefined() {
        return Value::Undefined;
    }

    match value {
        Value::Set(values) => values.len().into(),
        _ => value.to_string().len().into(),
    }
}

pub fn format(value: Value, format: Value) -> Value {
    if value.is_undefined() || format.is_undefined() {
        return Value::Undefined;
    }

    let format = format.to_string();

    match value {
        //        Value::Integer(integer) =>
        //        Value::Float(float) =>
        Value::Date(date) => format!("{}", date.format(format.as_str())).into(),
        Value::DateTime(date_time) => format!("{}", date_time.format(format.as_str())).into(),
        _ => value.to_string().into(),
    }
}

pub fn trim(string: Value) -> Value {
    if string.is_undefined() {
        return Value::Undefined;
    }

    string.to_string().trim().into()
}

pub fn trim_end(string: Value) -> Value {
    if string.is_undefined() {
        return Value::Undefined;
    }

    string.to_string().trim_end().into()
}

pub fn trim_start(string: Value) -> Value {
    if string.is_undefined() {
        return Value::Undefined;
    }

    string.to_string().trim_start().into()
}

pub fn index_of(search: Value, into: Value) -> Value {
    if search.is_undefined() || into.is_undefined() {
        return Value::Undefined;
    }

    let search = search.to_string();
    match into.to_string().find(search.as_str()) {
        Some(index) => index.into(),
        None => (-1).into(),
    }
}

pub fn repeat(string: Value, count: Value) -> Value {
    if string.is_undefined() || count.is_undefined() {
        return Value::Undefined;
    }

    let string = string.to_string();
    match count {
        Value::Integer(count) => {
            if count <= 0 {
                "".into()
            } else {
                string.repeat(count as usize).into()
            }
        }
        _ => string.into(),
    }
}

/// Set functions
pub fn min_set(value: Value) -> Value {
    match value {
        Value::Set(values) => internal_min_values(&values),
        _ => value,
    }
}

pub fn max_set(value: Value) -> Value {
    if value.is_undefined() {
        return Value::Undefined;
    }

    match value {
        Value::Set(values) => internal_max_values(&values),
        _ => value,
    }
}

pub fn avg_set(value: Value) -> Option<Value> {
    if value.is_undefined() {
        return Some(Value::Undefined);
    }

    match value {
        Value::Set(values) => internal_avg_numbers(&values),
        _ => None,
    }
}

pub fn sum_set(value: Value) -> Option<Value> {
    if value.is_undefined() {
        return Some(Value::Undefined);
    }

    match value {
        Value::Set(values) => internal_sum_numbers(&values),
        _ => None,
    }
}

/// Date functions
pub fn now() -> Value {
    Utc::now().into()
}

pub fn to_date(value: Value, format: Value) -> Value {
    if value.is_undefined() || format.is_undefined() {
        return Value::Undefined;
    }

    match value {
        Value::Date(_) => value,
        Value::DateTime(date_time) => date_time.into(),
        _ => {
            let format = format.to_string();
            let string = value.to_string();

            let date_time = NaiveDateTime::parse_from_str(string.as_str(), format.as_str());
            if let Some(date_time) = date_time.ok().map(Value::from) {
                return date_time;
            }

            let date = NaiveDate::parse_from_str(string.as_str(), format.as_str());
            if let Some(date) = date.ok().map(Value::from) {
                date
            } else {
                Value::Null
            }
        }
    }
}

pub fn seconds(duration: Value) -> Option<Value> {
    if duration.is_undefined() {
        return Some(Value::Undefined);
    }

    match duration {
        Value::Integer(seconds) => Some(Duration::seconds(seconds).into()),
        Value::Float(seconds) => {
            let seconds_part = seconds as i64;
            let nanoseconds_part = (seconds - (seconds_part as f64)) / 1_000_000_000.0;

            Some(
                (Duration::seconds(seconds_part) + Duration::nanoseconds(nanoseconds_part as i64))
                    .into(),
            )
        }
        Value::Duration(_) => Some(duration),
        _ => None,
    }
}

pub fn minutes(duration: Value) -> Option<Value> {
    if duration.is_undefined() {
        return Some(Value::Undefined);
    }

    match duration {
        Value::Integer(minutes) => Some(Duration::minutes(minutes).into()),
        Value::Float(minutes) => {
            let minutes_part = minutes as i64;
            let seconds_part = (minutes - (minutes_part as f64)) / 60.0;

            match seconds(seconds_part.into()) {
                Some(Value::Duration(seconds)) => {
                    Some((Duration::minutes(minutes_part) + seconds).into())
                }
                _ => None,
            }
        }
        Value::Duration(_) => Some(duration),
        _ => None,
    }
}

pub fn hours(duration: Value) -> Option<Value> {
    if duration.is_undefined() {
        return Some(Value::Undefined);
    }

    match duration {
        Value::Integer(hours) => Some(Duration::hours(hours).into()),
        Value::Float(hours) => {
            let hours_part = hours as i64;
            let minutes_part = (hours - (hours_part as f64)) / 24.0;

            match minutes(minutes_part.into()) {
                Some(Value::Duration(minutes)) => {
                    Some((Duration::hours(hours_part) + minutes).into())
                }
                _ => None,
            }
        }
        Value::Duration(_) => Some(duration),
        _ => None,
    }
}

pub fn days(duration: Value) -> Option<Value> {
    if duration.is_undefined() {
        return Some(Value::Undefined);
    }

    match duration {
        Value::Integer(days) => Some(Duration::days(days).into()),
        Value::Float(days) => {
            let days_part = days as i64;
            let hours_part = (days - (days_part as f64)) / 60.0;

            match hours(hours_part.into()) {
                Some(Value::Duration(hours)) => Some((Duration::days(days_part) + hours).into()),
                _ => None,
            }
        }
        Value::Duration(_) => Some(duration),
        _ => None,
    }
}

pub fn weeks(duration: Value) -> Option<Value> {
    if duration.is_undefined() {
        return Some(Value::Undefined);
    }

    match duration {
        Value::Integer(weeks) => Some(Duration::weeks(weeks).into()),
        Value::Float(weeks) => {
            let weeks_part = weeks as i64;
            let days_part = (weeks - (weeks_part as f64)) / 60.0;

            match days(days_part.into()) {
                Some(Value::Duration(days)) => Some((Duration::weeks(weeks_part) + days).into()),
                _ => None,
            }
        }
        Value::Duration(_) => Some(duration),
        _ => None,
    }
}

pub fn seconds_between(from: Value, to: Value) -> Option<Value> {
    if from.is_undefined() || to.is_undefined() {
        return Some(Value::Undefined);
    }

    match (&from, &to) {
        (Value::Date(_), Value::Date(_))
        | (Value::Date(_), Value::DateTime(_))
        | (Value::DateTime(_), Value::Date(_))
        | (Value::DateTime(_), Value::DateTime(_)) => to - from,
        _ => None,
    }
}

pub fn minutes_between(from: Value, to: Value) -> Option<Value> {
    if from.is_undefined() || to.is_undefined() {
        return Some(Value::Undefined);
    }

    let seconds = seconds_between(from, to);

    match seconds {
        Some(Value::Duration(seconds)) => {
            let seconds = seconds.num_seconds() as f64;
            Some((seconds / 60f64).into())
        }
        _ => None,
    }
}

pub fn hours_between(from: Value, to: Value) -> Option<Value> {
    if from.is_undefined() || to.is_undefined() {
        return Some(Value::Undefined);
    }

    let minutes = minutes_between(from, to);

    match minutes {
        Some(Value::Integer(minutes)) => Some(((minutes as f64) / 60f64).into()),
        Some(Value::Float(minutes)) => Some((minutes / 60f64).into()),
        _ => None,
    }
}

pub fn days_between(from: Value, to: Value) -> Option<Value> {
    if from.is_undefined() || to.is_undefined() {
        return Some(Value::Undefined);
    }

    let hours = hours_between(from, to);

    match hours {
        Some(Value::Integer(hours)) => Some(((hours as f64) / 24f64).into()),
        Some(Value::Float(hours)) => Some((hours / 24f64).into()),
        _ => None,
    }
}

pub fn weeks_between(from: Value, to: Value) -> Option<Value> {
    if from.is_undefined() || to.is_undefined() {
        return Some(Value::Undefined);
    }

    let days = days_between(from, to);

    match days {
        Some(Value::Integer(days)) => Some(((days as f64) / 7f64).into()),
        Some(Value::Float(days)) => Some((days / 7f64).into()),
        _ => None,
    }
}

/// Number functions
pub fn abs(number: Value) -> Option<Value> {
    if number.is_undefined() {
        return Some(Value::Undefined);
    }

    match number {
        Value::Integer(number) => Some(i64::abs(number).into()),
        Value::Float(number) => Some(f64::abs(number).into()),
        Value::Duration(duration) => {
            let duration = if duration < Duration::zero() {
                duration.neg()
            } else {
                duration
            };

            Some(duration.into())
        }
        _ => None,
    }
}

pub fn sqrt(number: Value) -> Option<Value> {
    if number.is_undefined() {
        return Some(Value::Undefined);
    }

    match number {
        Value::Integer(number) => sqrt(Value::Float(number as f64)),
        Value::Float(number) => Some(number.sqrt().into()),
        _ => None,
    }
}

pub fn pow(base: Value, exponent: Value) -> Option<Value> {
    if base.is_undefined() || exponent.is_undefined() {
        return Some(Value::Undefined);
    }

    match (base, exponent) {
        (Value::Integer(base), Value::Integer(exponent)) => {
            let result = f64::powf(base as f64, exponent as f64);
            Some((result as i64).into())
        }
        (Value::Float(base), Value::Float(exponent)) => Some(f64::powf(base, exponent).into()),
        (Value::Float(base), Value::Integer(exponent)) => {
            Some(f64::powf(base, exponent as f64).into())
        }
        (Value::Integer(base), Value::Float(exponent)) => {
            Some(f64::powf(base as f64, exponent).into())
        }
        _ => None,
    }
}

pub fn floor(number: Value) -> Option<Value> {
    if number.is_undefined() {
        return Some(Value::Undefined);
    }

    match number {
        Value::Integer(_) => Some(number),
        Value::Float(number) => Some(number.floor().into()),
        _ => None,
    }
}

pub fn ceil(number: Value) -> Option<Value> {
    if number.is_undefined() {
        return Some(Value::Undefined);
    }

    match number {
        Value::Integer(_) => Some(number),
        Value::Float(number) => Some(number.ceil().into()),
        _ => None,
    }
}

pub fn round(number: Value) -> Option<Value> {
    if number.is_undefined() {
        return Some(Value::Undefined);
    }

    match number {
        Value::Integer(_) => Some(number),
        Value::Float(number) => Some(number.round().into()),
        _ => None,
    }
}

pub fn binary_string(number: Value) -> Option<Value> {
    if number.is_undefined() {
        return Some(Value::Undefined);
    }

    match number {
        Value::Integer(number) => Some(format!("{:#0b}", number).into()),
        Value::Float(number) => Some(format!("{:#0b}", number.to_bits()).into()),
        _ => None,
    }
}

pub fn octal_string(number: Value) -> Option<Value> {
    if number.is_undefined() {
        return Some(Value::Undefined);
    }

    match number {
        Value::Integer(number) => Some(format!("{:#0o}", number).into()),
        Value::Float(number) => Some(format!("{:#0o}", number.to_bits()).into()),
        _ => None,
    }
}

pub fn hexadecimal_string(number: Value) -> Option<Value> {
    if number.is_undefined() {
        return Some(Value::Undefined);
    }

    match number {
        Value::Integer(number) => Some(format!("{:#0x}", number).into()),
        Value::Float(number) => Some(format!("{:#0x}", number.to_bits()).into()),
        _ => None,
    }
}

pub fn max_values(values: Vec<Value>) -> Value {
    internal_max_values(&values)
}

fn internal_max_values(values: &[Value]) -> Value {
    let mut max_value = None;

    for value in values {
        if value.is_undefined() {
            return Value::Undefined;
        }

        max_value = match max_value.take() {
            None => Some(value),
            Some(max) => Some(max.max(value)),
        };
    }

    max_value.cloned().unwrap_or(Value::Null)
}

pub fn min_values(values: Vec<Value>) -> Value {
    internal_min_values(&values)
}

fn internal_min_values(values: &[Value]) -> Value {
    let mut min_value = None;

    for value in values {
        if value.is_undefined() {
            return Value::Undefined;
        }

        min_value = match min_value.take() {
            None => Some(value),
            Some(min) => Some(min.min(value)),
        };
    }

    min_value.cloned().unwrap_or(Value::Null)
}

pub fn avg_numbers(values: Vec<Value>) -> Option<Value> {
    internal_avg_numbers(&values)
}

fn internal_avg_numbers(values: &[Value]) -> Option<Value> {
    if values.is_empty() {
        return None;
    }

    let length = values.len();

    let mut sum = Value::Integer(0);

    for value in values {
        if value.is_undefined() {
            return Some(Value::Undefined);
        }

        match &sum + value {
            Some(value) => sum = value,
            None => return None,
        }
    }

    sum.clone() / Value::Float(length as f64)
}

pub fn sum_numbers(values: Vec<Value>) -> Option<Value> {
    internal_sum_numbers(&values)
}

fn internal_sum_numbers(values: &[Value]) -> Option<Value> {
    if values.is_empty() {
        return None;
    }

    let mut sum = Value::Integer(0);

    for value in values {
        if value.is_undefined() {
            return Some(Value::Undefined);
        }

        match &sum + value {
            Some(value) => sum = value,
            None => return None,
        }
    }

    Some(sum.clone())
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::sync::Arc;

    #[test]
    fn test_is_null() {
        assert_eq!(is_null(Value::Null), (true).into());
        assert_eq!(is_null(Utc::now().date().into()), (false).into());
        assert_eq!(is_null(Utc::now().into()), (false).into());
        assert_eq!(is_null(Duration::min_value().into()), (false).into());
        assert_eq!(is_null("".into()), (false).into());
        assert_eq!(
            is_null(RegularExpression::new(r"\d+").into()),
            (false).into()
        );
        assert_eq!(is_null((0).into()), (false).into());
        assert_eq!(is_null((0.0).into()), (false).into());
        assert_eq!(is_null((false).into()), (false).into());

        let set: Vec<Value> = vec![1.into()];
        assert_eq!(is_null(Value::Set(set)), (false).into());

        let set: Vec<Value> = vec![Value::Null];
        assert_eq!(is_null(Value::Set(set)), (false).into());

        assert_eq!(
            is_null(Value::Range(Arc::new((0).into()), Arc::new((100).into()))),
            (false).into()
        );
        assert_eq!(
            is_null(Value::Range(Arc::new(Value::Null), Arc::new(Value::Null))),
            (false).into()
        );
    }

    #[test]
    fn test_to_string() {
        assert_eq!(to_string(Value::Null), "(null)".into());
        assert_eq!(to_string(Utc.ymd(2019, 3, 10).into()), "2019-03-10".into());
        assert_eq!(
            to_string(Utc.ymd(2019, 3, 10).and_hms(18, 56, 2).into()),
            "2019-03-10T18:56:02Z".into()
        );
        assert_eq!(to_string(Duration::seconds(10).into()), "PT10S".into());
        assert_eq!(to_string("a string".into()), "a string".into());
        assert_eq!(
            to_string(RegularExpression::new(r"\d+").into()),
            r"\d+".into()
        );
        assert_eq!(to_string((0).into()), "0".into());
        assert_eq!(to_string((0.1).into()), "0.1".into());
        assert_eq!(to_string((false).into()), "false".into());

        let set: Vec<Value> = vec![1.into(), Value::Null, "text".into()];
        assert_eq!(to_string(Value::Set(set)), r#"[1, (null), "text"]"#.into());

        let set: Vec<Value> = vec![Value::Null];
        assert_eq!(to_string(Value::Set(set)), "[(null)]".into());

        assert_eq!(
            to_string(Value::Range(Arc::new((0).into()), Arc::new((100).into()))),
            "0..100".into()
        );
        assert_eq!(
            to_string(Value::Range(Arc::new(Value::Null), Arc::new(Value::Null))),
            "(null)..(null)".into()
        );
    }

    #[test]
    fn test_type_of() {
        assert_eq!(type_of(&Value::Null), "Null".into());
        assert_eq!(type_of(&Utc::now().date().into()), "Date".into());
        assert_eq!(type_of(&Utc::now().into()), "DateTime".into());
        assert_eq!(type_of(&Duration::min_value().into()), "Duration".into());
        assert_eq!(type_of(&"".into()), "String".into());
        assert_eq!(
            type_of(&RegularExpression::new(r"\d+").into()),
            "RegularExpression".into()
        );
        assert_eq!(type_of(&(0).into()), "Integer".into());
        assert_eq!(type_of(&(0.0).into()), "Float".into());
        assert_eq!(type_of(&(false).into()), "Boolean".into());

        let set: Vec<Value> = vec![1.into()];
        assert_eq!(type_of(&Value::Set(set)), "Set".into());

        assert_eq!(
            type_of(&Value::Range(Arc::new((0).into()), Arc::new((100).into()))),
            "Range".into()
        );
    }

    #[test]
    fn test_nvl() {
        assert_eq!(nvl(vec![Value::Null, "A TEXT".into()]), "A TEXT".into());
        assert_eq!(nvl(vec![(false).into(), Value::Null]), (false).into());
        assert_eq!(nvl(vec![Value::Null, Value::Null]), Value::Null);
        assert_eq!(nvl(vec![]), Value::Null);
    }

    #[test]
    fn test_case() {
        assert_eq!(
            case((true).into(), Value::Null, "A TEXT".into()),
            Some(Value::Null)
        );
        assert_eq!(
            case((false).into(), "some text".into(), "other text".into()),
            Some("other text".into())
        );

        assert_eq!(
            case(Value::Null, "some text".into(), "other text".into()),
            None
        );
        assert_eq!(
            case("true".into(), "some text".into(), "other text".into()),
            None
        );
    }

    #[test]
    fn test_is_ascii() {
        assert_eq!(is_ascii((true).into()), (true).into());
        assert_eq!(is_ascii("hello".into()), (true).into());
        assert_eq!(is_ascii("Aí".into()), (false).into());

        let set: Vec<Value> = vec!["ó".into()];
        assert_eq!(is_ascii(Value::Set(set)), (false).into());
    }

    #[test]
    fn test_replace() {
        assert_eq!(
            replace("this is old".into(), "old".into(), "new".into()),
            "this is new".into()
        );
        assert_eq!(
            replace("this is old".into(), "lamb".into(), "new".into()),
            "this is old".into()
        );
    }

    #[test]
    fn test_replace_count() {
        assert_eq!(
            replace_count("this is old".into(), "old".into(), "new".into(), 0.into()),
            Some("this is old".into())
        );
        assert_eq!(
            replace_count("this is old".into(), "old".into(), "new".into(), 10.into()),
            Some("this is new".into())
        );
        assert_eq!(
            replace_count("this is old".into(), "is".into(), "ese".into(), 1.into()),
            Some("these is old".into())
        );
        assert_eq!(
            replace_count(
                "this is old".into(),
                "this".into(),
                "that".into(),
                (1.1).into(),
            ),
            None
        );
    }

    #[test]
    fn test_lower() {
        assert_eq!(lower(1234.into()), "1234".into());
        assert_eq!(lower("all lowercase".into()), "all lowercase".into());
        assert_eq!(lower("ALL LOWERCASE".into()), "all lowercase".into());
        assert_eq!(lower("ÁÉÍÓÚ AEIOU".into()), "áéíóú aeiou".into());
    }

    #[test]
    fn test_ascii_lower() {
        assert_eq!(ascii_lower(1234.into()), "1234".into());
        assert_eq!(ascii_lower("all lowercase".into()), "all lowercase".into());
        assert_eq!(ascii_lower("ALL LOWERCASE".into()), "all lowercase".into());
        assert_eq!(
            ascii_lower("ÁÉÍÓÚ AEIOU".into()),
            "ÁÉÍÓÚ aeiou".into()
        );
    }

    #[test]
    fn test_upper() {
        assert_eq!(upper((12.34).into()), "12.34".into());
        assert_eq!(upper("all uppercase".into()), "ALL UPPERCASE".into());
        assert_eq!(upper("ALL UPPERCASE".into()), "ALL UPPERCASE".into());
        assert_eq!(upper("áéíóú aeiou".into()), "ÁÉÍÓÚ AEIOU".into());
    }

    #[test]
    fn test_ascii_upper() {
        assert_eq!(ascii_upper((12.34).into()), "12.34".into());
        assert_eq!(ascii_upper("all uppercase".into()), "ALL UPPERCASE".into());
        assert_eq!(ascii_upper("ALL UPPERCASE".into()), "ALL UPPERCASE".into());
        assert_eq!(
            ascii_upper("áéíóú aeiou".into()),
            "áéíóú AEIOU".into()
        );
    }

    #[test]
    fn test_substring() {
        assert_eq!(
            substring("this is a test string".into(), 10.into(), None),
            Some("test string".into())
        );
        assert_eq!(
            substring("this is a test string".into(), 10.into(), Some(14.into())),
            Some("test".into())
        );
        assert_eq!(
            substring("this is a test string".into(), 10.into(), Some(999.into())),
            Some("test string".into())
        );
        assert_eq!(
            substring("this is a test string".into(), (-10).into(), None),
            Some("this is a test string".into())
        );
    }

    #[test]
    fn test_len() {
        assert_eq!(len((12.34).into()), 5.into());
        assert_eq!(len("".into()), 0.into());
        assert_eq!(len("string".into()), 6.into());
    }

    #[test]
    fn test_trim() {
        assert_eq!(trim("".into()), "".into());
        assert_eq!(trim(1234.into()), "1234".into());
        assert_eq!(trim(" test     spaces ".into()), "test     spaces".into());
        assert_eq!(trim("test     spaces ".into()), "test     spaces".into());
        assert_eq!(trim(" test     spaces".into()), "test     spaces".into());
        assert_eq!(trim("test     spaces".into()), "test     spaces".into());
    }

    #[test]
    fn test_trim_end() {
        assert_eq!(trim_end("".into()), "".into());
        assert_eq!(trim_end(1234.into()), "1234".into());
        assert_eq!(
            trim_end(" test     spaces ".into()),
            " test     spaces".into()
        );
        assert_eq!(
            trim_end("test     spaces ".into()),
            "test     spaces".into()
        );
        assert_eq!(
            trim_end(" test     spaces".into()),
            " test     spaces".into()
        );
        assert_eq!(trim_end("test     spaces".into()), "test     spaces".into());
    }

    #[test]
    fn test_trim_start() {
        assert_eq!(trim_start("".into()), "".into());
        assert_eq!(trim_start(1234.into()), "1234".into());
        assert_eq!(
            trim_start(" test     spaces ".into()),
            "test     spaces ".into()
        );
        assert_eq!(
            trim_start("test     spaces ".into()),
            "test     spaces ".into()
        );
        assert_eq!(
            trim_start(" test     spaces".into()),
            "test     spaces".into()
        );
        assert_eq!(
            trim_start("test     spaces".into()),
            "test     spaces".into()
        );
    }

    #[test]
    fn test_index_of() {
        assert_eq!(index_of("x".into(), "abcxabcx".into()), 3.into());
        assert_eq!(index_of("z".into(), "abcxabcx".into()), (-1).into());
        assert_eq!(index_of("".into(), "".into()), 0.into());
    }

    #[test]
    fn test_repeat() {
        assert_eq!(repeat("".into(), 3.into()), "".into());
        assert_eq!(repeat("a".into(), 3.into()), "aaa".into());
        assert_eq!(repeat("abc".into(), 3.into()), "abcabcabc".into());
    }
}
