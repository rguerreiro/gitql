use super::application::Column;
use super::ast::{Expression, Order, Statement};
use std::rc::Rc;
use std::sync::Arc;

#[derive(Eq, PartialEq, Debug, Copy, Clone)]
pub enum ScanScope {
    Value,
    Commit,
    Diff,
    Delta,
}

impl ScanScope {
    fn ordinal(self) -> u16 {
        match self {
            ScanScope::Delta => 0x010,
            ScanScope::Diff => 0x020,
            ScanScope::Commit => 0x040,
            ScanScope::Value => 0x080,
        }
    }
}

impl From<Column> for ScanScope {
    fn from(column: Column) -> Self {
        match column {
            Column::DeltaOldFilePath
            | Column::DeltaNewFilePath
            | Column::DeltaOldFileName
            | Column::DeltaNewFileName
            | Column::DeltaOldFileExtension
            | Column::DeltaNewFileExtension => ScanScope::Delta,

            Column::DiffAddedFiles
            | Column::DiffDeletedFiles
            | Column::DiffRenamedFiles
            | Column::DiffModifiedFiles
            | Column::DiffInsertions
            | Column::DiffFilesChanged
            | Column::DiffDeletions => ScanScope::Diff,

            Column::CommitAuthor
            | Column::CommitAuthorName
            | Column::CommitAuthorEmail
            | Column::CommitHash
            | Column::CommitMessage
            | Column::CommitDate
            | Column::CommitIsMerge
            | Column::CommitIsRoot => ScanScope::Commit,
        }
    }
}

impl Default for ScanScope {
    fn default() -> Self {
        ScanScope::Value
    }
}

impl core::cmp::Ord for ScanScope {
    fn cmp(&self, other: &ScanScope) -> core::cmp::Ordering {
        self.ordinal().cmp(&other.ordinal())
    }
}

impl core::cmp::PartialOrd for ScanScope {
    fn partial_cmp(&self, other: &ScanScope) -> Option<core::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Eq, PartialEq, Debug, Copy, Clone)]
pub struct ExecutionPlan {
    pub select: ScanScope,
    pub condition: ScanScope,
    pub order: ScanScope,
    pub group: ScanScope,
    pub having: ScanScope,
}

impl ExecutionPlan {
    pub fn new(statement: &Statement) -> Self {
        let mut select = None;
        let mut condition = None;
        let mut order = None;
        let mut group = None;
        let mut having = None;

        if let Some(s) = &statement.select {
            let expressions: Vec<Arc<Expression>> = s
                .selections
                .iter()
                .map(|selection| selection.expression.clone())
                .collect();
            select = Self::evaluate_scopes(&expressions);
        }

        if let Some(c) = &statement.condition {
            condition = Self::evaluate_scope(c);
        }

        if let Some(o) = &statement.order {
            let expressions: Vec<Arc<Expression>> = o
                .iter()
                .map(|o| match o {
                    Order::Asc(e) | Order::Desc(e) => e.clone(),
                })
                .collect();
            order = Self::evaluate_scopes(&expressions);
        }

        if let Some(g) = &statement.group {
            group = Self::evaluate_scopes(g);
        }

        if let Some(h) = &statement.having {
            having = Self::evaluate_scope(h);
        }

        Self {
            select: select.unwrap_or_default(),
            condition: condition.unwrap_or_default(),
            order: order.unwrap_or_default(),
            group: group.unwrap_or_default(),
            having: having.unwrap_or_default(),
        }
    }

    fn evaluate_scopes(expressions: &[Arc<Expression>]) -> Option<ScanScope> {
        let mut smallest_scope = None;

        for expression in expressions {
            let scope = Self::evaluate_scope(expression);
            smallest_scope = Self::smallest_scope(smallest_scope, scope);
        }

        smallest_scope
    }

    #[inline]
    fn smallest_scope(lhs: Option<ScanScope>, rhs: Option<ScanScope>) -> Option<ScanScope> {
        if lhs.is_none() && rhs.is_none() {
            return None;
        }

        if lhs.is_none() && rhs.is_some() {
            return rhs;
        }

        if rhs.is_none() {
            return lhs;
        }

        let lhs = lhs.unwrap();
        let rhs = rhs.unwrap();

        if lhs < rhs {
            Some(lhs)
        } else {
            Some(rhs)
        }
    }

    fn evaluate_scope(expression: &Expression) -> Option<ScanScope> {
        match expression {
            Expression::AllColumns => Some(ScanScope::Delta),
            Expression::Identifier(identifier) => {
                if let Some(column) = Column::from_name(identifier) {
                    Some(column.into())
                } else {
                    Some(ScanScope::Value)
                }
            }

            Expression::Null
            | Expression::String(_)
            | Expression::Integer(_)
            | Expression::Float(_)
            | Expression::Boolean(_)
            | Expression::RegularExpression(_) => Some(ScanScope::Value),

            Expression::InterpolatedString(expressions) | Expression::Set(expressions) => {
                Self::evaluate_scopes(expressions.as_slice())
            }

            Expression::Function(_, arguments) => {
                let expressions: Vec<Arc<Expression>> =
                    arguments.iter().map(|arg| arg.expression.clone()).collect();
                Self::evaluate_scopes(&expressions)
            }

            Expression::Not(expression)
            | Expression::Plus(expression)
            | Expression::Minus(expression)
            | Expression::BitInverse(expression) => Self::evaluate_scope(expression),

            Expression::Range(lhs, rhs)
            | Expression::Add(lhs, rhs)
            | Expression::Subtract(lhs, rhs)
            | Expression::Multiply(lhs, rhs)
            | Expression::Divide(lhs, rhs)
            | Expression::Modulo(lhs, rhs)
            | Expression::BitAnd(lhs, rhs)
            | Expression::BitOr(lhs, rhs)
            | Expression::BitXor(lhs, rhs)
            | Expression::BitShiftRight(lhs, rhs)
            | Expression::BitShiftLeft(lhs, rhs)
            | Expression::Equal(lhs, rhs)
            | Expression::NotEqual(lhs, rhs)
            | Expression::Contains(lhs, rhs)
            | Expression::NotContains(lhs, rhs)
            | Expression::Lower(lhs, rhs)
            | Expression::LowerOrEqual(lhs, rhs)
            | Expression::Greater(lhs, rhs)
            | Expression::GreaterOrEqual(lhs, rhs)
            | Expression::And(lhs, rhs)
            | Expression::Or(lhs, rhs) => {
                let lhs = Self::evaluate_scope(lhs);
                let rhs = Self::evaluate_scope(rhs);

                Self::smallest_scope(lhs, rhs)
            }
        }
    }

    pub fn min_scope(&self) -> ScanScope {
        self.select
            .min(self.condition)
            .min(self.order)
            .min(self.group)
            .min(self.having)
    }
}

#[cfg(test)]
mod tests {
    use super::super::ast::{self, ASTError};
    use super::*;

    #[test]
    fn test_simple_condition_smallest_scope() -> Result<(), ASTError> {
        let statement = ast::generate_ast(r#"where commit_hash ~ "bugfix";"#)?;

        let plan = ExecutionPlan::new(&statement);

        assert_eq!(plan.condition, ScanScope::Commit);
        Ok(())
    }

    #[test]
    fn test_all_scopes() -> Result<(), ASTError> {
        let statement = ast::generate_ast(r#"select old_file_path where commit_hash ~ "bugfix" order by commit_hash desc group by added_files having 1 = 1;"#)?;

        let plan = ExecutionPlan::new(&statement);

        assert_eq!(plan.select, ScanScope::Delta);
        assert_eq!(plan.condition, ScanScope::Commit);
        assert_eq!(plan.order, ScanScope::Commit);
        assert_eq!(plan.group, ScanScope::Diff);
        assert_eq!(plan.having, ScanScope::Value);
        Ok(())
    }
}
