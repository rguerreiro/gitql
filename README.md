[![dependency status](https://deps.rs/repo/gitlab/rguerreiro/gitql/status.svg)](https://deps.rs/repo/gitlab/rguerreiro/gitql)

# GitQL

GitQL is a query language implemented in Rust that can be used to extract information and insights of a git repository.

## Setup and run

### Install Rust and Cargo

[Rust official install guide](https://doc.rust-lang.org/cargo/getting-started/installation.html)

On Mac or Linux
```
curl https://sh.rustup.rs -sSf | sh
```

On Windows, [download and run rustup-init.exe.](https://win.rustup.rs/)

### Download the repository

Clone or download this repository.

### Running with cargo

* run => `cargo run --release`
* tests => `cargo test`
* build => `cargo build --release`
* install => `cargo install --path .`

### Running queries with nc

GitQL will open a TCP Server on the desired host and port, you can use any TCP client to connect and send requests.

On Mac
```
nc localhost 44785
http://git/repo/url path/of/the/repo select commit_hash where commit_hash ~ 'beef' limit 1;
```

## Query information

### Keywords

Name |
---|
and |
asc |
by |
desc |
distinct |
false |
group |
having |
limit |
null |
offset |
or |
select |
sort |
true |
where |

### Data types

Name | Notation
--- | ---
Null | `null`
String | `"string"` or `'string'`
Integer (Number) | `1_000` or `0b0101` or `0o007` or `0xbeef`
Float (Number) | `1.5` or `1.5e-1_000`
Boolean | `true` or `false`
Regular expression | `` `[a-zA-Z0-9]` ``
DateTime | `'2019-01-01T23:00:00Z'`
Date | `'2019-01-01'`
Duration | `'2019-01-01' - '2018-01-01'`
Range | `0..10`
Set | `[1, 2, 3]`

### Operators

Name | Symbol | Class | Types
--- | --- | --- | ---
Plus | + | Unary | Number
Minus | - | Unary | Number
Not | ! | Unary | Boolean
Bit inverse | ~ | Unary | Number
Addition | + | Binary | Number, String, Set, Date, DateTime, Duration, Set
Subtraction | - | Binary | Number, Date, DateTime, Duration
Multiplication | * | Binary | Number
Division | / | Binary | Number
Modulo | % | Binary | Number
Bit and | & | Binary | Number, Boolean
Bit or | &#124; | Binary | Number, Boolean
Bit xor | ^ | Binary | Number, Boolean
Bit shift left | << | Binary | Number
Bit shift right | >> | Binary | Number
Equal | = | Binary | *All*
Not equal | != | Binary | *All*
Contains | ~ | Binary | *All*
Not contains | != | Binary | *All*
Lower | < | Binary | *All*
Lower than or equal | <= | Binary | *All*
Greater | > | Binary | *All*
Greater than or equal | >= | Binary | *All*

### Columns

Name | Type | Scope
--- | --- | ---
old_file_path | Optional String | Commit Diff Delta
new_file_path | Optional String | Commit Diff Delta
old_file_name | Optional String | Commit Diff Delta
new_file_name | Optional String | Commit Diff Delta
old_file_extension | Optional String | Commit Diff Delta
new_file_extension | Optional String | Commit Diff Delta
added_files | Integer | Commit Diff
deleted_files | Integer | Commit Diff
renamed_files | Integer | Commit Diff
modified_files | Integer | Commit Diff
commit_insertions | Integer | Commit Diff
files_changed | Integer | Commit Diff
commit_deletions | Integer | Commit Diff
author | Optional String | Commit
author_name | Optional String | Commit
author_email | Optional String | Commit
commit_hash | String | Commit
commit_message | String | Commit
commit_date | DateTime | Commit
is_merge_commit | Boolean | Commit
is_root_commit | Boolean | Commit

### Functions

Name | Type | Examples | Description
--- | --- | --- | ---
is_null | *All* | `is_null(old_file_path)` | Returns true if the provided value is null, false otherwise
to_string | *All* | `to_string(modified_files)` | Returns the value as String, Null value is parsed as "(null)"
type_of | *All* | `type_of(last_branch_commit_date)` | Returns name of the type of a given value
nvl | *All* | `nvl(old_file_path, "/default/path")` | Returns the first non-null value, returns null if none is found 
coalesce | *All* | `coalesce(old_file_path, new_file_path)` | *Same as nvl*
case | *All* | `case(when: 1 == 1, then: modified_files, else: added_files)` | If the `when` argument is true, it returns the `then` argument, if it's false, it returns the `else` argument
max | *All* | `max(added_files, 10)` | Returns the largest of all of the values provided
min | *All* | `min(deleted_files, added_files)` | Returns the smallest of all of the values provided
is_ascii | String | `is_ascii("aéiou")` | Checks if all characters in this string are within the ASCII range
replace | String | `replace(author_name, from: "a", to: "e")` | Replaces all matches of the second argument by the third
replace | String | `replace(author_name, from: "a", to: "e", count: 1)` | Replaces the count number matches of the second argument by the third
lower | String | `lower("ALL UPPERCASE")` | Makes the string lowercase, even non-ascii characters
ascii_lower | String | `ascii_lower("ALL UPPERCASE")` | Makes the string lowercase, excluding non-ascii characters
upper | String | `upper("all lowercase")` | Makes the string uppercase, even non-ascii characters
ascii_upper | String | `ascii_upper("all lowercase")` | Makes the string uppercase, excluding non-ascii characters
substring | String | `substring("a text", from: 1, to: 5)` | 
subs | String | `subs("a text", from: 1, to: 5)` | Same as *substring(:from:to:)*
substring | String | `substring("a text", from: 2)` | 
subs | String | `subs("a text", from: 2)` | Same as *substring(:from:)*
substring | String | `substring("a text", to: 3)` | 
subs | String | `subs("a text", to: 3)` | Same as *substring(:to:)*
len | String | `len(commit_message)` |
trim | String | `trim("  with spaces  ")` |
trim_start | String | `trim_start("  with spaces  ")` |
trim_left | String | `trim_left("  with spaces  ")` | Same as *trim_start*
l_trim | String | `l_trim("  with spaces  ")` | Same as *trim_start*
ltrim | String | `ltrim("  with spaces  ")` | Same as *trim_start*
trim_end | String | `trim_end("  with spaces  ")` |
trim_right | String | `trim_right("  with spaces  ")` | Same as *trim_end*
r_trim | String | `r_trim("  with spaces  ")` | Same as *trim_end*
rtrim | String | `rtrim("  with spaces  ")` | Same as *trim_end*
index_of | String | `index_of("text", into: "looks for index in this text")` |
last_index_of | String | `last_index_of("text", into: "looks for index in this text")` | **Not implemented yet**
repeat | String | `repeat("text", count: 10)` |
replicate | String | `replicate("text", count: 10)` | Same as *replicate*
now | DateTime | `now()` | Returns the current timestamp as a DateTime
to_date | DateTime | `to_date("", format: "%Y-%m-%dT%H:%M:%SZ")` |
seconds | DateTime | `seconds(2_000)` |
second | DateTime | `second(1)` | Same as *seconds*
sec | DateTime | `sec(1000)` | Same as *seconds*
minutes | DateTime | `minutes(3600)` |
minute | DateTime | `minute(1)` | Same as *minutes*
hours | DateTime | `hours(24)` |
hour | DateTime | `hour(1)` | Same as *hours*
days | DateTime | `days(31)` |
day | DateTime | `day(1)` | Same as *days*
weeks | DateTime | `weeks(5)` |
week | DateTime | `week(1)` | Same as *weeks*
seconds_between | DateTime | `seconds_between(from: "2019-01-01", to: "2019-02-01")` |
minutes_between | DateTime | `minutes_between(from: "2019-01-01", to: "2019-02-01")` |
hours_between | DateTime | `hours_between(from: "2019-01-01", to: "2019-02-01")` |
days_between | DateTime | `days_between(from: "2019-01-01", to: "2019-02-01")` |
weeks_between | DateTime | `weeks_between(from: "2019-01-01", to: "2019-02-01")` |
avg | Number | `avg(modified_files, 100)` | Returns the average of all of the numbers provided
sum | Number | `sum(modified_files, added_files)` | Returns the sum of all of the numbers provided
abs | Number | `abs(-123)` | Returns the absolute value of the given number
sqrt | Number | `sqrt(25)` | 
pow | Number | `pow(base: 5, exponent: 3)` | 
floor | Number | `floor(5.9)` | 
ceil | Number | `ceil(5.1)` | 
round | Number | `round(5.4)` | 
binary_string | Number | `binary_string(55)` | 
as_binary | Number | `as_binary (55)` | Same as *binary_string*
to_binary | Number | `to_binary(55)` | Same as *binary_string*
bin_string | Number | `bin_string(55)` | Same as *binary_string*
as_bin | Number | `as_bin(55)` | Same as *binary_string*
to_bin | Number | `to_bin(55)` | Same as *binary_string*
octal_string | Number | `octal_string(55)` | 
as_octal | Number | `as_octal (55)` | Same as *octal_string*
to_octal | Number | `to_octal(55)` | Same as *octal_string*
oct_string | Number | `oct_string(55)` | Same as *octal_string*
as_oct | Number | `as_oct(55)` | Same as *octal_string*
to_oct | Number | `to_oct(55)` | Same as *octal_string*
hexadecimal_string | Number | `hexadecimal_string(55)` | 
as_hexadecimal | Number | `as_hexadecimal (55)` | Same as *hexadecimal_string*
to_hexadecimal | Number | `to_hexadecimal(55)` | Same as *hexadecimal_string*
hex_string | Number | `hex_string(55)` | Same as *hexadecimal_string*
as_hex | Number | `as_hex(55)` | Same as *hexadecimal_string*
to_hex | Number | `to_hex(55)` | Same as *hexadecimal_string*
max | Set | `max([added_files, 10])` | Returns the maximum of all of the values provided
min | Set | `min([deleted_files, added_files])` | Returns the minimum of all of the values provided
avg | Set | `avg([modified_files, 100])` | Returns the average of all of the numbers provided
sum | Set | `sum([modified_files, added_files])` | Returns the sum of all of the numbers provided

### Aggregate functions

Functions used with `group by` and filtered by `having`.

Name | Type | Examples | Description
--- | --- | --- | ---
count | *All* | `count(commit_insertions)` | 
count | *All* | `count(unique: commit_insertions)` | 
max | *All* | `max(commit_insertions)` | 
min | *All* | `min(commit_insertions)` |  
avg | Number | `avg(commit_insertions)` |  
sum | Number | `sum(commit_insertions)` |

### Directives

How gitql should behave

Name | Description
--- | --- |
#thread1 | Uses one thread to search. Change number 1 by the desired number of threads
#no_fetch | Prevents fetching before searching
#no_prune | When fetching, doesn't remove branches that don't exist in remote