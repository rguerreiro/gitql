use criterion::Criterion;
use gitql::gql;

fn simple_gql(c: &mut Criterion) {
    c.bench_function("Simple GQL", |b| {
        b.iter(|| {
            gql::parse(
                r#"(
                                author !~ "Rafael Guerreiro"
                                and date > 10 days
                            ) or (
                                file = /^.*?\.rs$/
                                and modified_files < 1_000
                            )
                            order by repository, commit asc, commit_message desc"#,
            );
        })
    });
}

fn large_gql(c: &mut Criterion) {
    c.bench_function("Large GQL", |b| {
        b.iter(|| {
            gql::parse(r#"(
                                commit_message !~ "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec id lobortis sem, id facilisis nibh."
                                and date > 10 days
                            ) or (
                                file = /^.*?\.rs$/
                                and modified_files < 1_000
                            )
                            order by repository, commit asc, commit_message desc"#);
        })
    });
}

criterion_group!(
    benches, simple_gql,
    //large_gql
);
